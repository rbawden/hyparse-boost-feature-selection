
#include "globals.h"
#include "utilities.h"
#include "srparser.h"
#include "treebank.h"
#include "sparse_encoder.h"
#include <getopt.h>
#include <iostream>
#include <fstream>
#include <sys/stat.h>
#include <string>

//This the parser command line interface

string infile;
string outfile;
string conllfile;
string nativefile;
string modelfile;
int processors=1;
int K=1;
int c;
bool marmot=false;

int main(int argc,char *argv[]){
    
    while(true){
        static struct option long_options[] ={
            {"help",no_argument,0,'h'},
            {"infile",required_argument,0,'I'},
            {"model",required_argument,0,'m'},
            {"processors",required_argument,0,'p'},
            {"kbest",required_argument,0,'K'},
            {"outfile",required_argument,0,'O'},
            {"conllx",required_argument,0,'C'},
            {"native",required_argument,0,'N'},
	    {"marmot",no_argument,0,'M'},
        };
        int option_index = 0;
        c = getopt_long (argc, argv, "I:hMm:p:K:O:C:N:",long_options, &option_index);
        if(c==-1){break;}
        switch(c){
            case 'I':
                infile = optarg;
                break;
            case 'm':
                modelfile = optarg;
                break;
            case 'p':
                processors = atoi(optarg);
                break;
            case 'K':
                K = atoi(optarg);
                break;
            case 'O':
                outfile = optarg;
                break;
            case 'C':
                conllfile = optarg;
                break;
            case 'N':
                nativefile = optarg;
                break;
	    case 'M':
	      marmot = true;
	      break;
            case 'h':
            default:
                cerr << "This is the command line for the Shift Reduce parser"<<endl;
                cerr << "Usage:"<<endl;
                cerr << "        -h --help                    displays this message and exits." << endl;
                cerr << "        -I --infile      [FILENAME]  sets the input file to FILENAME (defaults to <STDIN>)" <<endl;
                cerr << "        -O --outfile     [FILENAME]  sets the output ptb file to FILENAME (defaults to <STDOUT>)" <<endl;
                cerr << "        -C --conllx      [FILENAME]  sets the output conll file to FILENAME (defaults to none)" <<endl;
                cerr << "        -N --native      [FILENAME]  sets the output native file to FILENAME (defaults to none)" <<endl;
                cerr << "        -m --model       [DIRNAME]   sets the model DIRNAME (mandatory)"   <<endl;
                cerr << "        -p --processors  [INT]       sets the number of processors to use" <<endl;
                cerr << "        -K --kbest       [INT]       sets the number of solutions to output" <<endl;
		cerr << "        -M --marmot                  expects marmot output as input" <<endl;              
  cerr << endl;
                exit(1);
        }
    }
    if(modelfile.empty()){cerr<<"Oops! no model provided"<<endl<<"aborting."<<endl;exit(1);}

   
    //Sets up out stream
    MultiOutStream out;
    if (!outfile.empty()){out.setPennTreebankOutfile(outfile);}
    if(!conllfile.empty()){out.setConllOutfile(conllfile);}
    if(!nativefile.empty()){out.setNativeOutfile(nativefile);}
   
    //Start up the logger
    string logfile = modelfile+"/parse.log";
    PLOGGER_START(logfile.c_str());
   
    //Loads the parser
    IntegerEncoder::get()->load(modelfile+"/encoder");
    SrParser srp(modelfile);

    AbstractLexer *lex;
    if(marmot){
      vector<wstring> ttd;
      MarmotLexer::read_ttd(ttd,modelfile+"/ttd");
      lex = new MarmotLexer(ttd);
    }else{
      lex = new TbkLexer();
    }
    
    //RUN
    
    if(!infile.empty() && processors == 1){
        ifstream input_source(infile);
        srp.parse_corpus(lex,input_source,K,out);
        input_source.close();
        
    }else if(processors == 1){
        srp.parse_corpus(lex,cin,K,out);
    }
    
    //#ifdef ENABLE_PARALLEL
    if(!infile.empty() && processors > 1){
        ifstream input_source(infile);
        srp.batch_parse_corpus(processors*50,lex,input_source,K,out);
        input_source.close();
        
    }else if(processors > 1){
        srp.batch_parse_corpus(processors*50,lex,cin,K,out);
    }
    //#endif
    
    out.close();
    PLOGGER_STOP();
    delete lex;
    return 0;
}
