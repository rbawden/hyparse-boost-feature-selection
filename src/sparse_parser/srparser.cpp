

#include "srparser.h"
#include "treebank.h"
#include <string>
#include <cmath>

#ifdef ENABLE_PARALLEL
    #include <omp.h>
#endif

void dump_xvec(vector<unsigned int> const &xvec){
    
    for (int i = 0;i < xvec.size();++i){
        cout << xvec[i] << ",";
        }
        cout << endl;
        
}



//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//Aux helper struct for logging epochs
struct EpochSummary{
    int epoch_id;
    float loss;
    float trainF;
    float devF;
    float loc_acc_train;
    float loc_acc_dev;
    float eta;
    void init(){
        loss = 0.0;
        trainF = 0;
        devF = 0;
        loc_acc_train = 0;
        loc_acc_dev   = 0;
        eta = 0.0;
    }
    void display(){
        cerr << "Iteration "<<epoch_id << " : loss="<<loss<< " eta=" << eta <<" train-F-Score="<<trainF<<" dev-F-Score="<<devF;
        if(loc_acc_train > 0){cerr << " train local acc="<<loc_acc_train<< " dev local acc="<<loc_acc_dev;}
        cerr << endl;
    }
};

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

SrParser::SrParser(){
    parsing_model = new SparseFloatMatrix(KERNEL_SIZE,1);
}

SrParser::SrParser(SparseEncoder const &spencoder){
    this->spencoder = spencoder;
    parsing_model = new SparseFloatMatrix(KERNEL_SIZE,1);
}
SrParser::SrParser(const char* modelfile){
    parsing_model = NULL;//new SparseFloatMatrix(KERNEL_SIZE,1);
    reload(string(modelfile));
}

SrParser::SrParser(const string &modelfile){
    parsing_model = NULL;//new SparseFloatMatrix(KERNEL_SIZE,1);
    reload(modelfile);
}

SrParser::SrParser(SrParser const &other){
    
    this->spencoder = other.spencoder;
    this->grammar   = other.grammar;
    this->beam_size      = other.beam_size;
    delete parsing_model;
    this->parsing_model = new SparseFloatMatrix(*(other.parsing_model));
    this->transformed = other.transformed;
}

SrParser& SrParser::operator=(SrParser const &other){
    
    this->spencoder = other.spencoder;
    this->grammar   = other.grammar;
    this->beam_size = other.beam_size;
    delete parsing_model;
    this->parsing_model = new SparseFloatMatrix(*(other.parsing_model));
    this->transformed = other.transformed;

    return *this;
}

SrParser::~SrParser(){
    delete parsing_model;
}


void SrParser::save(string const &filename)const{
    
    grammar.save(filename+"/grammar");
    parsing_model->save(filename+"/model");
    
    ofstream beamout(filename+"/beam");
    beamout << beam_size << endl;
    beamout.close();
}

void SrParser::reload(string const &modelfile){

    //1.Reads sparse encoder
    TemplateTypeDefinition ttd;
    ttd.load(modelfile+"/ttd");
    this->spencoder = SparseEncoder(modelfile+"/templates",ttd);
   
    //2. Grammar
    this->grammar = SrGrammar(modelfile+"/grammar");
    transformed = grammar.has_tranformations();
   
    //3. beam
    string bfr;
    ifstream beamin(modelfile+"/beam");
    getline(beamin,bfr);
    beamin.close();
    beam_size = stoi(bfr);
    
    //4. Weights
    delete parsing_model;
    parsing_model = new SparseFloatMatrix(KERNEL_SIZE,grammar.num_actions(),modelfile+"/model");
}

/////////////////////////////  LARGE MARGIN && MAX VIOLATION UPDATE ///////////////////////////////////////////



/**
 *  A function object for valuating cost sensitive loss functions
 */
class CostSensitiveMargin{
    
public:
    CostSensitiveMargin(bool constant=false,float multiplier=1.0);
    
    float operator()(ParseDerivation &derivA,ParseDerivation &derivB,int i,InputDag const &input_sequence)const;
private:
    float multiplier = 1.0;
    bool constant = false;
};

CostSensitiveMargin::CostSensitiveMargin(bool constant,float multiplier){
    this->constant   = constant;
    this->multiplier = multiplier;
}

float CostSensitiveMargin::operator()(ParseDerivation &derivA,ParseDerivation &derivB, int i, InputDag const &input_sequence)const{
    if (constant){return 1.0;}
    return (multiplier * std::sqrt(derivA.hamming(derivB,i,&input_sequence)));
}





ParseState* SrParser::find_max_violation(ParseDerivation &ref_deriv,
                                          TSSBeam &beam,
                                          InputDag const &input_sequence,
                                          size_t N,
                                          ModelType modtype){
 
    ref_deriv.reweight_derivation(spencoder,*parsing_model,grammar,&input_sequence,N);
 
    float min_margin = std::numeric_limits<float>::infinity();
    ParseState* argminv = NULL;
 
    CostSensitiveMargin csfunc(true);
    
    for(int i = 1; i < ref_deriv.size() && beam.size_at(i) > 0 ;++i){
        for(int k = 0; k < beam.size_at(i);++k){
            ParseState *conc_top = beam.kth_at(i,k);
            ParseDerivation concurrent(conc_top);
            float margin = ref_deriv[i]->weight() - conc_top->weight();
            if(modtype == PERCEPTRON && margin <= 0 && margin <= min_margin){//Perceptron is not cost sensitive in this implementation
                if(!ref_deriv[i]->is_hamming_equivalent(*concurrent[i],&input_sequence,N)){
                    min_margin = margin;
                    argminv = conc_top;
                    break;
                }
            }
            if(modtype != PERCEPTRON && margin <= csfunc(ref_deriv,concurrent,i,input_sequence) && margin <= min_margin){
                if(!ref_deriv[i]->is_hamming_equivalent(*concurrent[i],&input_sequence,N)){
                    min_margin = margin;
                    argminv = conc_top;
                    break;
                }
            }
        }
    }
    return argminv;
 }
 
 //cost sensitive loss
 float SrParser::get_loss(ParseDerivation  &pred_deriv,ParseDerivation &ref_deriv,InputDag const &input_sequence,ModelType modtype){
     size_t idx = pred_deriv.size()-1;
     if(modtype == PERCEPTRON){return std::max<float>(0.0,(pred_deriv[idx]->weight() - ref_deriv[idx]->weight()));}
     return std::max<float>(0.0, (pred_deriv[idx]->weight() + std::sqrt(ref_deriv.hamming(pred_deriv,idx,&input_sequence))) - ref_deriv[idx]->weight());
 }


bool SrParser::collect_large_margin_delta(AbstractParseTree const *ref,TSSBeam &beam,SparseFloatMatrix &delta,float &loss, ModelType modtype){
 
    
    InputDag input_sequence;
    tree_yield(ref,input_sequence);
    ParseDerivation ref_deriv(ref,grammar,input_sequence,spencoder.has_morpho());
    size_t N = input_sequence.size();
 
    vector<unsigned int> xvec(spencoder.ntemplates(),0);
    vector<float> y_scores(grammar.num_actions(),0.0);
    
    predict_one(input_sequence,beam,xvec,y_scores,false);
    
    ParseState *maxv_state = find_max_violation(ref_deriv,beam,input_sequence,N,modtype);
    
    if(NULL != maxv_state){//if there is an error state violating the margin
        ParseDerivation pred_derivation(maxv_state);
        loss = get_loss(pred_derivation, ref_deriv, input_sequence, modtype);
        fill_delta(pred_derivation,ref_deriv,input_sequence,delta);
        return true;
    }
    return false;
}

float SrParser::large_margin_learn_minibatch(vector<const AbstractParseTree*> const &mbatch,
                                  SparseFloatMatrix &delta,
                                  SparseFloatMatrix &averaged_model,
                                  ModelType modtype,
                                  float &eta,//learning rate
                                  float T,//time step
                                  float t0,//init averaging at t0
                                  float C,//PA-I bound
                                  float lambda)//pegasos regularizer
{
    
    delta.clear();
    int nups = 0;
    size_t bN = mbatch.size();
    
    float batch_loss = 0;
    eta = 0;
    #pragma omp parallel for schedule(dynamic) shared(delta)
    for(int p = 0; p  < bN ; ++p){                                  //** parse in parallel **
        TSSBeam beam(beam_size,grammar);
        float loss = 0;
        if (collect_large_margin_delta(mbatch[p],beam, delta,loss,modtype)){
            #pragma omp atomic
            ++nups;
            #pragma omp atomic
            batch_loss+=loss;
        }
    }
    if(nups > 0){                                             //** serial update **
        switch (modtype) {
            case MIRA:
            {
                eta = min<float>(C,batch_loss/delta.sqL2norm());
                delta *= eta/(float)bN;
                *parsing_model += delta;
                //perceptron averaging trick
                delta *= T;
                averaged_model+=delta;
                break;
            }
            case PERCEPTRON:
            {
                eta = 1.0;
                delta *= eta / (float) bN;
                *parsing_model += delta;
                delta *= T;
                averaged_model += delta;
                break;
            }
            case PEGASOS:
            {
                eta = 1/(lambda * T);
                delta *= (eta/bN);
                *parsing_model *= (1-(1/T));
                *parsing_model += delta;
                //@idea from Bottou, ASGD averaging : \bar{w} = (T/T+1)\bar{w} + (1/T+1) w_t+1
                float delta_t = T-t0;
                if( delta_t == 1){ //Averaged SGD
                    averaged_model = *parsing_model;
                }else if( delta_t > 1){
                    averaged_model *= delta_t;
                    averaged_model += (*parsing_model);
                    averaged_model *= (1/(delta_t+1));
                }
                break;
            }
            default:
                cerr << "Error (update) aborting." << endl;
                exit(1);
        }
    }else{
        if(modtype==PEGASOS){
            *parsing_model *= (1-(1/T));
            //@idea from Bottou, ASGD averaging : \bar{w} = (T/T+1)\bar{w} + (1/T+1) w_t+1
            float delta_t = T-t0;
            if( delta_t == 1){ //Averaged SGD
                averaged_model = *parsing_model;
            }else if(delta_t > 1){
                averaged_model *= delta_t;
                averaged_model += (*parsing_model);
                averaged_model *= (1/(delta_t+1));
            }
        }
    }
    if(modtype==PERCEPTRON){
        eta = 1;
        return nups;
    }else{
        return batch_loss;
    }
}

float SrParser::large_margin_learn_one(AbstractParseTree const *ref,
                                            SparseFloatMatrix &delta,
                                            SparseFloatMatrix &averaged_model,
                                            ModelType modtype,
                                            float &eta,
                                            int T,//time step
                                            float t0,
                                            float C,//PA-I bound
                                            float lambda)

{
    delta.clear();
    TSSBeam beam(beam_size,grammar);
    float loss = 0;
    eta = 0;
    if (collect_large_margin_delta(ref,beam, delta,loss,modtype)){
        switch (modtype) {
            case MIRA:
            {
                eta = min<float>(C,loss/delta.sqL2norm());
                delta *= eta;
                *parsing_model += delta;
                delta *= T;
                averaged_model+=delta;
                break;
            }
            case PERCEPTRON:
            {
                eta = 1.0;
                *parsing_model += delta;
                delta *= T;
                averaged_model+=delta;
                break;
            }
            case PEGASOS:
            {
                eta = 1 / (lambda * T);
                delta *= eta;
                *parsing_model *= (1-(1/T));
                *parsing_model += delta;
                //averaging : \bar{w} = (T/T+1)\bar{w} + (1/T+1) w_t+1
                float delta_t = T-t0;
                if( delta_t == 1){ //Averaged SGD
                    averaged_model = *parsing_model;
                }else if(delta_t > 1){
                    averaged_model *= delta_t;
                    averaged_model += *parsing_model;
                    averaged_model *= (1/(delta_t+1));
                }
                break;
            }
            default:
                cerr << "Error (update) aborting." << endl;
                exit(1);
        }
        
    }else{
        if(modtype==PEGASOS){//passive update
            *parsing_model *= (1-(1/T));
            float delta_t = T - t0;
            if( delta_t == 1 ){
                averaged_model = *parsing_model;
            }else if (delta_t > 1) { //Averaged SGD
                //averaging : \bar{w} = (T/T+1)\bar{w} + (1/T+1) w_t+1
                float delta_t = T-t0;
                averaged_model *= delta_t;
                averaged_model += *parsing_model;
                averaged_model *= (1/(delta_t+1));
            }
        }
    }
    if (modtype==PERCEPTRON){
        eta = 1.0;
        return (float) loss != 0;
    }else{
        return loss;
    }
}


void SrParser::fill_delta(ParseDerivation const &pred_deriv,ParseDerivation const &ref_deriv,InputDag const &input_sequence,SparseFloatMatrix &delta){
        
    SparseFloatVector ref_xvec(KERNEL_SIZE);
    SparseFloatVector pred_xvec(KERNEL_SIZE);
    int N = input_sequence.size();
    
    for(int i = 0; i < ref_deriv.size()-1 && i < pred_deriv.size()-1;++i){
        
        StateSignature pred_sig;
        StateSignature ref_sig;
        
        //if(pred_deriv[i+1]->get_incoming_action() != ref_deriv[i+1]->get_incoming_action()){
            
        ref_deriv[i]->get_signature(ref_sig,&input_sequence,N);
        ref_deriv[i]->encode(ref_xvec,spencoder,ref_sig);
        pred_deriv[i]->get_signature(pred_sig,&input_sequence,N);
        pred_deriv[i]->encode(pred_xvec,spencoder,pred_sig);
        #pragma omp critical
        {

            delta.subs_rowY(pred_xvec,grammar.get_action_index(pred_deriv[i+1]->get_incoming_action()));
            delta.add_rowY(ref_xvec,grammar.get_action_index(ref_deriv[i+1]->get_incoming_action()));
        }
    }
}



///////////////////////////  BASIC EARLY UPDATE PERCEPTRON /////////////////////////////////
ParseState* SrParser::early_update_subsequence(ParseDerivation &ref_deriv,
                                              TSSBeam &beam,
                                              InputDag const &input_sequence,
                                              size_t N){// early
    
        for(int i = 1; (i < ref_deriv.size()-1) ;++i){
            StateSignature ref_sig;
            if (beam.size_at(i) == 0){
                return beam.best_at(i-1);
            }
            ref_deriv[i]->get_signature(ref_sig,&input_sequence,N);
            if(!beam.sig_in_kth_beam(ref_sig,i)){
                return beam.best_at(i);
            }
        }

    StateSignature ref_sig;
    StateSignature pred_sig;
    
    beam.best_success_state()->get_signature(pred_sig,&input_sequence,N);
    ref_deriv[ref_deriv.size()-1]->get_signature(ref_sig, &input_sequence,N);
    if(pred_sig == ref_sig){
        return NULL;
    }else{return beam.best_success_state();}
}



////////////////////////////////////// GLOBAL MODELS //////////////////////////////////////
// RAB : added option  'test-convergence' (default = false) and verbose mode (default=true)
void SrParser::train_global_model(Treebank &train_set,
                                  Treebank &dev_set,
                                  Treebank::Decoration const &transformation,
                                  size_t beam_size,
                                  size_t epochs,
                                  UpdateType uptype,
                                  ModelType modtype,
                                  float C,
                                  float lambda,
                                  bool verbose,
                                  bool test_convergence)
{
    Treebank binarytrain_set;
    Treebank binarydev_set;
    transform_dataset(train_set,dev_set,binarytrain_set,binarydev_set,transformation);
    
    binarytrain_set.update_encoder();
    binarydev_set.update_encoder();

    
    grammar = SrGrammar(binarytrain_set,binarydev_set,spencoder.has_morpho());
    this->beam_size = beam_size;
    
    Treebank binarytrain_sample;
    binarytrain_set.sample_trees(binarytrain_sample,dev_set.size());
    Treebank train_sample(binarytrain_sample);
    train_sample.detransform(transformation);

    delete parsing_model;
    
    parsing_model = new SparseFloatMatrix(KERNEL_SIZE,grammar.num_actions());
    SparseFloatMatrix *averaged_model = new SparseFloatMatrix(KERNEL_SIZE,grammar.num_actions());
    SparseFloatMatrix *delta = NULL;
    if(uptype != EARLY_UP){delta = new SparseFloatMatrix(KERNEL_SIZE,grammar.num_actions());}
    
    float T = 1;
    int t0 = (int)((epochs * binarytrain_set.size())/2);

    EpochSummary summary;

    // RAB : record last loss values to calculate convergence
    vector<float> losses(ITERATIONS_NO_CHANGE);
    float sum_loss_changes = 0.0;

    for(unsigned int e = 1; e < epochs+1;++e){
        summary.init();
        summary.epoch_id = e;
        train_set.shuffle();
        for(int i = 0; i < train_set.size();++i){
            if(modtype==PERCEPTRON && uptype==EARLY_UP){
                summary.loss += global_learn_one(binarytrain_set[i],*averaged_model,T);
                summary.eta+=1.0;
            }else{
                float loc_eta=0.0;
                summary.loss += large_margin_learn_one(binarytrain_set[i],*delta,*averaged_model,modtype,loc_eta,T,t0,C,lambda);
                summary.eta+=loc_eta;
            }
            ++T;
        }
        summary.eta /= train_set.size();
        if(modtype!=PERCEPTRON){summary.loss /= train_set.size();}
        tuple<float,float,float> evalT = eval_model(train_sample);
        tuple<float,float,float> evalD = eval_model(dev_set);
        summary.trainF = std::get<2>(evalT);
        summary.devF = std::get<2>(evalD);
        if (verbose)
            summary.display();
        LLOGGER_SET(LearnLogger::EPOCH,e);
        LLOGGER_SET_TRAIN(std::get<0>(evalT),std::get<1>(evalT),std::get<2>(evalT));
        LLOGGER_SET_DEV(std::get<0>(evalD),std::get<1>(evalD),std::get<2>(evalD));
        LLOGGER_SET(LearnLogger::LOSS,summary.loss);
        LLOGGER_WRITE();
        if(summary.loss==0){break;}

        // RAB : if average loss value for k last epochs within threshold limits, do not continue
        if (test_convergence) {
            losses.insert(losses.begin(), summary.loss); // insert at beginning
            losses.pop_back(); // remove last one
            if (e>ITERATIONS_NO_CHANGE){
                sum_loss_changes=0;
                for (int i=1; i<losses.size(); ++i)
                    sum_loss_changes+= losses[i]-losses[i-1];
                if ((sum_loss_changes/ITERATIONS_NO_CHANGE) > (-THRESHOLD_CONVERGENCE) && (sum_loss_changes/ITERATIONS_NO_CHANGE) < THRESHOLD_CONVERGENCE)
                    break;
            }
        }
    }
    if(uptype != EARLY_UP){delete delta;}
    
    //Final averaging
    if (modtype != PEGASOS){
        (*averaged_model) *= 1.0/T;
        (*parsing_model) -= (*averaged_model);
        delete averaged_model;
    }
    if (modtype == PEGASOS){
        delete parsing_model;
        parsing_model = averaged_model;
    }
}

bool SrParser::global_learn_one(AbstractParseTree const *root,
                                SparseFloatMatrix &averaged_model,
                                float C)
{
 
    InputDag input_sequence;
    tree_yield(root,input_sequence);
    ParseDerivation ref_deriv(root,grammar,input_sequence,spencoder.has_morpho());
    size_t N = input_sequence.size();
    
    
    StateSignature sig;
    vector<unsigned int> xvec(spencoder.ntemplates(),0);
    vector<float> y_scores(grammar.num_actions(),0.0);
    
    //PARSING STEP
    size_t eta = spencoder.has_morpho() ? (2 * N) : (3 * N);
    TSSBeam beam(beam_size,grammar);
    //cout << "*************** START ****************" << endl;
    for(int i = 1; i < eta;++i){
        for(int k = 0; k < beam.top_size();++k){
            ParseState *stack_top = beam[k];
            stack_top->get_signature(sig,&input_sequence,N);
            stack_top->encode(xvec,spencoder,sig);
            parsing_model->dot(xvec,y_scores);
            grammar.select_actions(y_scores,stack_top->get_incoming_action(),sig);
            beam.push_candidates(stack_top,y_scores);
        }
        beam.next_step(grammar,input_sequence,N);
    }

    //UPDATE STEP
    ParseState *last = early_update_subsequence(ref_deriv,beam,input_sequence,N);//early only
    
    if (NULL != last){//if has update
        ParseDerivation pred_deriv(last);
        
        SparseFloatVector ref_xvec(KERNEL_SIZE);
        SparseFloatVector pred_xvec(KERNEL_SIZE);
    
        for(int i = 0; i < ref_deriv.size()-1 && i < pred_deriv.size()-1;++i){
            StateSignature pred_sig;
            StateSignature ref_sig;
        
            //update
            ref_deriv[i]->get_signature(ref_sig,&input_sequence,N);
            ref_deriv[i]->encode(ref_xvec,spencoder,ref_sig);
            pred_deriv[i]->get_signature(pred_sig,&input_sequence,N);
            pred_deriv[i]->encode(pred_xvec,spencoder,pred_sig);
            
            parsing_model->subs_rowY(pred_xvec,grammar.get_action_index(pred_deriv[i+1]->get_incoming_action()));
            parsing_model->add_rowY(ref_xvec,grammar.get_action_index(ref_deriv[i+1]->get_incoming_action()));
            
            //averaging
            pred_xvec *= C;
            ref_xvec  *= C;
            averaged_model.subs_rowY(pred_xvec,grammar.get_action_index(pred_deriv[i+1]->get_incoming_action()));
            averaged_model.add_rowY(ref_xvec,grammar.get_action_index(ref_deriv[i+1]->get_incoming_action()));
        
            //if(pred_deriv[i+1]->get_incoming_action() != ref_deriv[i+1]->get_incoming_action()){has_update = true;}
        }
        return true;
    }
    //if(has_update){cout << "update!"<<endl;}
    return false;
}


////////////////////////////////////  MINI BATCHES  //////////////////////////////////////////

void SrParser::make_minibatches(size_t mbatch_size, Treebank &train_bank,vector<vector<const AbstractParseTree*>> &mini_batches)const{
    //makes mini batches of approx equal parsing time
    
    vector<pair<int,const AbstractParseTree*>> sized_tree_list;
    for(int i = 0 ; i < train_bank.size();++i){
        InputDag input_sequence;
        tree_yield(train_bank[i],input_sequence);
        sized_tree_list.push_back(make_pair(input_sequence.size(),train_bank[i]));
    }
    
    std::sort(sized_tree_list.begin(),sized_tree_list.end(),[] (pair<int,const AbstractParseTree*> const &treeA,
                                                               pair<int,const AbstractParseTree*> const &treeB) -> bool {return treeA.first < treeB.first;});
    
    
    size_t K = std::ceil((float)train_bank.size() /(float)mbatch_size);
    mini_batches.clear();
    mini_batches.resize(K,vector<const AbstractParseTree*>());
    
    int k = 0;
    for(int i = 0; i < sized_tree_list.size();++i){
        if(k >= K){k = 0;}
        mini_batches[k].push_back(sized_tree_list[i].second);
        ++k;
    }
}

void SrParser::shuffle_minibatches(vector<vector<const AbstractParseTree*>> &mini_batches)const{
    std::random_shuffle(mini_batches.begin(),mini_batches.end());
    for(int i = 0; i < mini_batches.size();++i){
        std::random_shuffle(mini_batches[i].begin(),mini_batches[i].end());
    }
}

// RAB : added option  'test-convergence' (default = false) and option verbose
void SrParser::train_global_minibatch_model(Treebank &train_set,
                                            Treebank &dev_set,
                                            Treebank::Decoration const &transformation,
                                            size_t beam_size,
                                            size_t epochs,
                                            size_t minibatch_size,
                                            UpdateType uptype,
                                            ModelType modtype,
                                            float C,
                                            float lambda,
                                            bool verbose,
                                            bool test_convergence){
    
    assert(minibatch_size > 1);//low minibatches size work but its ridiculous to use this function in that case.
    
    Treebank binarytrain_set;
    Treebank binarydev_set;
    transform_dataset(train_set,dev_set,binarytrain_set,binarydev_set,transformation);
    
    binarytrain_set.update_encoder();
    binarydev_set.update_encoder();

    
    grammar = SrGrammar(binarytrain_set,binarydev_set,spencoder.has_morpho());
    this->beam_size = beam_size;
    
    Treebank binarytrain_sample;
    binarytrain_set.sample_trees(binarytrain_sample,dev_set.size());
    Treebank train_sample(binarytrain_sample);
    train_sample.detransform(transformation);
    
    
    delete parsing_model;
    parsing_model = new SparseFloatMatrix(KERNEL_SIZE,grammar.num_actions());
    SparseFloatMatrix *averaged_model = new SparseFloatMatrix(KERNEL_SIZE,grammar.num_actions());

    vector<vector<const AbstractParseTree*> > batches;
    make_minibatches(minibatch_size, binarytrain_set, batches);
    
    unsigned int T = 1;
    int t0 = (int)((epochs * batches.size())/2);
    
    SparseFloatMatrix delta(KERNEL_SIZE,grammar.num_actions());
    EpochSummary summary;

    // RAB : record last loss value (and number of epochs where no change) to calculate convergence
    vector<float> losses(ITERATIONS_NO_CHANGE);
    float sum_loss_changes = 0.;

    
    for(unsigned int e = 1; e < epochs+1;++e){
        //if(e > (float)epochs/2.0){T=1;}//average only from the 2nd stage of the learning process
        summary.init();
        summary.epoch_id = e;
        shuffle_minibatches(batches);
        
        for(int i = 0; i < batches.size();++i){
            if(modtype==PERCEPTRON && uptype==EARLY_UP){
                summary.loss  += global_learn_minibatch(batches[i],delta,*averaged_model,T);
                summary.eta   += 1.0;
            }else{
                float loc_eta  = 0.0;
                summary.loss  += large_margin_learn_minibatch(batches[i], delta,*averaged_model, modtype,loc_eta,T,t0,C, lambda);
                summary.eta   += loc_eta;
            }
            //if(e > epochs/2.0){++T;}
            ++T;
        }
        summary.eta /= batches.size();
        if(modtype!=PERCEPTRON){summary.loss /= batches.size();}
        tuple<float,float,float> evalT = eval_model(train_sample);
        tuple<float,float,float> evalD = eval_model(dev_set);
        summary.trainF = std::get<2>(evalT);
        summary.devF = std::get<2>(evalD);
        if (verbose)
            summary.display();
        LLOGGER_SET(LearnLogger::EPOCH,e);
        LLOGGER_SET_TRAIN(std::get<0>(evalT),std::get<1>(evalT),std::get<2>(evalT));
        LLOGGER_SET_DEV(std::get<0>(evalD),std::get<1>(evalD),std::get<2>(evalD));
        LLOGGER_SET(LearnLogger::LOSS,summary.loss);
        LLOGGER_WRITE();
        if(summary.loss==0){break;}

        // RAB : if average loss value for k last epochs within threshold limits, do not continue
        if (test_convergence) {
            losses.insert(losses.begin(), summary.loss); // insert at beginning
            losses.pop_back(); // remove last one
            if (e>ITERATIONS_NO_CHANGE){
                sum_loss_changes=0;
                for (int i=1; i<losses.size(); ++i){
                    sum_loss_changes+= losses[i]-losses[i-1];
                }
                if ((sum_loss_changes/ITERATIONS_NO_CHANGE) > (-THRESHOLD_CONVERGENCE) && (sum_loss_changes/ITERATIONS_NO_CHANGE) < THRESHOLD_CONVERGENCE)
                    break;
            }
        }
    }
    //Final averaging
    if (modtype != PEGASOS){
        (*averaged_model) *= 1.0/T;
        (*parsing_model) -= (*averaged_model);
        delete averaged_model;
    }
    if (modtype == PEGASOS){
        delete parsing_model;
        parsing_model = averaged_model;
    }
}

/**
 * Performs a mini batch evaluation.
 * The mini batch evaluation returns *averaged* bonus and penalty vectors encoding the Delta for the update.
 * @param mbtach: a list of trees (the minibatch)
 * @param t: the t_th step in the learning process (for Pegasos learning rate and the averaging trick)
 * @return the number of wrongly predicted examples in this minibatch
 */
int SrParser::global_learn_minibatch(vector<const AbstractParseTree*> const &mbatch,SparseFloatMatrix &delta,SparseFloatMatrix &averaged_model,float T)
{

    delta.clear();
    
    int nups = 0;
    size_t bN = mbatch.size();
    
    #pragma omp parallel for schedule(dynamic) shared(delta)
    for(int p = 0; p  < bN ; ++p){                        //** parse in parallel **
        
        InputDag input_sequence;
        tree_yield(mbatch[p],input_sequence);
        ParseDerivation ref_deriv(mbatch[p],grammar,input_sequence,spencoder.has_morpho());
        size_t N = input_sequence.size();
        
        StateSignature sig;
        vector<unsigned int> xvec(spencoder.ntemplates(),0);
        vector<float> y_scores(grammar.num_actions(),0.0);
        
        //PARSING STEP
        size_t eta = spencoder.has_morpho() ? (2 * N) : (3 * N);
        
        TSSBeam beam(beam_size,grammar);
        //cout << "*************** START ****************" << endl;
        for(int i = 1; i < eta;++i){
            for(int k = 0; k < beam.top_size();++k){
                ParseState *stack_top = beam[k];
                stack_top->get_signature(sig,&input_sequence,N);
                stack_top->encode(xvec,spencoder,sig);
                parsing_model->dot(xvec,y_scores);
                grammar.select_actions(y_scores,stack_top->get_incoming_action(),sig);
                beam.push_candidates(stack_top,y_scores);
            }
            beam.next_step(grammar,input_sequence,N);
        }
        
        //Find violation
        ParseState *last = early_update_subsequence(ref_deriv,beam,input_sequence,N);
        if(NULL != last){
            ParseDerivation pred_deriv(last);
            SparseFloatVector ref_xvec(KERNEL_SIZE);
            SparseFloatVector pred_xvec(KERNEL_SIZE);
        
            //COLLECT MODS
            for(int i = 0; i < ref_deriv.size()-1 && i < pred_deriv.size()-1;++i){
                StateSignature pred_sig;
                StateSignature ref_sig;
                //update
                ref_deriv[i]->get_signature(ref_sig,&input_sequence,N);
                ref_deriv[i]->encode(ref_xvec,spencoder,ref_sig);
                pred_deriv[i]->get_signature(pred_sig,&input_sequence,N);
                pred_deriv[i]->encode(pred_xvec,spencoder,pred_sig);
                
                #pragma omp critical
                {
                    delta.subs_rowY(pred_xvec, grammar.get_action_index(pred_deriv[i+1]->get_incoming_action()));
                    delta.add_rowY(ref_xvec,grammar.get_action_index(ref_deriv[i+1]->get_incoming_action()));
                }
            }
            
            #pragma omp atomic
            ++nups;
        }
    }
    //END PARALLEL PARSING
    
    //UPDATE HERE                                                                           //   ** update in serial **
    if (nups > 0){
    
        delta *= 1.0 / bN;
        *parsing_model += delta;
                
        delta *= T;
        averaged_model += delta;
    }
    return nups;
}

////////////////////////////////////  LOCAL MODELS  ////////////////////////////////////

unsigned int _argmax(vector<float> scores,size_t Vs){
    float mmax = scores[0];
    unsigned int amax = 0;
    
    for(int j = 1; j <Vs ;++j){
        if(scores[j] > mmax){
            mmax = scores[j];
            amax = j;
        }
    }
    return amax;
}

void SrParser::train_local_model(Treebank &train_set,
                                 Treebank &dev_set,
                                 Treebank::Decoration const &transformation,
                                 size_t beam_size,
                                 size_t epochs,
                                 bool verbose)
{
    Treebank binarytrain_set;
    Treebank binarydev_set;
    transform_dataset(train_set,dev_set,binarytrain_set,binarydev_set,transformation);

    binarytrain_set.update_encoder();
    binarydev_set.update_encoder();

    grammar = SrGrammar(binarytrain_set,binarydev_set,spencoder.has_morpho());
    this->beam_size = beam_size;
    
    Treebank binarytrain_sample;
    binarytrain_set.sample_trees(binarytrain_sample,dev_set.size());
    Treebank train_sample(binarytrain_sample);
    train_sample.detransform(transformation);
    
    delete parsing_model;
    parsing_model = new SparseFloatMatrix(KERNEL_SIZE,grammar.num_actions());
    SparseFloatMatrix *averaged_model = new SparseFloatMatrix(KERNEL_SIZE,grammar.num_actions());
    
    //setup stats
    EpochSummary summary;

    float C = 1;
    for(unsigned int e = 1; e < epochs+1;++e){
        summary.init();
        summary.epoch_id = e;
        for(int i = 0; i < train_set.size();++i){
            summary.loss += local_learn_one(binarytrain_set[i],*averaged_model,C);
            C++;
        }
        //Logging
        tuple<float,float,float> evalT = eval_model(train_sample);
        summary.trainF = std::get<2>(evalT);
        tuple<float,float,float> evalD = eval_model(dev_set);
        summary.devF = std::get<2>(evalD);
        summary.loc_acc_train = eval_local_model(binarytrain_sample);
        summary.loc_acc_dev   = eval_local_model(binarydev_set);
        summary.eta = 1.0;
        
        LLOGGER_SET(LearnLogger::EPOCH,e);
        LLOGGER_SET_TRAIN(std::get<0>(evalT),std::get<1>(evalT),std::get<2>(evalT));
        LLOGGER_SET_DEV(std::get<0>(evalD),std::get<1>(evalD),std::get<2>(evalD));
        LLOGGER_SET(LearnLogger::LOSS,summary.loss);
        LLOGGER_WRITE();
        if (verbose){summary.display();}

    }
    //Final averaging
    (*averaged_model) *= 1.0/C;
    (*parsing_model) -= (*averaged_model);
    delete averaged_model;
}


int SrParser::local_learn_one(AbstractParseTree const *root,SparseFloatMatrix &averaged_model,float C){
   
    InputDag input_sequence;
    tree_yield(root,input_sequence);
    ParseDerivation deriv(root,grammar,input_sequence,spencoder.has_morpho());
    size_t N = input_sequence.size();
    StateSignature sig;
    int nupdates = 0;
    vector<unsigned int> xvec(spencoder.ntemplates(),0);
    vector<float> y_scores(grammar.num_actions(),0.0);
    
    
    for(int i = 0; i < deriv.size()-1;++i){
        
        deriv[i]->get_signature(sig,&input_sequence,N);
        deriv[i]->encode(xvec,spencoder,sig);
        parsing_model->dot(xvec,y_scores);
        grammar.select_actions(y_scores,deriv[i]->get_incoming_action(),sig);
        
        //find argmax action
        unsigned int argmax = _argmax(y_scores,y_scores.size());
        //update (if wrong)
        if(grammar[argmax] != deriv[i+1]->get_incoming_action()){
            SparseFloatVector lxvec(xvec,KERNEL_SIZE);
            //cout << lxvec.to_string() << endl;
            parsing_model->subs_rowY(lxvec,argmax);
            parsing_model->add_rowY(lxvec,grammar.get_action_index(deriv[i+1]->get_incoming_action()));
            
            //averaging
            lxvec *= C;
            averaged_model.subs_rowY(lxvec,argmax);
            averaged_model.add_rowY(lxvec,grammar.get_action_index(deriv[i+1]->get_incoming_action()));
            
            ++nupdates;
        }
    }
    return nupdates;
}



///////////////////////////////// PREDICTION ///////////////////////////////////////

AbstractParseTree* SrParser::predict_one(InputDag &input_sequence,TSSBeam &beam,vector<unsigned int> &xvec,vector<float> &y_scores,bool make_tree){
    
    size_t N = input_sequence.size();
    size_t eta = spencoder.has_morpho() ? (2 * N)-1 : (3 * N)-1;
    StateSignature sig;
    beam.reset();
    std::fill(xvec.begin(),xvec.end(),0);
    std::fill(y_scores.begin(),y_scores.end(),0);
    
    
    for(int i = 0; i < eta;++i){
      for(int k = 0; k < beam.top_size();++k){
          
          ParseState *stack_top = beam[k];
          stack_top->get_signature(sig,&input_sequence,N);
          stack_top->encode(xvec,spencoder,sig);
          parsing_model->dot(xvec,y_scores);
          grammar.select_actions(y_scores,stack_top->get_incoming_action(),sig);
          beam.push_candidates(stack_top,y_scores);
      }
      beam.next_step(grammar,input_sequence,N);
    }
    if (make_tree && beam.has_best_parse()){
        return beam.best_parse(input_sequence);
    }else{
        return NULL;
    }
}

AbstractParseTree* SrParser::get_kth_parse(int k, InputDag &input_sequence,TSSBeam &beam)const{
    if (k < beam.num_parses()){
        return beam.kth_best_parse(input_sequence,k);
    }
    return NULL;
}

void SrParser::batch_parse_corpus(size_t max_batch_size,AbstractLexer *lex,istream &input_source,int K,MultiOutStream &outstream){
    
  lex->skip_header(input_source);
    InputDag input_sequence;
    vector<InputDag> batch_input;
    size_t csize=0;
    while(true){
        
        bool eof = ! lex->next_sentence(input_source,input_sequence);
        if (csize == max_batch_size || eof){
            
            vector<AbstractParseTree*> results(K*csize,NULL);
            #pragma omp parallel for schedule(dynamic)
            for(int i = 0; i < csize;++i){
                
                TSSBeam beam(beam_size,grammar);
                vector<unsigned int> xvec(spencoder.ntemplates(),0);
                vector<float> y_scores(grammar.num_actions(),0.0);
                predict_one(batch_input[i],beam,xvec,y_scores,false);
                for(int j = 0; j < K && j < beam.num_parses();++j){
                    AbstractParseTree *p = beam.kth_best_parse(batch_input[i],j);
                    unpack_unaries(p);
                    unbinarize(p);
                    if(transformed){undecorate(p);}
                    results[i*K+j] = p;
                }
            }
            for(int i  = 0; i < csize;++i){
                for(int j = 0; j < K;++j){
                    if (results[i*K+j]==NULL){
                        outstream.flush_failure(batch_input[i]);
                    }else{
                        outstream.flush_parse(results[i*K+j],batch_input[i]);
                        destroy(results[i*K+j]);
                        results[i*K+j] = NULL;
                    }
                }
            }
            csize=0;
            batch_input.clear();
        }
        if(eof){break;}
        else{
            batch_input.push_back(input_sequence);
            ++csize;
        }
    }
}





void SrParser::parse_corpus(Treebank const &treebankIn,
                            MultiOutStream &outstream){
    
    
    TSSBeam beam(beam_size,grammar);
    vector<unsigned int> xvec(spencoder.ntemplates(),0);
    vector<float> y_scores(grammar.num_actions(),0.0);
    
    for(int i = 0; i < treebankIn.size();++i){
        InputDag input_sequence;
        tree_yield(treebankIn[i],input_sequence);
        //parsing
        PLOGGER_START_TIMER();
        AbstractParseTree *p = predict_one(input_sequence,beam,xvec,y_scores);
        //cout << *p << endl;
        PLOGGER_END_TIMER();
        PLOGGER_SET(ParseLogger::LEN,input_sequence.size());
        PLOGGER_WRITE();
        
        //1-best parsing
        if (beam.has_best_parse()){
                unpack_unaries(p);
                unbinarize(p);
                if(transformed){undecorate(p);}
                outstream.flush_parse(p,input_sequence);
                destroy(p);
        }else{
            outstream.flush_failure(input_sequence);
        }
    }
}


void SrParser::parse_corpus(AbstractLexer *lex,istream &input_source,int K,MultiOutStream &outstream){
    
  lex->skip_header(input_source);
    InputDag input_sequence;
    
    TSSBeam beam(beam_size,grammar);
    vector<unsigned int> xvec(spencoder.ntemplates(),0);
    vector<float> y_scores(grammar.num_actions(),0.0);
    
    while(lex->next_sentence(input_source,input_sequence)){
        
        //parsing
        PLOGGER_START_TIMER();
        AbstractParseTree *p = predict_one(input_sequence,beam,xvec,y_scores);
        //cout << *p << endl;
        PLOGGER_END_TIMER();
        PLOGGER_SET(ParseLogger::LEN,input_sequence.size());
        PLOGGER_WRITE();
        
        //postprocessing
        if(K > 1){ //K-best parsing
            unpack_unaries(p);
            unbinarize(p);
            if(transformed){undecorate(p);}
            outstream.flush_parse(p,input_sequence);
            destroy(p);
            
            for(int i = 1; i < K && i < beam.num_parses();++i){
                p = beam.kth_best_parse(input_sequence,i);
                unpack_unaries(p);
                unbinarize(p);
                if(transformed){undecorate(p);}
                outstream.flush_parse(p,input_sequence);
                destroy(p);
            }
            cout << endl;
            
        }else{ //1-best parsing
            if (beam.has_best_parse()){
                unpack_unaries(p);
                unbinarize(p);
                if(transformed){undecorate(p);}
                outstream.flush_parse(p,input_sequence);
                destroy(p);
            }else{
                outstream.flush_failure(input_sequence);
            }
        }
    }
}


///////////////////////////////// AUTO-EVAL ///////////////////////////////////////

float SrParser::eval_local_model(const Treebank &eval_set){

    float ncorrect = 0;
    float ntotal = 0;
    
    vector<InputToken> input_sequence;
    for(int i = 0; i < eval_set.size();++i){
        pair<float,float> res = local_eval_one(eval_set[i]);
        ncorrect += res.first;
        ntotal   += res.second;
    }
    //cout << "Local Accurracy: "<< ncorrect/ntotal << " ncorrect: " << ncorrect << " ntotal: " << ntotal << endl;
    return ncorrect/ntotal;
}

pair<float,float> SrParser::local_eval_one(AbstractParseTree const *root){
    
    InputDag input_sequence;
    tree_yield(root,input_sequence);
    ParseDerivation deriv(root,grammar,input_sequence,spencoder.has_morpho());
    size_t N = input_sequence.size();

    StateSignature sig;
    vector<unsigned int> xvec(spencoder.ntemplates(),0);
    vector<float> y_scores(grammar.num_actions(),0.0);
    
    float ncorrect = 0;
    float ntotal = 0;
    for(int i = 0; i < deriv.size()-1;++i){
        deriv[i]->get_signature(sig,&input_sequence,N);
        deriv[i]->encode(xvec,spencoder,sig);
        parsing_model->dot(xvec,y_scores);
        
        grammar.select_actions(y_scores,deriv[i]->get_incoming_action(),sig);
        
        //find argmax action
        unsigned int argmax = _argmax(y_scores,y_scores.size());
        if(grammar[argmax] == deriv[i+1]->get_incoming_action()){
            ++ncorrect;
        }
        ++ntotal;
    }
    return make_pair(ncorrect,ntotal);
}

tuple<float,float,float> SrParser::eval_model(const Treebank &eval_set){
    
    size_t N = eval_set.size();
    typedef tuple<float,float,float> EVAL_TRIPLE;
    vector<EVAL_TRIPLE> eval_res;
    eval_res.resize(N);
    
    #pragma omp parallel for schedule(dynamic)
    for(int i = 0; i < N;++i){
        InputDag input_sequence;
        tree_yield(eval_set[i], input_sequence);
        TSSBeam beam(beam_size,grammar);
        vector<unsigned int> x_vec(spencoder.ntemplates(),0);
        vector<float> y_scores(grammar.num_actions(),0.0);
        AbstractParseTree *root = predict_one(input_sequence,beam,x_vec,y_scores);
        if (beam.has_best_parse()){
            //cout << "Ref:" << *eval_set[i] << endl;
            //cout << *root << endl;
            unpack_unaries(root);
            unbinarize(root);
            if(transformed){undecorate(root);}
            eval_res[i] = compare(root,eval_set[i]);
            //cout << "Res :" << *root << endl;
            //cout << "pred:" << *eval_set[i] << endl;
            //cout << " F:" << get<2>(eval_res[i]) << endl;
            destroy(root);
        }else{
            eval_res[i] = make_tuple(0,0,0);
            //cout << "(())"<<endl;
            //cout << *eval_set[i] << endl;
        }
    }
    float p = 0;
    float r = 0;
    float f = 0;
    for(int i = 0; i < N;++i){
        p += get<0>(eval_res[i]);
        r += get<1>(eval_res[i]);
        f += get<2>(eval_res[i]);
    }
    return make_tuple(p/N,r/N,f/N);
}


/////////////////////////////////// TRANSFORMATIONS ////////////////////////////////

void SrParser::transform_dataset(Treebank &train_set,
                                 Treebank &dev_set,
                                 Treebank &train_trans,
                                 Treebank &dev_trans,
                                 Treebank::Decoration const &transform){
    
    train_set.transform(train_trans,spencoder.has_morpho(),0,transform);
    dev_set.transform(dev_trans,spencoder.has_morpho(),0,transform);
    /*
    for(int i=0;i< dev_set.size();++i){
        cout << *dev_trans[i]<<endl;
    }
     */
    this->transformed = (transform != Treebank::NO_DECORATIONS);
    
}



/////////////////////////////////// DEBUG ////////////////////////////////

bool SrParser::minibatch_debug(Treebank const &test_bank,SparseEncoder const &spencoder,size_t batch_size){

    size_t N = test_bank.size();
    int c = 0;
    
    bool no_prob = true;
    
    while(c < N){
        Treebank batch_bank;
        for(int i = c;i < N && i < c+batch_size ;++i){batch_bank.add_tree(clone(test_bank[i]));}
        c+= batch_size;
        
        Treebank detrans(batch_bank);
        detrans.detransform();
        
        SrParser parser(spencoder);
        parser.train_global_model(batch_bank, detrans,Treebank::NO_DECORATIONS,4,50);
        tuple<float,float,float> E = parser.eval_model(batch_bank);
        
        if(get<2>(E) < 1.0){
            
            no_prob = false;
            
            InputDag input_sequence;
            for(int j = 0; j < batch_bank.size();++j){
                input_sequence.clear();
                tree_yield(batch_bank[j], input_sequence);
                TSSBeam beam(parser.beam_size,parser.grammar);
                vector<unsigned int> xvec(parser.spencoder.ntemplates(),0);
                vector<float> y_scores(parser.grammar.num_actions(),0.0);
                AbstractParseTree *root = parser.predict_one(input_sequence,beam,xvec,y_scores);
                if (beam.has_best_parse()){
                    bool perf_parse = (get<2>(compare(root,batch_bank[j])) == 1.0);
                    if (!perf_parse){
                        //cout << "Res:" << *root << endl;
                        //cout << "Ref:" << *batch_bank[j] << endl;
                        ParseDerivation pred(beam.best_success_state());
                        ParseDerivation ref(batch_bank[j],parser.grammar,input_sequence,spencoder.has_morpho());
                        //cout << "Res:" << pred<<endl;
                        //cout << "Ref:" << ref<<endl;
                        //cout << endl;
                    }
                    destroy(root);
                }else {
                    cout << "Res: >>failure<< " << endl;
                    cout << "Ref:" << *batch_bank[j] << endl;
                }
            }
        }
    }
    return no_prob;
}



void SrParser::show_internals(Treebank const &test_bank){
    
    InputDag input_sequence;
    for(int i = 0; i < test_bank.size();++i){
        input_sequence.clear();
        tree_yield(test_bank[i],input_sequence);
        ParseDerivation ref_deriv(test_bank[i],grammar,input_sequence,spencoder.has_morpho());
        
        TSSBeam beam(beam_size,grammar);
        vector<unsigned int> xvec(spencoder.ntemplates(),0);
        vector<float> y_scores(grammar.num_actions(),0.0);

        cout << "----------- Size 1 ------------"<<endl;
        beam.set_size(1);
        AbstractParseTree *res = predict_one(input_sequence,beam,xvec,y_scores);
        beam.dump_beam();
        cout << "pred(N=" << input_sequence.size() << ")" << endl;
        float f1 = 0;
        if(beam.has_best_parse()){
            beam.display_kbest_derivations();
            cout << *res <<endl;
            f1 = get<2>(compare(test_bank[i], res));
            destroy(res);
        }else{
            cout << "** failure **" << endl;
        }
        cout << "ref:"<<endl;
        cout << ref_deriv << endl;
        cout << *test_bank[i]<< endl;
        cout << "F:"<<f1<<endl;
        cout << "----------- Size 2 ------------"<<endl;
        beam.set_size(2);
        res = predict_one(input_sequence,beam,xvec,y_scores);
        beam.dump_beam();
        
        cout << "pred(N=" << input_sequence.size() << ")" << endl;
        float f2 = 0;
        if(beam.has_best_parse()){
            beam.display_kbest_derivations();
            cout << *res <<endl;
            f2 = get<2>(compare(test_bank[i], res));
            destroy(res);
        }else{
            cout << "** failure **" << endl;
        }
        cout << "ref:"<<endl;
        cout << ref_deriv << endl;
        cout << *test_bank[i]<< endl;
        cout << "F:"<<f2<<endl;
    }
}





/////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////
///
/// What follows is about training with dynamic oracle
///
//////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////


struct ExploreEpochSummary : public EpochSummary{
    void display(int pred, int ambiguities){
        cerr << "Iteration "<<epoch_id << " : loss="<<loss<< " eta=" << eta <<" train-F-Score="<<trainF<<" dev-F-Score="<<devF;
        if(loc_acc_train > 0){cerr << " train local acc="<<loc_acc_train<< " dev local acc="<<loc_acc_dev;}
        cerr << " Number of oracle preditions : " << pred << " (" << ambiguities << " ambiguities)";
        cerr << endl;
    }
};


/// @@m
/// Learning with oracles :
///     The following implements Goldberg et Nivre 2013 TACL paper algorithm
///     When a prediction is wrong :
///         follow the gold decision with probability p
///         follow the wrong prediction with probability (1-p)
///     If the derivation is not gold, trains against the oracle instead of the gold sequence
void SrParser::train_local_model_exploration(Treebank &train_set,
                                 Treebank &dev_set,
                                 Treebank::Decoration const &transformation,
                                 size_t beam_size,
                                 size_t epochs,
                                 int k,
                                 double exploration_p,
                                 bool verbose)
{
    Treebank binarytrain_set;
    Treebank binarydev_set;
    transform_dataset(train_set,dev_set,binarytrain_set,binarydev_set,transformation);

    binarytrain_set.update_encoder();
    binarydev_set.update_encoder();

    grammar = SrGrammar(binarytrain_set,binarydev_set,spencoder.has_morpho());
    cerr << "Number of actions : " << grammar.num_actions() << endl;
    DynamicOracle oracle(grammar);
    this->beam_size = beam_size;

    Treebank binarytrain_sample;
    binarytrain_set.sample_trees(binarytrain_sample,dev_set.size());
    Treebank train_sample(binarytrain_sample);
    train_sample.detransform(transformation);

    delete parsing_model;
    parsing_model = new SparseFloatMatrix(KERNEL_SIZE,grammar.num_actions());
    SparseFloatMatrix *averaged_model = new SparseFloatMatrix(KERNEL_SIZE,grammar.num_actions());

    //setup stats
    ExploreEpochSummary summary;

    float C = 1;
    for(unsigned int e = 1; e < epochs+1;++e){
        summary.init();
        summary.epoch_id = e;

        oracle.n_non_determinism = 0;
        oracle.n_predictions = 0;

        if (e > k){
            for(int i = 0; i < train_set.size();++i){
                summary.loss += local_learn_one_exploration(binarytrain_set[i],*averaged_model,C, oracle, exploration_p);
                C++;
            }
        }else{
            for(int i = 0; i < train_set.size();++i){
                summary.loss += local_learn_one(binarytrain_set[i],*averaged_model,C);
                C++;
            }
        }
        //Logging
        tuple<float,float,float> evalT = eval_model(train_sample);
        summary.trainF = std::get<2>(evalT);
        tuple<float,float,float> evalD = eval_model(dev_set);
        summary.devF = std::get<2>(evalD);
        summary.loc_acc_train = eval_local_model(binarytrain_sample);
        summary.loc_acc_dev   = eval_local_model(binarydev_set);
        summary.eta = 1.0;

        LLOGGER_SET(LearnLogger::EPOCH,e);
        LLOGGER_SET_TRAIN(std::get<0>(evalT),std::get<1>(evalT),std::get<2>(evalT));
        LLOGGER_SET_DEV(std::get<0>(evalD),std::get<1>(evalD),std::get<2>(evalD));
        LLOGGER_SET(LearnLogger::LOSS,summary.loss);
        LLOGGER_WRITE();

        if (verbose){
            summary.display(oracle.n_predictions, oracle.n_non_determinism);
        }

    }
    //Final averaging
    (*averaged_model) *= 1.0/C;
    (*parsing_model) -= (*averaged_model);
    delete averaged_model;
}


int _argmax(vector<float> &scores,size_t Vs, vector<bool> &oracle){
    float mmax = - std::numeric_limits<float>::infinity();
    int amax = -1;

    for(int j = 0; j <Vs ;++j){
        if(scores[j] >= mmax && oracle[j]){
            mmax = scores[j];
            amax = j;
        }
    }
    return amax;
}

int SrParser::local_learn_one_exploration(AbstractParseTree const *root,SparseFloatMatrix &averaged_model,float C, DynamicOracle &oracle, double p){

    InputDag input_sequence;
    tree_yield(root,input_sequence);
    ParseDerivation deriv(root,grammar,input_sequence,spencoder.has_morpho());
    size_t N = input_sequence.size();

    ConstituentSet gold_set(deriv, grammar);
#ifdef DEBUG_ORACLE
    cerr << endl << *root << endl;
    cerr << endl << "gold set = " << gold_set << endl;
#endif

    StateSignature sig;
    int nupdates = 0;

    vector<unsigned int> xvec(spencoder.ntemplates(),0);
    vector<float> y_scores(grammar.num_actions(),0.0);
    vector<bool> y_oracle(grammar.num_actions(),false);
    vector<bool> allowed_actions(grammar.num_actions(),false);
    const float MINUS_INFINITY = - std::numeric_limits<float>::infinity();

    bool gold = true;

    TSSBeam beam(1, grammar);
    for(int i = 0; i < deriv.size()-1; ++i){
        if (beam.top_size() != 1){ cerr << "Exploration learning failed " << endl; return 0;}
        ParseState *s0 = beam[0];
        s0->get_signature(sig,&input_sequence,N);
        s0->encode(xvec,spencoder,sig);
        parsing_model->dot(xvec,y_scores);
        grammar.select_actions(allowed_actions,s0->get_incoming_action(),sig);
        for (int i = 0; i < allowed_actions.size(); i++){
            if (!allowed_actions[i])
                y_scores[i] = MINUS_INFINITY;
        }
        unsigned int argmax = _argmax(y_scores,y_scores.size());
        bool update = false;
        if (gold){
            update = grammar[argmax] != deriv[i+1]->get_incoming_action();
        }else{
            //@BC if (! oracle(s0, gold_set, y_oracle, allowed_actions))     // predict 0-cost transitions
            //@BC    cerr << "There is a problem in dynamic oracle, make sure there is no tagging and a unique temporary symbol" << endl;
            update = ! y_oracle.at(argmax);
        }
        if (update){
            int best = 0;
            if (gold){
                best = grammar.get_action_index(deriv[i+1]->get_incoming_action());
             }else{
                best = _argmax(y_scores, y_scores.size(), y_oracle);
                assert(best != -1);
            }
            SparseFloatVector lxvec(xvec,KERNEL_SIZE);

            parsing_model->subs_rowY(lxvec,argmax);
            //parsing_model->add_rowY(lxvec,grammar.get_action_index(deriv[i+1]->get_incoming_action()));
            parsing_model->add_rowY(lxvec, best);

            lxvec *= C;
            averaged_model.subs_rowY(lxvec,argmax);
            //averaged_model.add_rowY(lxvec,grammar.get_action_index(deriv[i+1]->get_incoming_action()));
            averaged_model.add_rowY(lxvec, best);

            ++nupdates;
            double s = (double)rand() / (double)RAND_MAX;
            if (s < p){
                gold = false;       // Let's explore !
            }else{
                assert(! isinf(y_scores[best]));
                y_scores[best] = y_scores[argmax] + 1;       // Let's stay on the righteous path !
            }
        }
        beam.push_candidates(s0, y_scores);
        beam.next_step(grammar,input_sequence,N);

    }
    return nupdates;
}


void SrParser::generate_dataset(Treebank &train_set, double p, vector<vector<unsigned int>> &X, vector<vector<bool>> &Y, vector<vector<bool>> &G, DynamicOracle &oracle){
    for (int i = 0; i < train_set.size(); i++){
        generate_dataset_one(train_set[i], p, X, Y, G, oracle);
    }
}

void SrParser::generate_dataset_one(AbstractParseTree const *root, double p, vector<vector<unsigned int>> &X, vector<vector<bool>> &Y, vector<vector<bool>> &G, DynamicOracle &oracle){

    InputDag input_sequence;
    tree_yield(root,input_sequence);
    ParseDerivation deriv(root,grammar,input_sequence,spencoder.has_morpho());
    size_t N = input_sequence.size();

    ConstituentSet gold_set(deriv, grammar);

    StateSignature sig;

    vector<unsigned int> xvec(spencoder.ntemplates(),0);
    vector<float> y_scores(grammar.num_actions(),0.0);
    vector<bool> y_oracle(grammar.num_actions(),false);
    vector<bool> y_g(grammar.num_actions(),false);

    const float MINUS_INFINITY = - std::numeric_limits<float>::infinity();

    bool gold = true;

    TSSBeam beam(1, grammar);
    for(int i = 0; i < deriv.size()-1; ++i){
        if (beam.top_size() != 1){ cerr << "Exploration learning failed " << endl; }

        std::fill(y_g.begin(), y_g.end(), false);
        std::fill(y_oracle.begin(), y_oracle.end(), false);

        ParseState *s0 = beam[0];
        s0->get_signature(sig,&input_sequence,N);
        s0->encode(xvec,spencoder,sig);

        X.push_back(xvec);

        parsing_model->dot(xvec,y_scores);
        grammar.select_actions(y_g,s0->get_incoming_action(),sig);
        for (int i2 = 0; i2 < y_g.size(); i2++){
            if (! y_g[i2]){
                y_scores[i2] = MINUS_INFINITY;
            }
        }
        G.push_back(y_g);

        unsigned int argmax = _argmax(y_scores,y_scores.size());
        unsigned int best;

        bool fail;

        if (gold){
            best = grammar.get_action_index(deriv[i+1]->get_incoming_action());
            y_oracle[best] = true;
            fail = argmax != best;
        }else{
            //@BC oracle(s0, gold_set, y_oracle, y_g);
            best = _argmax(y_scores, y_scores.size(), y_oracle);
            fail = ! y_oracle[best];
        }

        Y.push_back(y_oracle);

        if (fail){
            double s = (double)rand() / (double)RAND_MAX;
            if (s < p){
                gold = false;
            }else{
                y_scores[best] = y_scores[argmax] + 1;       // Let's stay on the righteous path !
            }
        }

        beam.push_candidates(s0, y_scores);
        beam.next_step(grammar,input_sequence,N);
    }
}

struct DaggerEpochSummary{
    int epoch_id;
    float trainF;
    float devF;
    float loc_acc_train;
    float loc_acc_dev;
    int datasize;
    void init(){
        trainF = 0;
        devF = 0;
        loc_acc_train = 0;
        loc_acc_dev   = 0;
        datasize = 0;
    }
    void display(){
        cerr << "Dagger epoch " << epoch_id << ": data size=" << datasize
                                            <<  " train-F-Score="<<trainF<<" dev-F-Score="<<devF
                                            <<  " train local acc="<<loc_acc_train<< " dev local acc="<<loc_acc_dev << endl << endl;
    }
};

void SrParser::train_dagger_model(Treebank &train_set, Treebank &dev_set, Treebank::Decoration const &transformation, size_t beam_size, size_t epochs, int dagger_epoch, double p, bool verbose){
    Treebank binarytrain_set;
    Treebank binarydev_set;
    transform_dataset(train_set,dev_set,binarytrain_set,binarydev_set,transformation);

    binarytrain_set.update_encoder();
    binarydev_set.update_encoder();

    grammar = SrGrammar(binarytrain_set,binarydev_set,spencoder.has_morpho());
    DynamicOracle oracle(grammar);
    this->beam_size = beam_size;

    Treebank binarytrain_sample;
    binarytrain_set.sample_trees(binarytrain_sample,dev_set.size());
    Treebank train_sample(binarytrain_sample);
    train_sample.detransform(transformation);

    vector<vector<unsigned int> >X;
    vector<vector<bool>> Y;
    vector<vector<bool>> G;



    for (int epoch = 0; epoch < dagger_epoch; epoch ++){
        DaggerEpochSummary dSummary;
        dSummary.init();
        dSummary.epoch_id = epoch;

        if (epoch == 0) generate_dataset(binarytrain_set, -1.0, X, Y, G, oracle);
        else            generate_dataset(binarytrain_set, p, X, Y, G, oracle);
        dSummary.datasize = X.size();

        delete parsing_model;
        parsing_model = new SparseFloatMatrix(KERNEL_SIZE,grammar.num_actions());
        SparseFloatMatrix *averaged_model = new SparseFloatMatrix(KERNEL_SIZE,grammar.num_actions());

        float C = 1;
        for(unsigned int e = 1; e < epochs+1;++e){

            EpochSummary summary;
            summary.init();
            summary.epoch_id = e;
            summary.loss = perceptron_learn_dataset(X,Y,G, *averaged_model, C);

            //Logging
            tuple<float,float,float> evalT = eval_model(train_sample);
            summary.trainF = std::get<2>(evalT);
            tuple<float,float,float> evalD = eval_model(dev_set);
            summary.devF = std::get<2>(evalD);
            summary.loc_acc_train = eval_local_model(binarytrain_sample);
            summary.loc_acc_dev   = eval_local_model(binarydev_set);
            summary.eta = 1.0;

            LLOGGER_SET(LearnLogger::EPOCH,e);
            LLOGGER_SET_TRAIN(std::get<0>(evalT),std::get<1>(evalT),std::get<2>(evalT));
            LLOGGER_SET_DEV(std::get<0>(evalD),std::get<1>(evalD),std::get<2>(evalD));
            LLOGGER_SET(LearnLogger::LOSS,summary.loss);
            LLOGGER_WRITE();
            if (verbose){summary.display();}

        }
        //Final averaging
        (*averaged_model) *= 1.0/C;
        (*parsing_model) -= (*averaged_model);

        //Logging
        tuple<float,float,float> evalT = eval_model(train_sample);
        dSummary.trainF = std::get<2>(evalT);
        tuple<float,float,float> evalD = eval_model(dev_set);
        dSummary.devF = std::get<2>(evalD);
        dSummary.loc_acc_train = eval_local_model(binarytrain_sample);
        dSummary.loc_acc_dev   = eval_local_model(binarydev_set);
        dSummary.display();

        delete averaged_model;

        //epochs = (epochs * 9) / 10;
        //if (epochs < 3) epochs = 3;
    }
}

int SrParser::perceptron_learn_dataset(vector<vector<unsigned int>> &X, vector<vector<bool>> &Y, vector<vector<bool>> &G, SparseFloatMatrix &averaged_model, float &C){
    assert(X.size() == Y.size() && X.size() == G.size());
    int nupdates = 0;
    const float MINUS_INFINITY = - std::numeric_limits<float>::infinity();
    int n = grammar.num_actions();
    vector<float> y_scores(n, 0.0);
    int argmax;
    int best;


    for (int i = 0; i < X.size(); i++){
        parsing_model->dot(X[i],y_scores);

        for (int j = 0; j < n; j++){
            if (! G[i][j]){
                y_scores[j] = MINUS_INFINITY;
            }
        }
        argmax = _argmax(y_scores, y_scores.size());
        if (! Y[i][argmax]){
            best = _argmax(y_scores, y_scores.size(), Y[i]);

            SparseFloatVector lxvec(X[i],KERNEL_SIZE);

            parsing_model->subs_rowY(lxvec,argmax);
            parsing_model->add_rowY(lxvec, best);

            lxvec *= C;
            averaged_model.subs_rowY(lxvec,argmax);
            averaged_model.add_rowY(lxvec, best);

            ++nupdates;
            ++C;
        }
    }
    return nupdates;
}
