#ifndef BOOSTING_HPP
#define BOOSTING_HPP
#ifdef ENABLE_PARALLEL
#include <omp.h>
#endif



/***************************************
 * WEAK LEARNER *
 ***************************************/


template <class Y>
WeakLearner<Y>::WeakLearner(string const &description,vector<Y> const &y_list,
                            int num){
    this->description = description;
    ylabels = Bimap<Y>(y_list);
    parsing_model = new SparseFloatMatrix(KERNEL_SIZE,ylabels.nlabels());
    wl_number = num; // index number
}

template <class Y>
WeakLearner<Y>::~WeakLearner(){
    delete parsing_model;
}

template <class Y>
WeakLearner<Y>::WeakLearner(WeakLearner<Y> const &other){
    
    ylabels = other.ylabels;
    *parsing_model = *(other.parsing_model);//aargh terribly heavy op)
    description = other.description;
 
    for(int i = 0; i < other.xvec_train.size();++i){
        xvec_train.push_back(other.xvec_train[i]);
    }
    for(int i = 0; i < other.xvec_dev.size();++i){
        xvec_dev.push_back(other.xvec_dev[i]);
    }
    for(int i = 0; i < other.train_constraints.size();++i){
        train_constraints.push_back(other.train_constraints[i]);
    }
    for(int i = 0; i < other.dev_constraints.size();++i){
        dev_constraints.push_back(other.dev_constraints[i]);
    }
    
    ys_train          = other.ys_train;
    ys_dev            = other.ys_dev;
    predictions_train = other.predictions_train;
    predictions_dev   = other.predictions_dev;
    werr              = other.werr;
    wl_number         = other.wl_number;
}




template <class Y>
WeakLearner<Y>& WeakLearner<Y>::operator=(WeakLearner<Y> const &other){
    
    ylabels = other.ylabels;
    *parsing_model = *(other.parsing_model);//aargh terribly heavy op)
    description = other.description;
    
    xvec_train.clear();
    xvec_dev.clear();
    train_constraints.clear();
    dev_constraints.clear();

    for(int i = 0; i < other.xvec_train.size();++i){
        xvec_train.push_back(other.xvec_train[i]);
    }
    for(int i = 0; i < other.xvec_dev.size();++i){
        xvec_dev.push_back(other.xvec_dev[i]);
    }
    for(int i = 0; i < other.train_constraints.size();++i){
        train_constraints.push_back(other.train_constraints[i]);
    }
    for(int i = 0; i < other.dev_constraints.size();++i){
        dev_constraints.push_back(other.dev_constraints[i]);
    }

    ys_train          = other.ys_train;
    ys_dev            = other.ys_dev;
    predictions_train = other.predictions_train;
    predictions_dev   = other.predictions_dev;
    werr              = other.werr;
    wl_number         = other.wl_number;
    return *this;
}


template <class Y>
float WeakLearner<Y>::get_error(bool train, vector<double> weights)const{

    if (train){
        assert(weights.size() == predictions_train.size());
        float werr = 0;
        for(int i = 0; i < predictions_train.size(); ++i){
            if(predictions_train[i] != ys_train[i]){
                werr+= weights[i];                
            }
        }
        return werr/std::accumulate(weights.begin(),weights.end(),0.0);
    }else{
        assert(weights.size() == predictions_dev.size());
        float werr = 0;
        for(int i = 0; i < predictions_dev.size();++i){
            if(predictions_dev[i] != ys_dev[i]){
                werr+= weights[i];
            }
        }
        return werr/std::accumulate(weights.begin(),weights.end(),0.0);
    }
}
template <class Y>
float WeakLearner<Y>::get_error(bool train)const{
    
    if(train){
        vector<double> w(ys_train.size(),1.0);
        return get_error(train, w);
    }else{
        vector<double> w(ys_dev.size(),1.0);
        return get_error(train, w);
    }
}

template <class Y>
void WeakLearner<Y>::add_train_datapoint(vector<XSP> const &xvals, Y ylbl){
    xvec_train.push_back(xvals);
    ys_train.push_back(ylbl);
    train_constraints.push_back(vector<float>());
}

template <class Y>
void WeakLearner<Y>::add_dev_datapoint(vector<XSP> const &xvals, Y ylbl){
    xvec_dev.push_back(xvals);
    ys_dev.push_back(ylbl);
    dev_constraints.push_back(vector<float>());
}



template <class Y>
void WeakLearner<Y>::add_train_datapoint(vector<XSP> const &xvals, Y ylbl,vector<float> const &constraints){
    xvec_train.push_back(xvals);
    ys_train.push_back(ylbl);
    train_constraints.push_back(constraints);
}

template <class Y>
void WeakLearner<Y>::add_dev_datapoint(vector<XSP> const &xvals, Y ylbl,vector<float> const &constraints){
    xvec_dev.push_back(xvals);
    ys_dev.push_back(ylbl);
    dev_constraints.push_back(constraints);
}



template <class Y>
void WeakLearner<Y>::apply_constraints(vector<float> &predictions,vector<float> &constraints){
    if (constraints.size()==0){return;}
    assert(predictions.size()==constraints.size());
    for(int i = 0; i < constraints.size();++i){
        predictions[i]+= constraints[i];
    }
}



template <class Y>
void WeakLearner<Y>::train(unsigned int nepochs,
                           vector<unsigned int> &samp_idces){

    SparseFloatMatrix *averaged_model = new SparseFloatMatrix(KERNEL_SIZE,ylabels.nlabels());
    float C = 1;
    for (int E = 0; E < nepochs; ++E){
        for(int j = 0; j < samp_idces.size();++j){
            unsigned int idx = samp_idces[j];
            Y yval = ys_train[idx];
            vector<float> y_eta(ylabels.nlabels(),0.0);

            parsing_model->dot(xvec_train[idx],y_eta);
            apply_constraints(y_eta,train_constraints[idx]);
            unsigned int argmax = _argmax(y_eta,ylabels.nlabels());
            if(ylabels.get_label(argmax) != yval && xvec_train[idx].size() > 0){
                //update
                SparseFloatVector lxvec(xvec_train[idx],KERNEL_SIZE);
                parsing_model->subs_rowY(lxvec,argmax);
                parsing_model->add_rowY(lxvec,ylabels.get_index(yval));
                //averaging
                lxvec *= C;
                averaged_model->subs_rowY(lxvec,argmax);
                averaged_model->add_rowY(lxvec,ylabels.get_index(yval));
            }
            C++;
        }
    }
    //swaps models (keep averaged only)
    (*averaged_model) *= 1.0/C;
    (*parsing_model) -= (*averaged_model);
    delete averaged_model;
}

template <class Y>
void WeakLearner<Y>::predict(bool train){

    if(train){
        predictions_train.clear();
        for(int i = 0; i < xvec_train.size();++i){
            vector<float> y_eta(ylabels.nlabels(),0.0);
            parsing_model->dot(xvec_train[i],y_eta);
            apply_constraints(y_eta,train_constraints[i]);
            unsigned int argmax = _argmax(y_eta,ylabels.nlabels());
            predictions_train.push_back(ylabels.get_label(argmax));
        }
    }else{
        predictions_dev.clear();
        for(int i = 0; i < xvec_dev.size();++i){
            vector<float> y_eta(ylabels.nlabels(),0.0);
            parsing_model->dot(xvec_dev[i],y_eta);
            apply_constraints(y_eta,dev_constraints[i]);
            unsigned int argmax = _argmax(y_eta,ylabels.nlabels());
            predictions_dev.push_back(ylabels.get_label(argmax));
        }
    }
}
template <class Y>
Y& WeakLearner<Y>::prediction_at(EXIDX idx, bool train) {
    
    if(train){
        assert(idx < predictions_train.size());
        return predictions_train[idx];
    }else{
        assert(idx < predictions_dev.size());
        return predictions_dev[idx];
    }
    
}

template <class Y>
bool WeakLearner<Y>::is_correct_at(EXIDX idx, bool train)const{
    
    if(train){
        assert(idx < ys_train.size());
        return ys_train[idx] == predictions_train[idx];
    }else{
        assert(idx < ys_dev.size());
        return ys_dev[idx] == predictions_dev[idx];
    }
}

template <class Y>
string WeakLearner<Y>::describe()const{
    return description;
}


//===============================================================
template <class Y,class X>
StrongLearner<Y,X>::StrongLearner(vector<Y> const &action_list,
                                unsigned int nexamples, vector<Y> train_ys,
                                vector<Y> dev_ys){
    ylabels = Bimap<Y>(action_list);
    initialise_weights(nexamples);
    train_gold_ys = train_ys;
    dev_gold_ys = dev_ys;
}

template <class Y,class X>
StrongLearner<Y,X>::StrongLearner(StrongLearner<Y,X> const &other){

  ylabels           = other.ylabels;
  weights           = other.weights;
  nclasses          = other.nclasses;
  alphas            = other.alphas;
  boosted_acc_train = other.boosted_acc_train;
  boosted_acc_dev   = other.boosted_acc_dev;
  exp_loss_train    = other.exp_loss_train;
  exp_loss_dev      = other.exp_loss_dev;
  train_gold_ys     = other.train_gold_ys;
  dev_gold_ys       = other.dev_gold_ys;
  last_iteration_duration = other.last_iteration_duration;
    
  for(int i = 0; i < other.train_predictions.size();++i){
    train_predictions[i] = other.train_predictions[i];
   }
  for(int i = 0; i < other.dev_predictions.size();++i){
    dev_predictions[i] = other.dev_predictions[i];
   }
}

template <class Y,class X>
StrongLearner<Y,X>& StrongLearner<Y,X>::operator=(StrongLearner<Y,X> const &other){
    
    ylabels           = other.ylabels;
    weights           = other.weights;
    nclasses          = other.nclasses;
    alphas            = other.alphas;
    boosted_acc_train = other.boosted_acc_train;
    boosted_acc_dev   = other.boosted_acc_dev;
    exp_loss_train    = other.exp_loss_train;
    exp_loss_dev      = other.exp_loss_dev;
    train_gold_ys     = other.train_gold_ys;
    dev_gold_ys       = other.dev_gold_ys;
    last_iteration_duration = other.last_iteration_duration;
    
    train_predictions.clear();
    dev_predictions.clear();
    for(int i = 0; i < other.train_predictions.size();++i){
        train_predictions[i] = other.train_predictions[i];
    }
    for(int i = 0; i < other.dev_predictions.size();++i){
        dev_predictions[i] = other.dev_predictions[i];
    }
    return *this;
}



template <class Y,class X>
void StrongLearner<Y,X>::initialise_weights(int nexamples){
    for (int i=0; i<nexamples; ++i) {
        weights.push_back(1.0/nexamples);
    }
}



template <class Y,class X>
void StrongLearner<Y,X>::add_weak_learner(X &data_set,WeakLearner<Y>* wl,
                                          bool remove){

    data_set.gen_examples(wl, false); // generate dev data
    wl->predict(false); // make predictions on dev

    //initializes predictions matrices if empty
    if (train_predictions.size() == 0){
        train_predictions.resize(wl->train_size(),vector<Y>());
    }
    if (dev_predictions.size() == 0){
        dev_predictions.resize(wl->dev_size(),vector<Y>());
    }
    assert(wl->train_size() == train_predictions.size());
    assert(wl->dev_size() == dev_predictions.size());

    //train predictions
    for(int i = 0; i < wl->train_size();++i){
        train_predictions[i].push_back(wl->prediction_at(i,true));
    }
    //dev predictions
    for(int i = 0; i < wl->dev_size();++i){
        dev_predictions[i].push_back(wl->prediction_at(i,false));
    }

    // calculate alpha
    float werr_t  = wl->get_error(true,weights);
    float alpha_t = log((1-werr_t)/werr_t)+log(ylabels.nlabels()-1);
    alphas.push_back(alpha_t);

    // calculate boosted predictions
    calculate_strong_prediction(true);
    calculate_strong_prediction(false);

    //recalculate_weights(wl); // use alpha to recalculate distribution

    // update learner pool (ParseData and best_weak_learner)
    //lp->update(wl, remove); //TODO
    //
    //if(remove){
    //    data_set.remove_learner( *wl );
    //}
    //delete wl;
}

template <class Y,class X>
void StrongLearner<Y,X>::display_highest_weighted_labels(unsigned int K){
    
    vector<tuple<int,double>> sorted_examples;
    for(int i = 0; i  < weights.size();++i){sorted_examples.push_back(make_tuple(i,weights[i]));}
    std::sort(sorted_examples.begin(),sorted_examples.end(),[](tuple<int,double> const &t1,tuple<int,double> const &t2){return get<1>(t1) > get<1>(t2);});
    for(int i = 0; i  < sorted_examples.size() && i < K ;++i){
            cout << get<0>(sorted_examples[i]) << " : "
                 << to_string(get_strong_prediction(get<0>(sorted_examples[i]),true)) << " / "
                 << to_string(train_gold_ys[get<0>(sorted_examples[i])])
                 <<"("<< get<1>(sorted_examples[i]) <<")"<<endl;
    }
}

template <class Y,class X>
double StrongLearner<Y,X>::weights_entropy()const{
    double H = 0;
    for(int i = 0; i < weights.size();++i){
        H += weights[i] * log(weights[i]);
    }
    return -H;
}

template <class Y,class X>
void StrongLearner<Y, X>::display_k_worst_classed_labels(bool train,
                                                         unsigned int k) {
    std::map<string, unsigned int> classes2nbr;
    string classes;

    assert(train_predictions.size()==train_gold_ys.size());
    assert(dev_predictions.size()==dev_gold_ys.size());

    unsigned int sum_correct=0;
    if (train) {
        for (int i=0; i < train_predictions.size(); ++i){
            if (train_gold_ys[i]==get_strong_prediction(i, train)) {
                sum_correct++;
            }
            else {
                classes = to_string(train_gold_ys[i])+"__"
                        +to_string(get_strong_prediction(i, train));
                classes2nbr[classes]++;
            }
        }
    }
    else {
        for (int i=0; i < dev_predictions.size(); ++i){
            if (dev_gold_ys[i]==get_strong_prediction(i, train)) {
                sum_correct++;
            }
            else {
                classes = to_string(dev_gold_ys[i])+"__"+to_string(get_strong_prediction(i, train));
                classes2nbr[classes]++;
            }
        }
    }
    vector<string> labels;
    vector<unsigned int> nbrincorrect;
    vector<unsigned int> indices;
    int j=0;
    for (auto ilabel: classes2nbr) {
        indices.push_back(j);
        labels.push_back(ilabel.first);
        nbrincorrect.push_back(ilabel.second);
        j++;
    }
    // sort
    std::sort(std::begin(indices),std::end(indices),
              [&](int i1, int i2) { return nbrincorrect[i1] > nbrincorrect[i2]; }
    );

    // print out the k worst classes labels
    cerr << "****" << to_string(k) << " worst classed labels ****" << endl;
    for (int i=0; i<indices.size(); ++i) {
        if (i<k) {
            cerr << "\t" << labels[indices[i]] << "\t = " << nbrincorrect[indices[i]] << endl;
        }
    }
}

template <class Y,class X>
double StrongLearner<Y,X>::get_biggest_weight() {
    double biggest=0.0;
    for (int i=0; i<weights.size(); ++i){
        if (weights[i]>biggest)
            biggest=weights[i];
    }
    return biggest;
}

template <class Y,class X>
const Y& StrongLearner<Y,X>::get_weak_prediction(EXIDX ex_idx,WLIDX weak_learner_idx,bool train)const{
    if(train){
        assert(ex_idx < train_predictions.size());
        assert(weak_learner_idx < train_predictions[ex_idx].size());
        return train_predictions[ex_idx][weak_learner_idx];
    }else{
        assert(ex_idx < dev_predictions.size());
        assert(weak_learner_idx < dev_predictions[ex_idx].size());
        return dev_predictions[ex_idx][weak_learner_idx];
    }
}

template <class Y,class X>
const Y& StrongLearner<Y,X>::get_strong_prediction(EXIDX ex_idx, bool train)const{

    if(train){
        
        assert(ex_idx < train_predictions.size());
        assert(alphas.size() == train_predictions[ex_idx].size());
        vector<float> eta(ylabels.nlabels(),0.0);
        //inner product for each label
        for(int i = 0; i < alphas.size();++i){
            unsigned int yidx = ylabels.get_index(train_predictions[ex_idx][i]);
            eta[yidx] += alphas[i];
        }
        return ylabels.get_label(_argmax(eta,ylabels.nlabels()));
        
    }else{
        
        assert(ex_idx < dev_predictions.size());
        assert(alphas.size() == dev_predictions[ex_idx].size());
        vector<float> eta(ylabels.nlabels(),0.0);
        //inner product for each label
        for(int i = 0; i < alphas.size();++i){
            unsigned int yidx = ylabels.get_index(dev_predictions[ex_idx][i]);
            eta[yidx] += alphas[i];
        }
        return ylabels.get_label(_argmax(eta,ylabels.nlabels()));
    }
}

template <class Y,class X>
void StrongLearner<Y,X>::calculate_strong_prediction(bool train) {
    assert(train_predictions.size()==train_gold_ys.size());
    assert(dev_predictions.size()==dev_gold_ys.size());

    unsigned int sum_correct=0;
    if (train) {
        for (int i=0; i < train_predictions.size(); ++i){
            if (train_gold_ys[i]==get_strong_prediction(i, train)) {
                sum_correct++;
            }
        }
        boosted_acc_train = sum_correct/(float)train_predictions.size();
    }
    else {
        for (int i=0; i < dev_predictions.size(); ++i){
            if (dev_gold_ys[i]==get_strong_prediction(i, train)) {
                sum_correct++;
            }
        }
        boosted_acc_dev = sum_correct/(float)dev_predictions.size();
    }
}

template <class Y,class X>
void StrongLearner<Y,X>::recalculate_weights(WeakLearner<Y>* wl){
    
    double cumul=0;
    for(int i = 0; i < weights.size();++i){
        weights[i] *= exp(alphas.back() * !wl->is_correct_at(i,true));
        cumul+=weights[i];
    }

    // normalise
    for(int i = 0; i < weights.size();++i){
        weights[i]/=cumul;
    }
}


template <class Y,class X>
WeakLearner<Y>* StrongLearner<Y,X>::select_weak_learner(X &data_set,
                                    unsigned int sample_size,
                                    unsigned int perc_iter) {

//return data_set.get_next_wl(); // debug

    time_t ctime = time(0);
    vector<unsigned int> sidxs = sample_data(weights, sample_size);

    WeakLearner<Y> *max_learner;
    if(data_set.has_next_wl()){
        max_learner = data_set.get_next_wl();
        max_learner->train(perc_iter,sidxs);
        max_learner->predict(true);
        clog << "1 of " << data_set.get_num_accessible_learners() << "\t";
        //cout << max_learner->describe() << " :: werr = "<< max_learner->get_error(true,weights)<<endl;
    }else{
        cerr << "Can't access any weak learners. (aborting)"<<endl;
        exit(1);
    }

    int num=1;
    #pragma omp parallel for
    for (int i=1; i<data_set.get_num_accessible_learners(); ++i) {
        WeakLearner<Y> *current = data_set.get_next_wl();
        current->train(perc_iter,sidxs);
        current->predict(true);
        #pragma omp critical
        {
            num++;
            clog << "\r"+to_string(num)+" of "
                 << data_set.get_num_accessible_learners() << "\t";
            //cout << current->describe() << " :: werr = "<< current->get_error(true,weights)<<endl;
            if(current->get_error(true, weights) < max_learner->get_error(true, weights)){
                delete max_learner;
                max_learner = current;
            }else{
                delete current;
            }
        }
    }
    clog << endl;

    /*int num=0;
        #pragma omp parallel
        #pragma omp single
        {
            int num_procs = 1;
            #ifdef ENABLE_PARALLEL
                num_procs = omp_get_num_threads();
            #endif
            if(num_procs > 1){
                //for (int i=1; i<data_set.get_num_accessible_learners(); ++i) {
                while ( data_set.has_next_wl()){
                    WeakLearner<Y> *current = data_set.get_next_wl();
                    #pragma omp task
                    {
                        current->train(perc_iter,sidxs);
                        current->predict(true);
                        #pragma omp critical
                        {
                            num++;
                            //clog << "\r"+to_string(num)+" of "
                            //     << data_set.get_num_accessible_learners()+"\t";
                            cout << current->describe() << " :: werr = "<< current->get_error(true,weights)<<endl;
                            if(current->get_error(true, weights) < max_learner->get_error(true, weights)){
                                delete max_learner;
                                max_learner = current;
                            }else{
                                delete current;
                            }
                        }
                    }
                }
            }else{
                while ( data_set.has_next_wl()){
                    WeakLearner<Y> *current = data_set.get_next_wl();
                    current->train(perc_iter,sidxs);
                    current->predict(true);
                    num++;
                    clog << "\r"+to_string(num+1)+" of "
                         << data_set.get_num_accessible_learners();
                    //cout << current->describe() << " :: werr = "<< current->get_error(true,weights)<<endl;
                    if(current->get_error(true, weights) < max_learner->get_error(true, weights)){
                        delete max_learner;
                        max_learner = current;
                    }else{
                        delete current;
                    }
                }
            }
        }*/
        last_iteration_duration = time(0) - ctime; //this timing is incorrect for parallel scenario
        return max_learner;
}



///////////////////////////////////////////////////////////////

#endif
