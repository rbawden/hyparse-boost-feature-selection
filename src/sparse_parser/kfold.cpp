#include "globals.h"
#include "utilities.h"
#include "srparser.h"
#include "treebank.h"
#include "sparse_encoder.h"
#include <getopt.h>
#include <iostream>
#include <fstream>
#include <sys/stat.h>

#ifdef ENABLE_PARALLEL
  #include "omp.h"
#endif



enum MODEL_LOCALITY{GLOBAL,LOCAL};
string train_path;
string dev_path;
string test_path;
string tpl_path;
string model_path;
string kfold_path;

int beam = 4;
int perceptron_iterations = 35;
int num_procs = 1;
int batch_size = 1;
float lambda = 0.001;//pegasos regularizer
float C = 0.01; //MIRA bound

MODEL_LOCALITY mlocality = GLOBAL;
SrParser::UpdateType uptype = SrParser::EARLY_UP;
SrParser::ModelType modtype = SrParser::PERCEPTRON;
Treebank::Decoration transformation = Treebank::NO_DECORATIONS;


int main(int argc,char *argv[]){

  srand(1);
  
  int c;
  //defaults
  tpl_path   = "default.tpl";
  model_path = "default.model";
 

  while(true){
    static struct option long_options[] ={
      {"help",no_argument,0,'h'},
      {"iterations",required_argument,0,'i'},
      {"batch-size",required_argument,0,'B'},
      {"templates-filename",required_argument,0,'t'},
      {"model-name",required_argument,0,'m'},
      {"beam-size",required_argument,0,'b'},
      {"processors",required_argument,0,'p'},
      {"local-model",no_argument,0,'l'},
      {"max-Violation",no_argument,0,'V'},
      {"pegasos",required_argument,0,'P'},
      {"mira",required_argument,0,'M'},
      {"transformation",required_argument,0,'T'}
    };
    int option_index = 0;
    c = getopt_long (argc, argv, "m:i:ht:b:B:lVp:M:P:",long_options, &option_index);
    if(c==-1){break;}
    switch(c){
    case 'i':
      perceptron_iterations = atoi(optarg);
      break;
    case 't':
      tpl_path = string(optarg);
      break;
    case 'm':
      model_path = string(optarg);
      break;
    case 'p':
      num_procs = atoi(optarg);
      break;
    case 'b':
      beam = atoi(optarg);
      break;
    case 'B':
      batch_size = atoi(optarg);
      break;
    case 'P':
     lambda = atof(optarg);
     modtype = SrParser::PEGASOS;
     break;
    case 'M':
     C = atof(optarg);
     modtype = SrParser::MIRA;
     break;
    case 'l':
      mlocality = LOCAL;
      break;
    case 'V':
      uptype = SrParser::MAXV_UP;
      break;
    case 'h':
      //display_help_message();
      exit(0);
    }
  }
  if (optind == 1){exit(0);}//abort if no arg at all
  if (optind < argc+2){
    train_path = string(argv[optind]);
    dev_path = string(argv[optind+1]);
    test_path = string(argv[optind+2]);
  }else{
    cerr << "Missing either training or dev file name ... aborting." <<endl;
    exit(1);
  }

  Treebank all_tbk(train_path);
  all_tbk.update_trees(dev_path);
  all_tbk.update_trees(test_path);

  //Treebank dev_bank(dev_path);

  #ifdef ENABLE_PARALLEL
      if (num_procs > batch_size){num_procs = batch_size;}
      omp_set_num_threads(num_procs);
  #else
    num_procs = 1;
  #endif
  if(mlocality==LOCAL){
      num_procs = 1;
      batch_size = 1;
      modtype = SrParser::PERCEPTRON;
  }

  if(modtype != SrParser::PERCEPTRON){
      mlocality = GLOBAL;
      uptype = SrParser::MAXV_UP;
  }
  //if(batch_size > base_tbk.size()){batch_size = base_tbk.size();}

  TemplateTypeDefinition ttd(all_tbk.colnames);
  SparseEncoder templates(tpl_path,ttd);
  templates.do_sanity_check();//checks that templates are semantically valid.
  //wcout << templates.toString(ttd) << endl;
  SrParser srp(templates);

  mkdir(model_path.c_str(), S_IRUSR | S_IWUSR | S_IXUSR);//creates model dir
  //RUN


  Treebank kparsed;
  string kfold_path("k-");
  unsigned Kfolds=10;
  for(int k = 0 ; k < Kfolds; ++k){ 
    Treebank base_tbk;
    Treebank dev_bank;
    all_tbk.fold(base_tbk,dev_bank,k,Kfolds);
    if (modtype == SrParser::PERCEPTRON && mlocality == LOCAL){srp.train_local_model(base_tbk,dev_bank,transformation,beam,perceptron_iterations);}
    if (mlocality == GLOBAL && batch_size == 1){srp.train_global_model(base_tbk,dev_bank,transformation,beam,perceptron_iterations,uptype,modtype,C,lambda);}
    if (mlocality == GLOBAL && batch_size > 1){srp.train_global_minibatch_model(base_tbk,dev_bank,transformation,beam,perceptron_iterations, batch_size,uptype,modtype,C,lambda);}
    
    //parse corpus
    MultiOutStream out;
    string outfile(kfold_path);
    outfile+= std::to_string(k);
    out.setPennTreebankOutfile(outfile);
    srp.parse_corpus(dev_bank,out);
    
  }
}



   



