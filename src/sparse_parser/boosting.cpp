#include "boosting.h"
#include "state_graph.h"
#include "treebank.h"
#include "srgrammar.h"
#include "lexer.h"
#include "srparser.h"


#include <random>
#ifdef ENABLE_PARALLEL
    #include <omp.h>
#endif


//resamples the data with D
//vector<unsigned int> sample_data(const vector<double> &data_weights,
//                                 unsigned int sample_size){
    
//    //generate weighted sample
//    std::random_device rd;         //defined in <random>
//    std::mt19937 gen(rd());
//    std::discrete_distribution<int> D(data_weights.begin(),data_weights.end());

//    vector<unsigned int> samp_idces;
//    for(int i = 0; i < sample_size;++i){
//        samp_idces.push_back(D(gen));
//    }
//    return samp_idces;
//}


struct CompareSort{
    const vector<double> & value_vector;

    CompareSort(const vector<double> & val_vec):
        value_vector(val_vec) {}

    bool operator()(double i1, double i2){
        return value_vector[i1] < value_vector[i2];
    }
};


vector<unsigned int> sample_data(const vector<double> &data_weights,
                                 unsigned int sample_size) {

    vector<double> new_data_weights = vector<double>(data_weights.size());

    //TODO: take out extreme 5% largest weights (artificially, and then put back)
    if (true) {
        int num_at_extremes = data_weights.size()*0.05;

        //cout << "num at extremes = " << num_at_extremes<< endl;

        //sort
        vector<int> idces = vector<int>();
        for (int i=0; i< data_weights.size(); ++i)
            idces.push_back(i);
        sort(idces.begin(), idces.end(), CompareSort(data_weights));

        // set weights to 0
        float sum = 0.0;
        for (int i=0; i< idces.size(); ++i) {
            if (i >= data_weights.size() - num_at_extremes)
                new_data_weights[idces[i]] = 0.0; // artificially set to 0
            else {
                sum += data_weights[idces[i]];
                new_data_weights[idces[i]] = data_weights[idces[i]];
            }
        }
        // renormalise
        for (int i=0; i< data_weights.size(); ++i)
            new_data_weights[i] = new_data_weights[i]/sum;

    }
    else
        new_data_weights = data_weights;

    std::map<double, int> weights2num = std::map<double, int>();
    // print weights
    /*for (int i=0; i< new_data_weights.size(); ++i) {
        weights2num[new_data_weights[i]]++;
    }
    for (auto const &weight : weights2num) {
        cout << weight.first << "\t" << weight.second << endl;
    }
    cout << "-------------------" << endl;*/


    // calculate cumulative weights
    vector<double> cumul_weights = vector<double>(data_weights.size());
    for (int i=0; i< data_weights.size(); ++i) {
        if (i==0)
            cumul_weights[i] = new_data_weights[i];
        else
            cumul_weights[i] = new_data_weights[i]+cumul_weights[i-1];
    }

    // sample indices
    vector<unsigned int> sample_indices;
    for (unsigned int i=0; i<sample_size; ++i) {
        sample_indices.push_back(resample_one_idx(cumul_weights));
    }
    return sample_indices;
}

unsigned int resample_one_idx(const vector<double> &cumul_weights) {
    // get random number and calculate which example corresponds
    double rnd = (rand() / ((double)RAND_MAX))
            *cumul_weights[cumul_weights.size()-1];

    unsigned int local_example_idx = get_idx_binsearch(cumul_weights, rnd);

    return local_example_idx;
}

unsigned int get_idx_binsearch(const vector<double> &cumul_weights,
                               const double rnd, int first,
                               int last) {
    // starting point
    if (last==-1)
        last = cumul_weights.size()-1;
    // end
    if (first==last)
        return first;

    int midpoint = first + (last-first)/2;
    if (rnd > cumul_weights[midpoint])
        return get_idx_binsearch(cumul_weights,
                                 rnd, midpoint+1, last);
    else
        return get_idx_binsearch(cumul_weights,
                                 rnd, first, midpoint);
}

//returns the index of the max element in the scores vector
unsigned int _argmax(vector<float> const &scores,size_t Vs){
    
    float mmax = scores[0];
    unsigned int amax = 0;
    
    for(int j = 1; j <Vs ;++j){
        if(scores[j] > mmax){
            mmax = scores[j];
            amax = j;
        }
    }
    return amax;
}

bool _file_exists(const string &fileName) {
    ifstream infile(fileName);
    return infile.good();
}

void _empty_folder(string &folder_path) {
    string delallmodels = "exec rm -r "+folder_path;
    int res = system(delallmodels.c_str());
    if (res!=0) {
        throw("Cannot delete all model files for some reason\n");
    }
    mkdir(folder_path.c_str(), S_IRUSR | S_IWUSR | S_IXUSR);
}

bool _parent_folder_exists(string output_folder) {
    // get parent folder name based on position of last slash

    unsigned int pos_last_slash = output_folder.find_last_of("/");
    string parent_folder = output_folder.substr(0, pos_last_slash);

    struct stat sb;
    if (stat(parent_folder.c_str(), &sb) == 0 && S_ISDIR(sb.st_mode)) {
        return true;
    }
    clog << parent_folder << " does not exist" << endl;
    return false;
}

string _remove_trailing_slash(string &folder) {
    const size_t len = folder.length();
    if(folder[len-1] == '\\' || folder[len-1] == '/' )
        return folder.substr(0, folder.length()-1);
    else
        return folder;
}


// convert wstring to string
string ws2s(wstring& wstr){
    string newstr(wstr.begin(), wstr.end());
    return newstr;
}

//////////////////////// MAIN /////////////////////////

int main_parser(){

    #ifdef ENABLE_PARALLEL
        omp_set_num_threads(1);
    #endif

    /* Params:
     *  - two treebanks (train and dev)
     *  - default set/skeleton/no restrictions
     *  - pre-ranking + k + local/global optimsiation + num perceptron iters
     *  - grouping (bool)
     *  - num perceptron iterations
     *  - sample size (percentage of data?)
     *  - output folder
     */

    unsigned int num_perceptron_iters = 15;
    float data_size = 1.0; // proportion of data used


    // 0)  ===========  Prepare output folder  ===========
    //string output_folder("/Users/bcrabbe/hyparse/trunk/Boosting_experiments/test");
    string output_folder("../Boosting_experiments/test");
    //string train_file("/Users/bcrabbe/hyparse/trunk/data/test1_ttd.tbk");
    string train_file("../data/ftb20-train-ttd.tbk");
    // "../data/FRENCH/pred/ptb/train/treebank.tbk"
    //string train_file("/media/rachel/DOCUMENTS/Paris_7/Stage M2/Data/ftb20-trainsmall.tbk");
    //string train_file("/media/rachel/DOCUMENTS/Paris_7/Stage M2/Data/one_example.tbk");

    //string dev_file("/Users/bcrabbe/hyparse/trunk/data/test1_ttd.tbk");
    string dev_file("../data/ftb20-dev-ttd.tbk");
    // "../data/FRENCH/pred/ptb/dev/treebank.tbk"
    //string dev_file("/media/rachel/DOCUMENTS/Paris_7/Stage M2/Data/ftb20-devsmall.tbk");
    //string dev_file("/media/rachel/DOCUMENTS/Paris_7/Stage M2/Data/one_example.tbk");
    string skeleton_file("../data/Skeletons/s0.t_q0.skn");
    // "../data/Skeletons/q0-2_s0-1.skn"
    string default_set_file("../data/generic.tpl"); //  "/Users/bcrabbe/hyparse/trunk/data/generic2.tpl"

    BLOGGER blogger(output_folder);
    //blogger.set_output_folder(output_folder);
    srand(time(NULL));


    // 1) =========== ParseData ===========

    //Treebank train("/Users/bcrabbe/hyparse/trunk/data/ftb20-train-ttd.tbk");
    //Treebank dev("/Users/bcrabbe/hyparse/trunk/data/ftb20-dev-ttd.tbk");
    //Treebank train("../data/FRENCH/pred/ptb/train/treebank.tbk");
    //Treebank dev("../data/FRENCH/pred/ptb/dev/treebank.tbk");


    Treebank train(train_file);
    Treebank dev(dev_file);

    ParseData data(train, dev);
    data.get_random_error(); // TODO place in constructor?

    // i) initialise heuristics used to reduce search space
    // they are mutually (last option set is the chosen one)
    data.set_default_set(default_set_file);
    blogger.default_file = default_set_file;

    //data.set_skeleton(skeleton_file);
    //blogger.skeleton_file = skeleton_file;


    //data.no_restrictions();

    // ii) pre-rank and specify how many are searched TODO!
    data.use_prerank(5, true, output_folder); // only look at k best-ranked at a time

    // iii) Decide whether to group or not
    //data.set_grouping(true);

    blogger.initialise_params(&data, true);
    blogger.num_perc_iters=num_perceptron_iters;
    blogger.dataset_train=train_file;
    blogger.dataset_dev=dev_file;
    blogger.print_out_params();

    data.generate_candidates(); // get candidates

    // 2) =========== LearnerPool ===========
    //LearnerPool<ParseAction> lp(data);
    //lp.generate_candidates();

    blogger.set_duration_prerank(data.get_duration_prerank());

    // 3) =========== StrongLearner ===========
    vector<ParseAction> acvec;
    data.grammar.get_ordered_actions(acvec);

    StrongLearner<ParseAction,ParseData> sl(acvec, data.nexamples_train,
                                  data.get_gold_ys_train(),
                                  data.get_gold_ys_dev());

    // 4) =========== RUN ===========

    WeakLearner<ParseAction>* wl;
    bool remove(false);

    for (int i=0; i<data.get_num_available_learners(); ++i) {

        clog << "\n------------\nIteration number " << i+1 << endl;

        wl=sl.select_weak_learner(data, (int)sl.get_weights().size()*data_size,
                                  num_perceptron_iters);
        // what does it have in it?
        //clog << wl->describe() << endl;

        if (wl->get_error(true, sl.get_weights()) < data.get_random_error()) {

            sl.add_weak_learner(data, wl,remove);
            blogger.new_iteration(&sl, wl, data);
            blogger.print_iteration();

            sl.recalculate_weights(wl);
            data.learner_chosen(wl, remove);

            //lp.update(wl, remove);
            delete wl;
        }
        else {
            delete wl;
            break;
        }
    }
    return 0;
}

int main_numeric(){
#ifdef ENABLE_PARALLEL
    omp_set_num_threads(3);
#endif
    
    //true solution (0,1,2)
    vector<unsigned int> vec{0,1,3};//specifies templates
    NumericData D(vec,string("/Users/bcrabbe/digitst.txt"),string("/Users/bcrabbe/digitst.txt"));
    vector<unsigned int> vec2{1,2,3};//specifies templates
    D.add_templates(vec2);
    vector<unsigned int> vec3{0,2,3};//specifies templates
    D.add_templates(vec3);
    vector<unsigned int> vec4{0,1,4};//specifies templates
    D.add_templates(vec4);
    vector<unsigned int> vec5{1,2,4};//specifies templates
    D.add_templates(vec5);
    vector<unsigned int> vec6{0,2,4};//specifies templates
    D.add_templates(vec6);
    vector<unsigned int> vec7{0,1,2};//specifies templates
    D.add_templates(vec7);
    
    vector<unsigned int> ylabs = D.get_label_set();
    StrongLearner<unsigned int,NumericData> strong(ylabs, D.trainN(),D.train_y(),D.dev_y());
    
    WeakLearner<unsigned int>* wl;
    bool remove = false;
    for (int i=0; i< 50 ; ++i) {
        clog << "------------\nIteration number " << i+1 << endl;
        D.reset_learner_pool();
        wl=strong.select_weak_learner(D,D.trainN(),1);
        
        // what does it have in it?
        //cout << wl->describe() << endl;
        
        if (wl->get_error(true, strong.get_weights()) > D.get_random_error()) {
            break;
        }else{
            strong.add_weak_learner(D,wl,remove);
            
            strong.recalculate_weights(wl); // use alpha to recalculate distribution
            if(remove){D.remove_learner( *wl );}
            delete wl;
            if(strong.get_train_accurracy() == 1.0){
                break;
            }
        }
        cout << strong.get_train_accurracy()<<"/"<<strong.get_dev_accurracy()<< endl;
    }
    cout << strong.get_train_accurracy()<<"/"<<strong.get_dev_accurracy()<< endl;
    return 0;
}
/*
int main(){
    main_parser();
    //main_numeric();
}*/

//////////////////// TEMPLATE INFO //////////////////

TemplateInfo::TemplateInfo() {
}


TemplateInfo::~TemplateInfo() {
    //TODO
}

TemplateInfo::TemplateInfo(TemplateInfo const &other) {
    conditions          = other.conditions;
    templates           = other.templates;
    template_groups     = other.template_groups;
    available_templates = other.available_templates;
    available_learners = other.available_learners;
    ttd                 = other.ttd;
    ccombos2tgs         = other.ccombos2tgs;
    skeleton            = other.skeleton;
}

TemplateInfo& TemplateInfo::operator=(TemplateInfo const &other) {
    conditions          = other.conditions;
    templates           = other.templates;
    template_groups     = other.template_groups;
    available_templates = other.available_templates;
    available_learners = other.available_learners;
    ttd                 = other.ttd;
    ccombos2tgs         = other.ccombos2tgs;
    skeleton            = other.skeleton;
    return *this;
}

const FeatureTemplate& TemplateInfo::get_template(FTIDX ftidx) const {
    return templates[ftidx];
}

wstring TemplateInfo::describe_template(FTIDX ftidx) const{

    return templates[ftidx].toString(ttd);
}

// associate condition combos (as vector of idx nums) to template group numbers
int TemplateInfo::associate_conds_to_tgidx(bool group, const int &k,
                                        vector<unsigned int> included_cnums,
                                        int start_index, int tg_num) {
    if (k==0)
        return tg_num;
    else {
        vector<unsigned int> new_included_cnums;
        for (int i=start_index; i<conditions.size(); ++i) {
            // copy vector condition nums
            new_included_cnums = vector<unsigned int>(included_cnums);
            new_included_cnums.push_back(i);

            // associate all condition combos with the template number and
            // increase template number
            if (group && new_included_cnums.size() == 3 ) {
                for (vector<unsigned int> &combo: get_combos(new_included_cnums,
                                                     3)) {
                    if (ccombos2tgs.count(combo) == 0) {
                        ccombos2tgs[combo] = vector<unsigned int>();
                    }
                    ccombos2tgs[combo].push_back(tg_num);
                }
                tg_num++;
            }
            else if (!group){
                if (ccombos2tgs.count(new_included_cnums) == 0) {
                    ccombos2tgs[new_included_cnums] = vector<unsigned int>();
                }
                ccombos2tgs[new_included_cnums].push_back(tg_num);
                tg_num++;
            }
            tg_num = associate_conds_to_tgidx(group, k-1, new_included_cnums, i+1,
                                              tg_num);
        }
    }
    return tg_num;
}

unsigned int TemplateInfo::get_size_wl(WLIDX idx) const{

    unsigned int size=0;
    for (int i=0; i<template_groups[idx].size(); ++i) {
        if (available_templates[template_groups[idx][i]])
            size++;
    }
    return size;
}

/*
 * Return the weak learner index of the next available weak learner
 * (current learner_num starts at 0)
 */
unsigned int TemplateInfo::get_idx_next(unsigned int current_learner_num) const{

    unsigned int num_available=0;
    for (int i=0; i<available_learners.size(); ++i) {
        if (available_learners[i]) {
            if (num_available==current_learner_num) {
                return i;
            }
            else
                num_available++;
        }
    }

    clog << "There has been a problem in the selection of the next index."
            "You are trying to access another learner and none are left."
            "Aborting!" << endl;
    exit(1);
}

/* get all combinations of condition numbers given a maximum number of
 * conditions in a combo k
 */
vector<vector<unsigned int>> TemplateInfo::get_combos(
        const vector<unsigned int> &cnums, int k, int start_index,
        vector<unsigned int> prefix) {

    vector<vector<unsigned int>> combos;
    vector<unsigned int> combo;
    if (k==0)
        return combos;
    for (int i=start_index; i<cnums.size(); ++i) {
        combo = vector<unsigned int>(prefix);
        combo.push_back(cnums[i]);
        combos.push_back(combo);
        for (vector<unsigned int> &new_combo : get_combos(cnums, k-1, i+1, combo)) {
            combos.push_back(new_combo);
        }
    }
    return combos;
}


/* Generate conditions for templates (stock as wstrings)
 * - If a skeleton is set, only generate conditions in those positions
 * - Otherwise generate s2-s0 and q0-q3
 */
void TemplateInfo::generate_conditions(vector<wstring> lex_info) {

    wstring posn;
    // create and add all possible Q conditions
    for (int qnum = 0; qnum < 4; ++qnum) {
        posn = L"q"+ to_wstring(qnum);
        for (int id = 0; id < lex_info.size(); ++id) {
            add_condition(posn + L"("+lex_info[id]+L")", posn);
        }
    }
    // create and all possible S conditions
    for (int snum = 0; snum < 3; ++snum) {
        posn = L"s"+to_wstring(snum);
        // phrase categories
        add_condition(posn + L"(t,c,_)", posn+L".t");
        // only dtrs of s0 and s1 used
        if (snum != 2) {
            add_condition(posn + L"(r,c,_)", posn+L".r");
            add_condition(posn + L"(l,c,_)", posn+L".l");
        }
        // lexical info
        for (int id = 0; id < lex_info.size(); ++id) {
            add_condition(posn+L"(t,h,"+lex_info[id]+ L")",posn+L".t");
            // only dtrs of s0 and s1 used
            if (snum != 2) {
                add_condition(posn+L"(r,h,"+lex_info[id]+ L")",posn+L".r");
                add_condition(posn+L"(l,h,"+lex_info[id]+ L")",posn+L".l");

                // span
                add_condition(posn+L"(L,_,"+lex_info[id]+L")", posn+L".LSPAN");
                add_condition(posn+L"(R,_,"+lex_info[id]+L")", posn+L".RSPAN");
            }
        }
    }
}

void TemplateInfo::add_condition(wstring str_condition, wstring position) {
    if (skeleton.empty() || position_in_skeleton(position)) {
        conditions.push_back(str_condition);
    }
}



/* Create all feature templates from combinations of conditions
 * (using recursion)
 */
void TemplateInfo::generate_templates(int k, vector<unsigned int> prefix,
                                   int start_index) {
    if (k==0)
        return;
    else {
        vector<unsigned int> new_prefix;
        for (int i=start_index; i<conditions.size(); ++i) {

            // copy prefix (list of cond nums)
            new_prefix = vector<unsigned int>(prefix);
            new_prefix.push_back(i);
            create_template(new_prefix);
            generate_templates(k-1, new_prefix, i+1);
        }
    }
}

/* Create an individual template from a vector of condition numbers
 */
void TemplateInfo::create_template(const vector<unsigned int> &condition_nums) {

    wstring template_string = L"P("+ to_wstring(ntemplates+1) + L") = ";
    int condition_nums_size = condition_nums.size();

    for (int i=0; i<condition_nums_size; ++i) {
        template_string += conditions[condition_nums[i]];
        if (i<condition_nums.size()-1) template_string += L" & ";
    }

    FeatureTemplate ft(template_string, ttd);
    templates.push_back(ft);


    // add template to template group
    for (int tg_num : ccombos2tgs[condition_nums]) {
        template_groups[tg_num].push_back(ntemplates);
    }
    ntemplates++;
}



bool TemplateInfo::position_in_skeleton(wstring position) {

    for(int i=0; i<skeleton.size(); ++i) {
        if (skeleton[i]==position)
            return true;
    }
    return false;
}

bool TemplateInfo::template_already_included(FTIDX idx) {
    if (included_templates.find(idx) != included_templates.end())
        return true;
    else
        return false;
}

void TemplateInfo::remove_template(FTIDX idx) {
    // remove from template groups but never from 'templates'
    for (int i=0; i < template_groups.size(); ++i) {
        for (int j=0; j < template_groups[i].size(); ++j) {
            if (template_groups[i][j]==idx) {
                swap(template_groups[i][j], template_groups[i].back());
                template_groups[i].pop_back();
                //break; // only once! CHECK!!!
            }
        }
    }
}



//////////////////// PARSE DATA //////////////////

/* Initialise ParseData
 * Includes:
 * - calculating number of local examples in train and dev
 * - stocking gold predictions of local examples
 */
ParseData::ParseData(Treebank train, Treebank dev) {

    // prepare data
    train.transform(bintreebank_train, false, 0, Treebank::NO_DECORATIONS);
    dev.transform(bintreebank_dev, false, 0, Treebank::NO_DECORATIONS);
    bintreebank_train.update_encoder();
    bintreebank_dev.update_encoder();
    grammar = SrGrammar(bintreebank_train, bintreebank_dev, false);
    tpl_info.ttd = TemplateTypeDefinition(bintreebank_train.colnames);

    // prepare gold predictions and save number of examples for train
    StateSignature sig;
    ParseAction outgoing_action;
    for (int i=0; i<bintreebank_train.size(); ++i) {
        InputDag input_sequence;
        tree_yield(bintreebank_train[i], input_sequence);
        size_t Nseq = input_sequence.size();
        ParseDerivation deriv(bintreebank_train[i], grammar,
                              input_sequence, false);
        for (int j=0; j<deriv.size()-1; ++j) {
            nexamples_train++;
            deriv[j]->get_signature(sig, &input_sequence, Nseq);
            outgoing_action = deriv[j+1]->get_incoming_action();
            gold_ys_train.push_back(outgoing_action);
        }
    }

    // prepare gold predictions and save number of examples for dev
    for (int i=0; i<bintreebank_dev.size(); ++i) {
        InputDag input_sequence;
        tree_yield(bintreebank_dev[i], input_sequence);
        size_t Nseq = input_sequence.size();
        ParseDerivation deriv(bintreebank_dev[i], grammar,
                              input_sequence, false);
        for (int j=0; j<deriv.size()-1; ++j) {
            nexamples_dev++;
            deriv[j]->get_signature(sig, &input_sequence, Nseq);
            outgoing_action = deriv[j+1]->get_incoming_action();
            gold_ys_dev.push_back(outgoing_action);
        }
    }
}

ParseData::~ParseData() {
}

ParseData::ParseData(ParseData const &other) {
    grammar             = other.grammar;
    bintreebank_dev     = other.bintreebank_dev;
    bintreebank_train   = other.bintreebank_train;
    gold_ys_dev         = other.gold_ys_dev;
    gold_ys_train       = other.gold_ys_train;
    random_error        = other.random_error;
    default_set         = other.default_set;
    group               = other.group;
    prerank             = other.prerank;
    prerank_file        = other.prerank_file;
    kbest               = other.kbest;
    current_learner     = other.current_learner;
    tpl_info            = other.tpl_info;
}

ParseData& ParseData::operator=(ParseData const &other) {
    grammar             = other.grammar;
    bintreebank_dev     = other.bintreebank_dev;
    bintreebank_train   = other.bintreebank_train;
    gold_ys_dev         = other.gold_ys_dev;
    gold_ys_train       = other.gold_ys_train;
    random_error        = other.random_error;
    default_set         = other.default_set;
    group               = other.group;
    prerank             = other.prerank;
    prerank_file        = other.prerank_file;
    kbest               = other.kbest;
    current_learner     = other.current_learner;
    tpl_info            = other.tpl_info;
    return *this;
}


/* Generate examples from the resampled indices in ParseData and stock the
 * examples in the WeakLearner given as a parameter.
 */
void ParseData::gen_examples(WeakLearner<ParseAction>* wl, bool train)const{

    Treebank const *bintreebank;
    if (train)
        bintreebank = &bintreebank_train;
    else
        bintreebank = &bintreebank_dev;

    // create sparse encoder corresponding to wl
    SparseEncoder sp_encoder = SparseEncoder();
    for (FeatureTemplate &ft : tpl_info.get_learner_templates(wl->wl_number)) {
        sp_encoder.add_template(ft);
    }

    vector<unsigned int> xvec(sp_encoder.ntemplates(),0);
    ParseAction outgoing_action;

    int ex_num=0;

    for (unsigned int i=0; i<bintreebank->size(); ++i){
        InputDag input_sequence;
        tree_yield((*bintreebank)[i], input_sequence);
        size_t Nseq = input_sequence.size();
        ParseDerivation deriv((*bintreebank)[i], grammar, input_sequence, false);
        for (unsigned int j=0; j<deriv.size()-1; ++j) {
            StateSignature sig;
            deriv[j]->get_signature(sig, &input_sequence, Nseq);
            deriv[j]->encode(xvec,sp_encoder,sig);
            outgoing_action = deriv[j+1]->get_incoming_action();
            vector<float> grammar_constraints(grammar.num_actions(),0.0);
            grammar.select_actions(grammar_constraints,deriv[j]->get_incoming_action(),sig);
            if (train){
                wl->add_train_datapoint(xvec, outgoing_action,grammar_constraints);
            }else{
                wl->add_dev_datapoint(xvec, outgoing_action,grammar_constraints);
            }
            ex_num++;
        }
    }
}

float ParseData::get_random_error() {
    return 1 - (1.0/grammar.num_actions());
}

vector<ParseAction> ParseData::get_gold_ys_train() {
    return gold_ys_train;
}

vector<ParseAction> ParseData::get_gold_ys_dev() {
    return gold_ys_dev;
}

void ParseData::set_skeleton(string template_guide_file) {

    if (! _file_exists(template_guide_file)) {
        clog << "Skeleton file path not valid. Aborting." << endl;
        exit(1);
    }

    wifstream is(template_guide_file);
    wstring position;
    while (getline(is, position)) {
        tpl_info.skeleton.push_back(position);
    }
    is.close();
}

void ParseData::set_default_set(string template_guide_file) {
    group=false; // incompatible with grouping
    default_set=true;

    if (! _file_exists(template_guide_file)) {
        clog << "Default template filepath does not exist. Aborting." << endl;
        exit(1);
    }

    // read file and stock templates
    wifstream is(template_guide_file);
    FeatureTemplate tpl(0);
    while(FeatureTemplate::get_template(is,tpl,tpl_info.ttd)){
        tpl_info.templates.push_back(tpl);
    }
    is.close();
}


void ParseData::no_restrictions() {
    tpl_info.templates.clear();
    tpl_info.template_groups.clear();
    tpl_info.conditions.clear();
    tpl_info.skeleton.clear();
    default_set=false;
}

void ParseData::use_prerank(unsigned int topntemplates, bool local, string folder) {
    prerank=true;
    kbest=topntemplates;
    prerank_local = local;
    output_folder = _remove_trailing_slash(folder);
}

void ParseData::use_prerank_from_file(unsigned int topntpls, string &filepath) {
    prerank=true;
    kbest=topntpls;
    prerank_local = true;
    prerank_file = filepath;
}

void ParseData::set_grouping(bool groupparam) {
    if (!default_set) {
        group=groupparam;
    }
    else {
        if (default_set)
            clog << "Cannot use grouping when using a default set!" << endl;
    }
}


void ParseData::generate_candidates(){

    if (default_set) {
        // templates already loaded, add each one to a separate group
        vector<FTIDX> tgroup;
        if (true) {
            for (int i=0; i < tpl_info.templates.size(); ++i) {
                tgroup.clear();
                tgroup.push_back(i);
                tpl_info.template_groups.push_back(tgroup);
                tpl_info.ntemplates++;
            }
        }
        else { // put ALL templates in same group!
            for (int i=0; i < tpl_info.templates.size(); ++i) {
                tgroup.push_back(i);
                tpl_info.ntemplates++;
            }
            tpl_info.template_groups.push_back(tgroup);
        }
    }

    else {
        // need to generate templates automatically (with or without skeleton)
        tpl_info.generate_conditions(bintreebank_train.colnames);
        unsigned int ngroups = tpl_info.associate_conds_to_tgidx(group, 3);
        tpl_info.template_groups = vector<vector<FTIDX>>(ngroups);
        tpl_info.generate_templates();
    }

    // all available at beginning
    tpl_info.available_templates = vector<bool>(tpl_info.ntemplates, true);

    tpl_info.available_learners = vector<bool>(tpl_info.template_groups.size(),
                                                true);

    /*if (prerank) {
        time_t ctime = time(0);
        sort_classifiers_by_performance();
        duration_prerank = time(0) - ctime;
    }*/
}

void ParseData::prerank_classifiers() {
    generate_candidates();
    sort_classifiers_by_performance();
}

unsigned int ParseData::get_num_available_learners()const {
    // number of ones available
    int count=0;

    for (int i=0; i<tpl_info.template_groups.size(); ++i) {
        if (tpl_info.available_learners[i])
            count++;
    }
    return count;
}

/*
 *  The number of accessible learners. Not always equal to the number
 * of available ones (if preranking is used and k is set to a lower number
 * than the number of total weak learners)
 */
unsigned int ParseData::get_num_accessible_learners() const{

    if (prerank && kbest < get_num_available_learners()) {
        return kbest;
    }
    else
        return get_num_available_learners();
}

/*
 * thread-safe
 */
WeakLearner<ParseAction>* ParseData::get_next_wl(){

    // find next accessible learner (the current_learner-th accessible one)
  
    WeakLearner<ParseAction>* wl;
    #pragma omp critical
    {
        WLIDX next_idx;
        vector<ParseAction> acvec;
        
        current_learner++;
        /*assert(current_learner < get_num_accessible_learners()
               && "Cannot access another weak learner");*/

        grammar.get_ordered_actions(acvec);
        next_idx = tpl_info.get_idx_next(current_learner);

        wl = new WeakLearner<ParseAction>(describe_next_wl(next_idx),acvec, next_idx);
    }
        wl->set_size(tpl_info.get_size_wl(wl->wl_number));
        gen_examples(wl, true);
    //}
    return wl;
}

bool ParseData::has_next_wl() {

    // next index = current_learner+1
    if (current_learner+1<get_num_accessible_learners()) {
        return true;
    }
    else {
        return false;
    }
}


void ParseData::learner_chosen(WeakLearner<ParseAction>* wl, bool remove) {

    // remove learner and all its templates included
    if (remove)
        remove_learner(*wl);

    // add to included
    for (FTIDX ftnum : tpl_info.get_learner_template_nums(wl->wl_number))
        tpl_info.included_templates.insert(ftnum);

    // reinitialise current_learner for next iteration
    current_learner=-1;

}

vector<FeatureTemplate> TemplateInfo::get_learner_templates(WLIDX idx) const{
    vector<FeatureTemplate> fts;
    for (FTIDX ft_num : template_groups[idx]) {
        if (available_templates[ft_num])
            fts.push_back(get_template(ft_num));
    }
    return fts;
}

vector<FTIDX> TemplateInfo::get_learner_template_nums(WLIDX idx) const{
    vector<FTIDX> fts;

    for (FTIDX ft_num : template_groups[idx]) {
        if (available_templates[ft_num])
            fts.push_back(ft_num);
    }
    return fts;
}

string ParseData::describe_next_wl(WLIDX wl_idx) const{
    wstring description;
    //WLIDX wl_idx = tpl_info.get_idx_next(current_learner);

    if (current_learner>=get_num_accessible_learners()) {
        description =  L"NULL - No more miniparsers available";
    }
    else {
        for (FTIDX ft_num : tpl_info.get_learner_template_nums(wl_idx)) {
             description += tpl_info.describe_template(ft_num);
             //description += L"\n";
        }
    }

    string str_description(description.begin(), description.end());
    return str_description;
}

string ParseData::describe_params() {

    // HEURISTICS
    wstring descr=L"Heuristics\n";
    if (default_set)
        descr+= L"\tUsing a default set of templates\n";
    else if (tpl_info.skeleton.size()>0) {
        descr+= L"\tUsing a skeleton: ";
        for (int i=0; i<tpl_info.skeleton.size(); ++i)
            descr+= tpl_info.skeleton[i] + L" ";
        descr+=L"\n";
    }
    else
        descr+= L"\tGenerating all templates s0-2 and q0-3\n";

    if (group)
        descr+= L"\tGrouping activated\n";
    else
        descr+= L"\tGrouping not activated\n";
    if (prerank) {
        descr += L"\tPreranking with k = "+to_wstring(kbest)+L" and ";
        if (prerank_local)
            descr+=L"local optimisation\n";
        else
            descr+=L"global optimisation\n";
        descr += L"\tTime taken to prerank (secs)   : " +
                to_wstring(duration_prerank)+L"\n";
    }
    else
        descr += L"\tNo preranking used\n";

    string new_descr(descr.begin(), descr.end());
    return new_descr;
}


void ParseData::remove_learner(WeakLearner<ParseAction> const &wl) {

    WLIDX wl_idx = wl.wl_number; // index number
    // make template group inaccessible
    tpl_info.available_learners[wl_idx] = false;
}


void ParseData::load_ordered_classifiers(){
    clog << "\nLoading pre-ranking of classifiers ..." << endl;
    // load order
    ifstream pkf;
    string line;
    vector<WLIDX> indices;
    unsigned int max_idx=0;
    pkf.open(prerank_file);
    if (pkf.is_open()) {
        while (getline (pkf, line)){
            if (line!="" && line!=" ")
                indices.push_back(atoi(line.c_str()));
                if (indices.back()>max_idx)
                    max_idx = indices.back();
        }
    }
    else {
        cerr << "Could not open preranking file: " << prerank_file <<". Aborting." << endl;
        exit(1);
    }

    if (max_idx!=get_num_available_learners()-1
            && indices.size() != get_num_available_learners()) {
        cerr << "There was a problem with the preranking file. The number of templates"
             << " listed does not correspond to the number of weak learners in this case."
             << "Aborting." << endl;
        exit(1);
    }

    vector<vector<FTIDX>> new_template_groups;
    for (WLIDX idx: indices)
        new_template_groups.push_back(tpl_info.template_groups[idx]);

    tpl_info.template_groups = new_template_groups;
    clog << endl << "\tRanking finished!\t\t" << endl;
}

void ParseData::sort_classifiers_by_performance(){

    vector<vector<FTIDX>> new_template_groups;
    clog << "\nStarting pre-ranking of classifiers ..." << endl;
    // need params local/global and perceptron iters

    vector<int> indices;
    vector<float> acc;
    for (int i=0; i<get_num_available_learners(); ++i) {
        indices.push_back(i);
        acc.push_back(0); // initialise at 0
    }
    int num=0;

    #pragma omp parallel for
    for (int i=0; i<get_num_available_learners(); ++i) {
        SparseEncoder encoder;
        SrParser srp;
        # pragma omp critical
        {
            num++;
            clog << "\r\tGroup number " << num << " of "
                << get_num_available_learners() << "\t";
        }
        encoder = SparseEncoder();
        for (FTIDX &ftidx : tpl_info.template_groups[i]) {
            encoder.add_template(tpl_info.templates[ftidx]);
        }
        srp = SrParser(encoder);

        // train
        if (prerank_local) {
        srp.train_local_model(bintreebank_train, bintreebank_dev,
                              Treebank::NO_DECORATIONS,
                              4, 1, false);
        }
        else {
            srp.train_global_model(bintreebank_train, bintreebank_dev,
                                   Treebank::NO_DECORATIONS,
                                   4, 1,
                                   SrParser::MAXV_UP,
                                   SrParser::PERCEPTRON,
                                   1, 1,false);
        }

        // evaluate
        //indices.push_back(i);
        if (prerank_local) {
            acc[i] = srp.eval_local_model(bintreebank_dev);
        }
        else{
            acc[i] = std::get<0>(srp.eval_model(bintreebank_dev));
        }
    }

    // sort template group indices by performance
    std::sort(std::begin(indices),std::end(indices),
              [&](int i1, int i2) { return acc[i1] > acc[i2]; }
    );

    // copy sorted templates
    tpl_info.sorted_indices = vector<WLIDX>();
    for (int i=0; i<get_num_available_learners(); ++i) {
        new_template_groups.push_back(tpl_info.template_groups[indices[i]]);
        tpl_info.sorted_indices.push_back(indices[i]);
    }
}

void ParseData::dump_ordered_classifiers(string output_file){
    ofstream outfile;
    outfile.open(output_file);

    current_learner = 0;
    for (WLIDX idx: tpl_info.sorted_indices) {
        outfile << idx << endl;
        //cout << idx << endl;
        current_learner++;
    }
    outfile.close();
}

NumericData::NumericData(vector<unsigned int> const &sel_columns,string const &train,string const &dev){
    add_templates(sel_columns);
    read_dataset(train,true);
    read_dataset(dev,false);

}

void NumericData::read_dataset(string const &filename,bool train){
    str::SimpleTokenizer tok;
    if(train){
        ifstream sin(filename);
        string bfr;
        vector<wstring> svalues;
        vector<unsigned int> ivalues;
        while(getline(sin,bfr)){
            size_t arity = tok.llex(bfr,svalues);
            if(arity > 0){
                ivalues.clear();
                for(int i = 0; i < arity-1;++i){
                    ivalues.push_back(stoi(svalues[i]));
                }
                xtrain_dataset.push_back(ivalues);
                ytrain_lbls.push_back(stoi(svalues.back()));
            }
        }
        set<unsigned int> lab_set(ytrain_lbls.begin(),ytrain_lbls.end());
        ylab_set.assign(lab_set.begin(), lab_set.end());
        ydims = ylab_set.size();
        sin.close();
    }else{
        ifstream sin(filename);
        string bfr;
        vector<wstring> svalues;
        vector<unsigned int> ivalues;
        while(getline(sin,bfr)){
            size_t arity = tok.llex(bfr,svalues);
            if(arity > 0){
                ivalues.clear();
                for(int i = 0; i < arity-1;++i){
                    ivalues.push_back(stoi(svalues[i]));
                }
                xdev_dataset.push_back(ivalues);
                ydev_lbls.push_back(stoi(svalues.back()));
            }
        }
        sin.close();
    }
}


void NumericData::remove_learner(WeakLearner<unsigned int> const &wl){
    available_templates[wl.wl_number]=false;
}

NumericData::NumericData(NumericData const &other){
    cerr << "attempt to copy Numeric Data (forbidden)\naborting."<<endl;
    exit(1);
}

NumericData& NumericData::operator=(NumericData const &other){
    cerr << "attempt to copy Numeric Data (forbidden)\naborting."<<endl;
    exit(1);
}
WeakLearner<unsigned int>* NumericData::get_learner(size_t idx) const{
    assert(idx < available_templates.size() && available_templates[idx]);
    vector<unsigned int> current_tpls = numeric_templates[idx];
    string d;
    for(int i  = 0; i < current_tpls.size();++i){
        d+= " "+to_string(current_tpls[i]);
    }
    WeakLearner<unsigned int> *wl = new WeakLearner<unsigned int>(d,ylab_set,idx);
    gen_examples(wl,true);
    return wl;
}



bool NumericData::has_next_wl()const{
    
    int nidx = next_index;
    ++nidx;
    while (nidx < numeric_templates.size() && !is_available(nidx)){
        ++nidx;
    }
    if (nidx >= numeric_templates.size()){
        return false;
    }else{
        return true;
    }
    
}

void NumericData::reset_learner_pool(){
    next_index = -1;
}


WeakLearner<unsigned int>* NumericData::get_next_wl(){
    
    WeakLearner<unsigned int> *wl;
    #pragma omp critical
    {
        ++next_index;
        while (next_index < numeric_templates.size() && !is_available(next_index)){
            ++next_index;
        }
        if(next_index <= numeric_templates.size()){
            assert(next_index < available_templates.size() && available_templates[next_index]);
            vector<unsigned int> current_tpls = numeric_templates[next_index];
            string d;
            for(int i  = 0; i < current_tpls.size();++i){
                d+= " "+to_string(current_tpls[i]);
            }
            wl = new WeakLearner<unsigned int>(d,ylab_set,next_index);

            clog << "going to generate examples" << endl;
            gen_examples(wl,true);
        }
    }
    return wl;
}



void NumericData::gen_examples(WeakLearner<unsigned int> *wl,bool train)const{
    
    
    vector<unsigned int> current_tpls = numeric_templates[wl->wl_number];

    if(train){
        for(int i = 0; i < xtrain_dataset.size();++i){
            //featurizes each data point and sends it to weak classifier
            vector<unsigned int> xsp;
            for(int j = 0; j < current_tpls.size();++j){
                if(xtrain_dataset[i][current_tpls[j]] == 1){
                    xsp.push_back(current_tpls[j]);
                }
            }
            wl->add_train_datapoint(xsp,ytrain_lbls[i]);
        }
    }else{
        
        for(int i = 0; i < xdev_dataset.size();++i){
            //featurizes each data point and sends it to weak classifier
            vector<unsigned int> xsp;
            for(int j = 0; j < current_tpls.size();++j){
                if(xdev_dataset[i][current_tpls[j]] == 1){
                    xsp.push_back(current_tpls[j]);
                }
            }
            wl->add_dev_datapoint(xsp,ydev_lbls[i]);
        }
    }
}



float NumericData::get_random_error()const{
    return 1 - (1.0/ylab_set.size());
}

void NumericData::add_templates(vector<unsigned int> const &selected_columns){
    //simplistic/dummy template creation function
    assert(selected_columns.size() < 4);

    //unigrams
    for(int i = 0; i < selected_columns.size();++i){
        vector<unsigned int> v = {selected_columns[i]};
        numeric_templates.push_back(v);
    }
    //bigrams
    if(selected_columns.size() > 1){
        for(int i = 0; i < selected_columns.size();++i){
            for(int j = i+1; j < selected_columns.size();++j){
                vector<unsigned int> v = {selected_columns[i],selected_columns[j]};
                numeric_templates.push_back(v);
            }
        }
    }
    //trigrams
    if(selected_columns.size() > 2){
        for(int i = 0; i < selected_columns.size();++i){
            for(int j = i+1; j < selected_columns.size();++j){
                for(int k = j+1; k < selected_columns.size();++k){
                    vector<unsigned int> v = {selected_columns[i],selected_columns[j],selected_columns[k]};
                    numeric_templates.push_back(v);
                }
            }
        }
    }
    //all templates are initially available
    available_templates = vector<bool>(numeric_templates.size(),true);
}



//////////////////// LOGGER //////////////////

BLOGGER::BLOGGER(string output_folder) {
    // prepare folder
    this->output_folder= _remove_trailing_slash(output_folder);
    if (!_parent_folder_exists(this->output_folder)) {
        clog << "Parent folder of output folder must exist! Exiting." << endl;
        exit(1);
    }
    // create, empty and create files
    mkdir(output_folder.c_str(), S_IRUSR | S_IWUSR | S_IXUSR);
    _empty_folder(output_folder);
    //cout << output_folder+"/log" << endl;
    ofstream logfile(output_folder+"/log");
    logfile.close();
    ofstream tplfile(output_folder+"/templates.tpl");
    tplfile.close();
    ofstream dfile (output_folder+"/descriptionParams.txt");
    dfile.close();
}

void BLOGGER::new_iteration(StrongLearner<ParseAction, ParseData> *sl,
                            WeakLearner<ParseAction> *wl,
                            ParseData &data) {

    alpha             = sl->alphas[sl->alphas.size()-1];
    boosted_acc_train = sl->boosted_acc_train;
    boosted_acc_dev   = sl->boosted_acc_dev;
    err               = wl->get_error(true); // unweighted
    werr              = wl->get_error(true, sl->get_weights()); // weighted

    wl_num            = wl->wl_number;
    ntemplates        = wl->size;
    //vector<FTIDX> ft_idces =
    //        data.tpl_info.get_learner_template_nums(wl->wl_number);


    fts=L"";
    for (FTIDX ft_num :data.tpl_info.get_learner_template_nums(wl->wl_number)) {
        if (!data.tpl_info.template_already_included(ft_num)) {
            fts+= data.tpl_info.describe_template(ft_num)+L"\n";
            total_ntemplates++;
        }

    }
    //fts=wl->describe();

    time_secs         = sl->last_iteration_duration;
    cumul_time_secs   += time_secs;
    iternum++;
}

void BLOGGER::initialise_params(ParseData *data, bool replacement) {

    group_used            = data->group;
    for (int i=0; i<data->tpl_info.skeleton.size(); ++i) {
        skeleton += ws2s(data->tpl_info.skeleton[i])+", ";
    }
    prerank_used          = data->prerank;
    defaultset_used       = data->default_set;
    k                     = data->kbest;
    duration_prerank_secs = data->duration_prerank; // maybe not yet calculated
    total_num_learners    = data->get_num_available_learners();
    use_replace           = replacement;

    // might be useful to write datasets used too
    // name of default set ?
    // + number of perceptron iterations ?
}

void BLOGGER::set_duration_prerank(unsigned int duration) {
    assert(output_folder!="");
    duration_prerank_secs = duration;
    string d = "Time taken to prerank      :   "+to_string(duration_prerank_secs)
            +" seconds\n";
    // print out to log file
    if (duration>0){
        ofstream logfile(output_folder+"/descriptionParams.txt", ios::app);
        logfile<< d;
        logfile.close();
    }
    clog << d;
}

/*
 * Print to file and to screen
 */

void BLOGGER::print_out_params() {
    assert(output_folder!="");
    string d="\nBoosting Experiment for Statistical Parsing\n\n";


    d+= "Train data                 :   "+dataset_train+"\n";
    d+= "Dev data                   :   "+dataset_dev+"\n";

    if (skeleton.length()!=0) {
        d+= "Skeleton used              :   "+skeleton+"\n";
        d+="                               ("+skeleton_file+")\n";
    }
    if (defaultset_used) {
        d+= "Default set used           :   "+default_file+"\n";
    }
    if (prerank_used)
        d+= "Preranking                 :   yes, with k = "+to_string(k)+"\n";

    else
        d+= "Preranking                 :   no\n";
    if (group_used)
        d+= "Grouping                   :   yes (backoff-style)\n";
    else
        d+= "Grouping                   :   no\n";
    if (max_num_tpls!=-1)
        d+= "Max size model (in tpls)   :   "+to_string(max_num_tpls)+"\n";
    if (use_replace)
        d+= "Replacement of templates   :   true\n";
    else
        d+= "Replacement of templates   :   false\n";
    d+="Training data size         :   "+to_string(proportion_data)+"\n";
    d+="Num perceptron iters       :   "+to_string(num_perc_iters)+"\n";
    d+="Number of processors used  :   "+to_string(num_procs)+"\n";

    //d+="\nTotal num learners         :   "+to_string(total_num_learners)+"\n";

    ofstream logfile(output_folder+"/descriptionParams.txt", ios::app);
    logfile<<d;
    logfile.close();

    clog << d;
}

void BLOGGER::write_num_learners(unsigned int num) {
    total_num_learners = num;
    string d="Total num learners         :   "+to_string(total_num_learners)+"\n";

    ofstream logfile(output_folder+"/descriptionParams.txt", ios::app);
    logfile<<d;
    logfile.close();
    clog << d;
}

void BLOGGER::print_iteration() {
    assert(output_folder!="");
    string d = to_string(iternum)+") ";
    d        += "WLnum:"+to_string(wl_num)+", ";
    d        += "SizeWL:"+to_string(ntemplates)+", ";
    d        += "TotalSizeModel:"+to_string(total_ntemplates)+", ";
    d        += "Err:"+to_string(err)+", ";
    d        += "WErr:"+to_string(werr)+", ";
    d        += "Alpha:"+to_string(alpha)+", ";
    d        += "BoostAccTrain:"+to_string(boosted_acc_train)+", ";
    d        += "BoostAccDev:"+to_string(boosted_acc_dev)+", ";
    d        += "H:"+to_string(entropy_weights)+", ";
    d        += "BiggestWeight:"+to_string(biggest_weight)+", ";
    d        += "Time(s):"+to_string(time_secs)+", ";
    d        += "TotalTime(s):"+to_string(cumul_time_secs);

    clog << d << endl;

    string l = to_string(iternum) +"\t";
    l        +=to_string(wl_num)+"\t";
    l        += to_string(ntemplates)+"\t";
    l        += to_string(total_ntemplates)+"\t";
    l        += to_string(err)+"\t";
    l        += to_string(werr)+"\t";
    l        += to_string(alpha)+"\t";
    l        += to_string(boosted_acc_train)+"\t";
    l        += to_string(boosted_acc_dev)+"\t";
    l        += to_string(entropy_weights)+"\t";
    l        += to_string(biggest_weight)+"\t";
    l        += to_string(time_secs)+"\t";
    l        += to_string(cumul_time_secs)+"\t";

    // headers for first iteration
    if (iternum==1) {
        ofstream logfile(output_folder+"/log", ios::app);
        logfile << "Iter\tWLnum\tSizeWL\tTotalSizeModel\t"
                << "Err\tWErr\tAlpha\tBoostAccTrain\tBoostAccDev\t"
                << "H\tBiggestWeight\t"
                << "Time(s)\tTotalTime(s)" << endl;
        logfile.close();
    }

    ofstream logfile(output_folder+"/log", ios::app);
    logfile << l << endl;
    logfile.flush();
    logfile.close();

    wofstream tplfile(output_folder+"/templates.tpl", ios::app);
    tplfile << fts;
    tplfile.flush();
    tplfile.close();

}

