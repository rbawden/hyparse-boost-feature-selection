#include "featureselection.h"
#ifdef ENABLE_PARALLEL
    #include <omp.h>
#endif


int main(){

    //MULTIPROCESSING SETUP
    #ifdef ENABLE_PARALLEL
        omp_set_num_threads(1); // just for global StdWrapper
     #endif

    //---------------- OPTIONS FEATURE SELECTOR ----------------
    string output_folder = "../rachel_experiments/15-06-15_boostadditive_ftball_1iteration";


    // parser options
    unsigned int beam_size = 4;
    unsigned int max_iterations_perceptron = 1;
    bool train_locally(false); // just for StdWrapper
    bool eval_locally(false); // just for Stdwrapper

    // feature options
    unsigned int max_conditions = 3; // max conditions in a feature template
    int nqueue = 4; // number of Q elements
    int nstack = 3; // number of S elements

    // heuristics - if additive_ss, then group_templates is necessarily false
    bool group_templates(false);
    bool additive_ss(true);

    // debug
    bool balance=false; // to balance classes (just for debugging boost)


    mkdir(output_folder.c_str(), S_IRUSR | S_IWUSR | S_IXUSR);


    // 1. ---------------- Load treebanks ----------------
    //Treebank train("../data/ftb20-train-ttd.tbk");
    //Treebank dev("../data/ftb20-dev-ttd.tbk");
    Treebank train("../data/ftball-train-ttd.tbk");
    Treebank dev("../data/ftball-dev-ttd.tbk");
    //Treebank train("../data/FRENCH/pred/ptb/train/treebank.tbk");
    //Treebank dev("../data/FRENCH/pred/ptb/dev/treebank.tbk");
    //Treebank train("/media/rachel/DOCUMENTS/Paris_7/Stage M2/Data/ftb20-devsmall.tbk");
    //Treebank dev("/media/rachel/DOCUMENTS/Paris_7/Stage M2/Data/one_example.tbk");
    //Treebank train("/media/rachel/DOCUMENTS/Paris_7/Stage M2/Data/one_example.tbk");
    //Treebank dev("/media/rachel/DOCUMENTS/Paris_7/Stage M2/Data/one_example.tbk");

    train.update_encoder();
    dev.update_encoder();

    // 2. ---------------- Define search space ----------------
    SearchSpace search_space(train.colnames, nqueue, nstack, max_conditions,
                             group_templates, additive_ss);

    wcout << ">> Search space created" << endl;

    // 3. ---------------- Create feature selector -----------------
    //               choose either StdWrapper or Boost

    Boost feature_selector(search_space, dev, train, output_folder,
                          max_iterations_perceptron, beam_size, true, balance);

/*
    StdWrapper feature_selector(search_space, dev, train, output_folder,
                                       max_iterations_perceptron, beam_size,
                                       train_locally, eval_locally, true);
*/


    // 4. ---------------- Run feature selection ----------------
    feature_selector.run(true);

    return 0;
}

/////////////////////// GENERAL FUNCTIONS ////////////////////////////////
// get number of combinations of n things into groups of k
unsigned nChoosek(unsigned n, unsigned k){
    if (k>n) return 0;
    if (k*2 > n) k = n-k;
    if (k==0) return 1;

    int result = n;
    for(int i=2; i<=k; ++i) {
        result*=(n-i+1);
        result/=i;
    }
    return result;
}

void empty_folder(string folder_path) {
    string delallmodels = "exec rm -r "+folder_path;
    int res = system(delallmodels.c_str());
    if (res!=0) {
        throw("Cannot delete all model files for some reason\n");
    }
    mkdir(folder_path.c_str(), S_IRUSR | S_IWUSR | S_IXUSR);
}

////////////////// LOG FUNCTIONS ///////////////////
LogFeatureSelection::LogFeatureSelection(string &output_folder, bool verbose)
    : _verbose(verbose), _output_folder(output_folder){
    _iteration=0;
}


void LogFeatureSelection::_start_log(string algo, bool train_locally,
                                     bool eval_locally, int beam_size,
                                     unsigned int max_iterations_perceptron,
                                     unsigned int nconditions,
                                     unsigned int ntemplates,
                                     unsigned int ntemplategroups,
                                     unsigned int devsize,
                                     unsigned int trainsize,
                                     bool group,
                                     bool additive_ss) {

    // open files
    _logfile.open(_output_folder+"/log");
    _templatefile.open(_output_folder+"/templates.tpl");

    string heuristics("");
    if (group)
        heuristics = "groups of templates (backoff)";
    else if (additive_ss)
        heuristics="additive search space";
    else
        heuristics="none";

    // write summary
    string summary = "------------------\nChosen algorithm : "+algo+"\n" +
            "Local training = "+to_string(train_locally)+"\n" +
            "Local evaluation = "+to_string(eval_locally)+"\n" +
            "Beam size = "+to_string(beam_size)+"\n" +
            "Max number of perceptron iterations = "+
            to_string(max_iterations_perceptron)+"\n"+
            "Dev size = "+to_string(devsize)+"\n"+
            "Train size = "+to_string(trainsize)+"\n\n"+
            "Heuristics = "+heuristics+"\n\n"+
            to_string(nconditions)+" conditions\n" +
            to_string(ntemplates)+" templates\n" +
            to_string(ntemplategroups)
            +" template groups\n------------------\n";
    if (_verbose)
        clog << summary;
    _logfile << summary;
    _logfile.flush();

    _start_clock();
}

LogFeatureSelection& LogFeatureSelection::operator=(const LogFeatureSelection
                                                    &other_log) {
    this->_start_time = other_log._start_time;
    this->_last_time = other_log._last_time;
    this->_iteration = other_log._iteration;
    this->_verbose = other_log._verbose;
    this->_output_folder = other_log._output_folder;
    this->_logfile.open(_output_folder+"/log");
    this->_templatefile.open(_output_folder+"/templates.tpl");
    return *this;
}

LogFeatureSelection::LogFeatureSelection(const LogFeatureSelection &other_log) {
    this->_start_time = other_log._start_time;
    this->_last_time = other_log._last_time;
    this->_iteration = other_log._iteration;
    this->_verbose = other_log._verbose;
    this->_output_folder = other_log._output_folder;
    this->_logfile.open(_output_folder+"/feature_selection.log");
    this->_templatefile.open(_output_folder+"/feature_selection.tpl");
    //this->_alphafile.open(_output_folder+"/feature_selection.alphas");
}

void LogFeatureSelection::_start_clock() {
    _start_time = time(0);
    _last_time = _start_time;
}

void LogFeatureSelection::_log_iteration(SearchSpace &ss,
                                         const float &acc,
                                         const float &acc_gain,
                                         vector<unsigned int> &ft_nums,
                                         const double &alpha,
                                         const float &boost_acc,
                                         const float &boost_train_acc,
                                         const float &exploss) {
    _iteration++;

    int ntemplates = ft_nums.size();
    for (unsigned int &ft_num : ft_nums) {
        _templatefile << ss._templates[ft_num].toString(ss._ttd) << endl;
    }
    _templatefile.flush();

    time_t ctime = time(0);
    int duration = ctime -_last_time;
    int total_duration = ctime - _start_time;
    _last_time = ctime;

    string outstr = to_string(_iteration) + ")"+
            "Acc="+to_string(acc) +
            ",Nfts="+to_string(ntemplates);
    if (exploss!=-1)
        outstr+= ", Exploss="+to_string(exploss);
    if (acc_gain!=-1)
        outstr += ",Acc gain="+to_string(acc_gain);
    if (alpha!=-1)
        outstr += ",Alpha="+to_string(alpha);
    if (boost_acc!=-1)
        outstr += ",Boost acc dev="+to_string(boost_acc);
    if (boost_train_acc!=-1)
        outstr += ",Boost acc train="+to_string(boost_train_acc);
    outstr += ",Srchspace="+to_string(ss._tgroups.size()+1);
    outstr += ",Time="+to_string(duration) +
            "/"+to_string(total_duration) + "\n";

    if (_verbose)
        clog << outstr;

    _logfile << outstr;
    _logfile.flush();
}

LogFeatureSelection::~LogFeatureSelection() {
    _logfile.close();
    _templatefile.close();
}

////////////////////// LOCAL SET FUNCTIONS ////////////////////////////
LocalSet::LocalSet(Treebank train, Treebank dev) {
    Treebank binary_train;
    Treebank binary_dev;
    train.transform(binary_train, true, 0, Treebank::NO_DECORATIONS);
    dev.transform(binary_dev, true, 0, Treebank::NO_DECORATIONS);
    binary_train.update_encoder();
    binary_dev.update_encoder();

    _grammar = SrGrammar(binary_train, binary_dev, false);
    _bintreebank = binary_train;
}


void LocalSet::add_example(LocalExample example) {
    _examples.push_back(example);
}

LocalExample& LocalSet::operator[](unsigned int idx) {
    return _examples[idx];
}

LocalSet& LocalSet::operator=(const LocalSet &other_data){
    this->_examples = other_data._examples;
    this->_weights = other_data._weights;
    this->_cumul_weights = other_data._cumul_weights;
    return *this;
}

LocalSet::LocalSet(const LocalSet &other_data){
    this->_examples = other_data._examples;
    this->_weights = other_data._weights;
    this->_cumul_weights = other_data._cumul_weights;
}

void LocalSet::initialise_weights() {

    _weights.empty();
    _cumul_weights.empty();
    for (int i=0; i<size(); ++i) {
        _weights.push_back(1/(float)size());
        //_weights[i]=_weights[i]; // NORMAL VERSION
        if (LOG)
            _weights[i]=log(_weights[i]); // LOG VERSION
        _cumul_weights.push_back((1/(float)size())+
                                      (i/(float)size()));
    }
}

void LocalSet::initialise_weights(std::map<unsigned int, float> &ex2weight) {

    _weights.empty();
    _cumul_weights.empty();
    if (LOG)
        _weights.push_back(log(ex2weight[0]));
    else
        _weights.push_back(ex2weight[0]);

    _cumul_weights.push_back(ex2weight[0]);
    for (int i=1; i<size(); ++i) {

        _weights.push_back(ex2weight[i]);
        _cumul_weights.push_back(ex2weight[i]+_cumul_weights[i-1]);
    }

    float sumweights = _cumul_weights[_cumul_weights.size()-1];

    // normalise
    if (LOG){
        _weights.push_back(log(ex2weight[0]/(float)sumweights));
    }
    else {

    }
    _cumul_weights.push_back(ex2weight[0]);

    for (int i=1; i<size(); ++i) {
        if (LOG) {
            _weights.push_back(log(ex2weight[i]/(float)sumweights));
            _cumul_weights.push_back(exp(_weights[i])+_cumul_weights[i-1]);
        }
        else {
             _weights.push_back(ex2weight[i]/(float)sumweights);
            _cumul_weights.push_back(_weights[i]+_cumul_weights[i-1]);
        }
    }

}

void LocalSet::recalculate_weights(set<unsigned int> &correct_examples,
                                   double &alpha) {
    double sum_weights=0;
    int correct = 0;
    for (int i=0; i<_weights.size(); ++i) {
        if (correct_examples.find(i) != correct_examples.end()) {
            // DO NOTHING WHEN CORRECT
            correct++;
            _weights[i] *= exp(alpha*0);
        }
        else {
            _weights[i] *= exp(alpha*1);

        }
        if (LOG)
            sum_weights += exp(_weights[i]); // LOG VERSION
        else
            sum_weights += _weights[i]; //NORMAL VERSION


    }
    // normalise and calculate cumulative weights
    if (LOG)
        _weights[0] -= sum_weights; // LOG VERSION
    else
        _weights[0] /=sum_weights; // NORMAL VERSION

    if (LOG)
        _cumul_weights[0] = exp(_weights[0]); // LOG VERSION
    else
    _cumul_weights[0] = _weights[0]; // NORMAL VERSION


    for (int i=1; i<_weights.size(); ++i) {
        if (LOG) {
            _weights[i] -= log(sum_weights); // LOG VERSION
            _cumul_weights[i] = exp(_weights[i])+_cumul_weights[i-1];
        }
        else {
            _weights[i] /= sum_weights; // NORMAL VERSION
            _cumul_weights[i] = _weights[i]+_cumul_weights[i-1];
        }

    }

    // print first 20 weights
    for (int i=0; i < 20; ++i) {
        wcout << _weights[i] << ",";
    }
    wcout << endl;
}

LocalSet LocalSet::resample_data() {

    if (_weights.size()!=_examples.size()) {
        cerr << "Error! Weights must be initialised to resample data" << endl;
        exit(1);
    }
    LocalSet resampled;

    //LocalExample local_example;
    for (int i=0; i<_examples.size(); ++i) {
        resampled.add_example(_examples[_resample_one_idx()]); // STOCK
    }

    return resampled;
}

void LocalSet::resample_idxs(unsigned int number) {
    // store the random numbers in sent2step2num
    sent2step2num.clear();

    unsigned int idx;
    unsigned int sent;
    unsigned int step;

    for (unsigned int i=0; i<number; ++i) {
        idx = _resample_one_idx();
        sent = std::get<0>(idx2sentstep[idx]);
        step = std::get<1>(idx2sentstep[idx]);
        if (sent2step2num[sent].empty()) {
            sent2step2num[sent] = std::map<unsigned int, unsigned int>();
        }
        sent2step2num[sent][step]++;
    }

}

void LocalSet::generate_examples_from_idxs(vector<FeatureTemplate> &fts) {
    _examples.clear(); // empty local examples

    map<tuple<unsigned int, unsigned int>, vector<unsigned int>> sentstep2exidxs;

    // create full sparse encoder (all possible templates)
    SparseEncoder sp_encoder = SparseEncoder();
    for (FeatureTemplate &ft : fts) {
        sp_encoder.add_template(ft);
        //wcout << "generating example with" << ft << endl;
    }

    StateSignature sig;
    vector<unsigned int> xvec(sp_encoder.ntemplates(),0);
    ParseAction outgoing_action;
    //ParseAction incoming_action;

    int trainsize = _bintreebank.size()*PERCENTAGE_DATA;
    int k=0;
    // stock local examples for trainset
    for (unsigned int i=0; i<trainsize; ++i) {
        InputDag input_sequence;
        tree_yield(_bintreebank[i], input_sequence);
        size_t Nseq = input_sequence.size();
        ParseDerivation deriv(_bintreebank[i], _grammar, input_sequence, false);
        for (unsigned int j=0; j<deriv.size()-1; ++j) {

            if (sent2step2num[i][j]>0) {
                //sentstep2exidxs[std::make_tuple(i, j)] = vector<unsigned int>();
                deriv[j]->get_signature(sig, &input_sequence, Nseq);
                outgoing_action = deriv[j+1]->get_incoming_action();
                xvec.clear();
                sp_encoder.encode(xvec, sig);
                for (int num=0; num<sent2step2num[i][j]; ++num) {
                    _examples.push_back(std::make_tuple(xvec, outgoing_action));
                    //sentstep2exidxs[std::make_tuple(i, j)].push_back(k);
                    k++;
                }
            }
        }
    }
    shuffle(); // shuffle them all up
}

unsigned int LocalSet::_resample_one_idx() {
    // get random number and calculate which example corresponds
    double rnd = (rand() / ((double)RAND_MAX))
            *_cumul_weights[_cumul_weights.size()-1];

    unsigned int local_example_idx = _get_idx_binsearch(rnd);

    return local_example_idx;
}

unsigned int LocalSet::_get_idx_binsearch(const double rnd, int first,
                                          int last) {
    // starting point
    if (last==-1)
        last = _cumul_weights.size()-1;
    // end
    if (first==last)
        return first;

    int midpoint = first + (last-first)/2;
    if (rnd > _cumul_weights[midpoint])
        return _get_idx_binsearch(rnd, midpoint+1, last);
    else
        return _get_idx_binsearch(rnd, first, midpoint);
}

unsigned int LocalSet::size() {
    return N;
}

void LocalSet::shuffle() {
    vector<unsigned int> idxes;
    for(int i = 0; i < _examples.size(); ++i){
      idxes.push_back(i);
    }

    random_shuffle(idxes.begin(),idxes.end());

    vector<LocalExample> new_examples;
    for(int i = 0; i < _examples.size(); ++i){
      new_examples.push_back(this->_examples[idxes[i]]);
    }
    _examples = new_examples;
}


////////////////// SEARCH SPACE ///////////////////

SearchSpace::SearchSpace(const vector<wstring> &lex_info,
                         const unsigned int &nqueue,
                         const unsigned int &nstack,
                         const unsigned int &n_max_conditions,
                         const bool &group,
                         const bool &additive_ss)
    :_group(group), _additive_ss(additive_ss),
      _n_max_conditions(n_max_conditions) {

    _ttd = TemplateTypeDefinition(lex_info);
    _ntemplates = 0;

    // if additive ss heuristic, never group!
    if (_additive_ss)
        _group = false;

    // initialise conditions & associate condition combo nums to template groups
    _initialise_conditions(lex_info, nqueue, nstack);
   _associate_cidx_combos_to_tgidx(_n_max_conditions);

    // initialise template & template group structures by pre-calculating sizes
    int num_t=0;
    for (int i=_n_max_conditions; i>0; --i)
        num_t += (nChoosek(_conditions.size(), i));
    int num_tg=0;
    if (_group)
        num_tg = (nChoosek(_conditions.size(), _n_max_conditions));
    else
        num_tg = num_t;
    _templates = vector<FeatureTemplate>(num_t);

    // for additive ss heuristic, only add unigram templates
    if (_additive_ss)
        _tgroups = vector<TemplateGroup>(_conditions.size());
    else
        _tgroups = vector<TemplateGroup>(num_tg);

    // create templates and associate with template groups at the same time
    _initialise_templates(_n_max_conditions);

}

// generate all possible conditions as wstrings
void SearchSpace::_initialise_conditions(vector<wstring> lex_info,
                                         const unsigned int nqueue,
                                         const unsigned int nstack) {

    // create and add all possible Q conditions
    for (int qnum = 0; qnum < nqueue; ++qnum) {
        for (int id = 0; id < lex_info.size(); ++id) {
            _conditions.push_back(L"q"+ to_wstring(qnum)+
                                  L"("+lex_info[id]+L")");
        }
    }
    // create and all possible S conditions
    for (int snum = 0; snum < nstack; ++snum) {
        _conditions.push_back(L"s"+to_wstring(snum)+
                              L"(t,c,_)"); // phrase category
        // only dtrs of s0 and s1 used
        if (snum != 2) {
            _conditions.push_back(L"s"+to_wstring(snum)+
                                  L"(r,c,_)"); // phrase category
            _conditions.push_back(L"s"+to_wstring(snum)+
                                  L"(l,c,_)"); // phrase category
        }
        for (int id = 0; id < lex_info.size(); ++id) {
            _conditions.push_back(L"s"+to_wstring(snum)+
                                  L"(t,h,"+ lex_info[id]+ L")");
            // only dtrs of s0 and s1 used
            if (snum != 2) {
                _conditions.push_back(L"s"+to_wstring(snum)+
                                      L"(r,h,"+ lex_info[id]+ L")");
                _conditions.push_back(L"s"+to_wstring(snum)+
                                      L"(l,h,"+ lex_info[id]+ L")");
            }
        }
    }
}

// associate condition combos (as vector of idx nums) to template group numbers
int SearchSpace::_associate_cidx_combos_to_tgidx(const int &k,
                                                 vector<unsigned int> included_cnums,
                                                 int start_index, int tg_num) {

    if (k==0)
        return tg_num;
    else {
        vector<unsigned int> new_included_cnums;
        for (int i=start_index; i<_conditions.size(); ++i) {
            // copy vector condition nums
            new_included_cnums = vector<unsigned int>(included_cnums);
            new_included_cnums.push_back(i);


            // associate all condition combos with the template number and
            // increase template number
            if (_group && new_included_cnums.size() == _n_max_conditions ) {
                for (vector<unsigned int> &combo: get_combos(new_included_cnums,
                                                     _n_max_conditions)) {
                    if (_ccombos2tgs.count(combo) == 0) {
                        _ccombos2tgs[combo] = vector<unsigned int>();
                    }
                    _ccombos2tgs[combo].push_back(tg_num);
                }
                tg_num++;
            }
            else if (!_group){
                if (_ccombos2tgs.count(new_included_cnums) == 0) {
                    _ccombos2tgs[new_included_cnums] = vector<unsigned int>();
                }
                _ccombos2tgs[new_included_cnums].push_back(tg_num);
                tg_num++;
            }
            tg_num = _associate_cidx_combos_to_tgidx(k-1, new_included_cnums,
                                                     i+1, tg_num);
        }
    }
    return tg_num;
}

// get all combinations of condition numbers given a maximum number of
// conditions in a combo k
vector<vector<unsigned int>> SearchSpace::get_combos(const vector<unsigned int> &cnums, int k,
                                            int start_index,
                                            vector<unsigned int> prefix) {
    vector<vector<unsigned int>> combos;
    vector<unsigned int> combo;
    if (k==0)
        return combos;
    for (int i=start_index; i<cnums.size(); ++i) {
        combo = vector<unsigned int>(prefix);
        combo.push_back(cnums[i]);
        combos.push_back(combo);
        for (vector<unsigned int> &new_combo : get_combos(cnums, k-1, i+1, combo)) {
            combos.push_back(new_combo);
        }
    }
    return combos;
}

// create all feature templates from combinations of conditions
void SearchSpace::_initialise_templates(int k, vector<unsigned int> prefix,
                                        vector<unsigned int> included_ft_nums,
                                        int start_index) {
    if (k==0)
        return;
    else {
        vector<unsigned int> new_prefix;
        vector<unsigned int> included_ft_nums2;
        for (int i=start_index; i<_conditions.size(); ++i) {
            included_ft_nums2 = included_ft_nums;
            new_prefix = vector<unsigned int>(prefix); // copy prefix (list of cond nums)

            new_prefix.push_back(i);
            _create_template(new_prefix, included_ft_nums2);

            included_ft_nums2.push_back(_ntemplates); // add ft num
            _initialise_templates(k-1, new_prefix, included_ft_nums2, i+1);
        }
    }
}

// create an individual template from a vector of condition numbers
// initialise template groups too (based on a list of included feature templates
void SearchSpace::_create_template(const vector<unsigned int> &condition_nums,
                                   vector<unsigned int> included_ft_nums) {

    wstring template_string = L"P("+ to_wstring(_ntemplates+1) + L") = ";
    int condition_nums_size = condition_nums.size();

    for (int i=0; i<condition_nums_size; ++i) {
        template_string += _conditions[condition_nums[i]];
        if (i<condition_nums.size()-1) template_string += L" & ";
    }

    FeatureTemplate ft(template_string, _ttd);
    _templates[_ntemplates] = ft;

    included_ft_nums.push_back(_templates.size());

    // associate templates with groups
    // for additive ss only associate unigram templates
    for (int tg_num : _ccombos2tgs[condition_nums]) {
        if (!(_additive_ss)) {
            _tgroups[tg_num].push_back(_ntemplates);
            _ntgs++;
        }
        else if (_additive_ss && condition_nums.size()==1) {
            _tgroups[_ntgs].push_back(_ntemplates);
            _ntgs++;
        }
    }
    _tidx2condsidx[_ntemplates] = condition_nums;
    _ccombos2tmps[condition_nums] = _ntemplates;
    _ntemplates++;
}

unsigned int SearchSpace::size() const {
    unsigned int size = _tgroups.size();
    return size;
}

SearchSpace& SearchSpace::operator=(const SearchSpace &other_space) {
    this->_conditions = other_space._conditions;
    this->_templates = other_space._templates;
    this->_tgroups = other_space._tgroups;
    this->_ntemplates = other_space._ntemplates;
    this->_ttd = other_space._ttd;
    this->_group = other_space._group;
    this->_n_max_conditions = other_space._n_max_conditions;
    this->_additive_ss = other_space._additive_ss;
    this->_ccombos2tgs = other_space._ccombos2tgs;
    this->_ccombos2tmps = other_space._ccombos2tmps;
    this->_tidx2condsidx = other_space._tidx2condsidx;
    this->_added_tidxs = other_space._added_tidxs;
    return *this;
}

SearchSpace::SearchSpace(const SearchSpace &other_space) {
    this->_conditions = other_space._conditions;
    this->_templates = other_space._templates;
    this->_tgroups = other_space._tgroups;
    this->_ntemplates = other_space._ntemplates;
    this->_ttd = other_space._ttd;
    this->_group = other_space._group;
    this->_n_max_conditions = other_space._n_max_conditions;
    this->_additive_ss = other_space._additive_ss;
    this->_ccombos2tgs = other_space._ccombos2tgs;
    this->_ccombos2tmps = other_space._ccombos2tmps;
    this->_tidx2condsidx = other_space._tidx2condsidx;
    this->_added_tidxs = other_space._added_tidxs;
}

bool SearchSpace::_template_included(unsigned int ft_num) {
    if (_added_tidxs.find(ft_num)!=_added_tidxs.end())
        return true;
    return false;
}

bool SearchSpace::_template_in_search_space(unsigned int ft_num) {
    for (vector<unsigned int> &ss_tg : _tgroups) {
        for (const unsigned int &ss_ft : ss_tg) {
            if (ss_ft==ft_num)
                return true;
        }
    }
    return false;
}

void SearchSpace::_increase_space(unsigned int last_ft_idx) {
    if (!_additive_ss)
        return;

    std::vector<unsigned int> cidxs;
    std::set<unsigned int> set_cidxs;
    unsigned int new_tidx;
    for (unsigned int tidx : _added_tidxs) {
        if (tidx != last_ft_idx){

            cidxs = _tidx2condsidx[last_ft_idx];
            cidxs.insert(cidxs.end(),
                         _tidx2condsidx[tidx].begin(),
                         _tidx2condsidx [tidx].end());
            set_cidxs = set<unsigned int>(cidxs.begin(), cidxs.end());

            if (set_cidxs.size()<= _n_max_conditions
                    && set_cidxs.size()>1
                    && set_cidxs.size()==cidxs.size()) {
                cidxs =  vector<unsigned int>(set_cidxs.begin(),set_cidxs.end());
                std::sort(cidxs.begin(), cidxs.end());

                new_tidx = _ccombos2tmps[cidxs];

                if (!_template_included(new_tidx) &&
                        !_template_in_search_space(new_tidx)) {
                    TemplateGroup tg;
                    tg.push_back(new_tidx);
                    _tgroups.push_back(tg);
                }
            }
        }
    }
}


////////////////////// SELECTION METHODS /////////////////////////
ForwardMethod::ForwardMethod(const SearchSpace &search_space,
                             string &output_folder,
                             const unsigned int &max_perceptron_iterations,
                             const unsigned int &beam_size,
                             bool verbose)
    : _search_space(search_space),
      _max_perceptron_iterations(max_perceptron_iterations),
      _beam_size(beam_size),
      _log(output_folder, verbose) {
}

void ForwardMethod::run(bool verbose) {
    if (_search_space._additive_ss)
        run(_search_space._templates.size(), verbose);
    else
        run(_search_space.size(), verbose);
}

void ForwardMethod::run(unsigned int max_iterations, bool verbose) {

    tuple<unsigned int, float> tgindex_acc;
    bool condition = true;
    unsigned int next_tg_idx;
    float next_acc;
    int i=0;
    do {
        ++i;

        tgindex_acc = _select_next(verbose);
        next_acc = std::get<1>(tgindex_acc);
        next_tg_idx = std::get<0>(tgindex_acc);

        if (next_acc > _threshold_acc) {

            vector<unsigned int> ft_nums;
            for (unsigned int &ft_num : _search_space._tgroups[next_tg_idx]) {
                if (! (_search_space._template_included(ft_num))) {
                    ft_nums.push_back(ft_num);;
                }
            }            
            _add_templates_to_final_model(ft_nums, next_acc);
            _remove_templates_from_search_space(next_tg_idx);
            _log_iteration(next_acc, ft_nums); // must be before threshold
            _reset_threshold(next_acc);

            if (_search_space._additive_ss) {
                assert(ft_nums.size()==0);
                _search_space._increase_space(ft_nums[0]);
            }
        }
        else
            condition=false;
        if (i==max_iterations)
            condition=false; // stop anyway if max iterations reached

    } while(condition);
}


void ForwardMethod::_remove_templates_from_search_space(unsigned int tg_index) {
    swap(_search_space._tgroups[tg_index],
         _search_space._tgroups.back()); // put template group to back
    _search_space._tgroups.pop_back(); // pop
}


////////////////////// STD FWARD /////////////////////////
StdWrapper::StdWrapper(const SearchSpace &search_space,
                                     const Treebank &dev,
                                     const Treebank &train,
                                     string &output_folder,
                                     unsigned &max_perceptron_iterations,
                                     unsigned int &beam_size,
                                     bool train_locally,
                                     bool eval_locally,
                                     bool verbose)
    : ForwardMethod(search_space, output_folder, max_perceptron_iterations,
                    beam_size, verbose),
      _dev(dev),
      _train(train),
      _train_locally(train_locally),
      _eval_locally(eval_locally) {

    _threshold_acc=0.;
    _log._start_log(algo, train_locally, eval_locally,
                    beam_size, max_perceptron_iterations,
                    search_space._conditions.size(),
                    search_space._templates.size(),
                    search_space._tgroups.size(),
                    _dev.size(), _train.size(),
                    _search_space._group,
                    _search_space._additive_ss);

}


tuple<unsigned int, float> StdWrapper::_select_next(bool verbose) {
    SparseEncoder temp_encoder;
    SrParser srp;
    unsigned int index_best(0);
    float acc(0.);
    float max_acc(0.);

    for (int i=0; i<_search_space.size(); ++i){

        clog << to_string(i+1)+"/"+to_string(_search_space.size());

        temp_encoder = _current_encoder; // reinitialise encoder
        _add_to_encoder(temp_encoder, i, verbose); // add template group

        srp = SrParser(temp_encoder);
        _train_model(srp, verbose);
        acc = _evaluate_model(srp, verbose);

        if (verbose)
            clog << "acc = "<< acc << endl;

        // stock if better than past max_acc
        if (acc>max_acc){
            max_acc = acc;
            index_best = i;
        }
        clog << "\r";
    }
    clog << endl;

    return std::make_tuple(index_best, max_acc);
}


void StdWrapper::_train_model(SrParser &srp, bool verbose) {
    if (_train_locally) {
        srp.train_local_model(_train, _dev, Treebank::NO_DECORATIONS,
                              _beam_size, _max_perceptron_iterations, verbose);
    }
    else {
        srp.train_global_minibatch_model(_train, _dev, Treebank::NO_DECORATIONS,
                                         _beam_size, _max_perceptron_iterations,
                                         32, SrParser::MAXV_UP,
                                         SrParser::PERCEPTRON,
                                         1, 1,verbose,true);
//        srp.train_global_model(_train, _dev, Treebank::NO_DECORATIONS,
//                               _beam_size, _max_perceptron_iterations,
//                               SrParser::MAXV_UP,
//                               SrParser::PERCEPTRON,
//                               1, 1,verbose,true);
    }
}

float StdWrapper::_evaluate_model(SrParser &srp, bool verbose) {
    float acc;
    if (_eval_locally) {
        acc = srp.eval_local_model(_dev);
    }
    else {
        acc = std::get<0>(srp.eval_model(_dev));
        //float f = get<2>(srp.eval_model(_dev));
        //wcout << "f = " << f << endl;
    }
    return acc;
}

void StdWrapper::_update_current_encoder(vector<unsigned int> &ft_nums) {
    for (unsigned int &ft_num: ft_nums) {
        _current_encoder.add_template(ft_num);
    }
}

void StdWrapper::_add_templates_to_final_model(vector<unsigned int> &ft_nums,
                                                      float) {
    for (unsigned int &ft_num: ft_nums) {
        _search_space._added_tidxs.insert(ft_num);
        _current_encoder.add_template(_search_space._templates[ft_num]);
    }
}

void StdWrapper::_reset_threshold(float &acc) {
    _threshold_acc=acc;
}

void StdWrapper::_add_to_encoder(SparseEncoder &temp_encoder,
                                 unsigned int tg_index, bool verbose) {

    for(int ft_num: _search_space._tgroups[tg_index]) {
        if (! (_search_space._template_included(ft_num))) {
            temp_encoder.add_template(_search_space._templates[ft_num]);
        }
    }
    if (verbose) {
        wcout << L"\nModel to be trained : " << endl;
        wcout << temp_encoder.toString(_search_space._ttd);
    }
}

void StdWrapper::_log_iteration(float next_acc,
                                   vector<unsigned int> &ft_nums) {

    float acc_diff = next_acc-_threshold_acc;
    _log._log_iteration(_search_space, next_acc, acc_diff, ft_nums);
}

////////////////////// BOOST /////////////////////////


/*---------------------------------------------------------------------------*/

Boost::Boost(const SearchSpace &search_space,
             Treebank &dev,
             Treebank &train,
             string &output_folder,
             unsigned int &max_perceptron_iterations,
             unsigned int &beam_size,
             bool verbose, bool balance)
    : ForwardMethod(search_space, output_folder, max_perceptron_iterations,
                    beam_size, verbose),
    _balance(balance){


    // 1) get local data (in _dev and _train) and initialise train weights
    _prepare_local_data(dev, train);

    if (balance)
        _train.initialise_weights(ex2weight); // DEBUG - even distribution
    else
        _train.initialise_weights();


    // 2) Set threshold (when to add new features)
    _threshold_acc = 1/(float)_nactions;

    // 3) Prepare log
    _log._start_log(algo, true, true,
                    beam_size, max_perceptron_iterations,
                    search_space._conditions.size(),
                    search_space._templates.size(),
                    search_space._tgroups.size(),
                    _dev.size(), _train.size(),
                    search_space._group,
                    search_space._additive_ss);

    // 4) stock model info
    ofstream beamout(_log._output_folder+"/beam");
    beamout << _beam_size << endl;
    beamout.close();
    _grammar.save(_log._output_folder+"/grammar");
    _search_space._ttd.save(_log._output_folder+"/ttd");
    string modelfolder = _log._output_folder+"/models";
    mkdir(modelfolder.c_str(), S_IRUSR | S_IWUSR | S_IXUSR);
    empty_folder(modelfolder);
    IntegerEncoder *enc = IntegerEncoder::get();
    enc->save(_log._output_folder+"/encoder");

    srand(time(NULL)); // important for random sampling
}

/*---------------------------------------------------------------------------*/
unsigned int _get_argmax(vector<float> scores,size_t Vs){
    float mmax = scores[0];
    unsigned int amax = 0;

    for(int j = 1; j <Vs ;++j){
        if(scores[j] > mmax){
            mmax = scores[j];
            amax = j;
        }
    }
    return amax;
}

/*---------------------------------------------------------------------------*/

SparseFloatMatrix* Boost::_train_local_model(vector<unsigned int> &ft_nums,
                                      bool verbose) {
    SparseFloatMatrix *parsing_model =
            new SparseFloatMatrix(KERNEL_SIZE, _grammar.num_actions());
    SparseFloatMatrix *averaged_model =
            new SparseFloatMatrix(KERNEL_SIZE, _grammar.num_actions());

    vector<FeatureTemplate> fts;
    for (unsigned int ft_num: ft_nums)
        fts.push_back(_search_space._templates[ft_num]);
    // generate xvecs from random idxs
    _train.generate_examples_from_idxs(fts);

    int loss;
    float C = 1;

    if (verbose) {
        wcout << "\nModel trained = \n";
        for (unsigned int ft_num : ft_nums) {
            wcout<<L"\t"+
                   _search_space._templates[ft_num].toString(_search_space._ttd)
                << endl;
        }
    }

    for (unsigned int e = 1; e < _max_perceptron_iterations+1;++e) {
        loss=0;
        _train.shuffle();

        for (int i=0; i<_train.size();++i) {
            loss += _local_learn_one(_train[i], *averaged_model,
                                     parsing_model, C);
            C++;
        }
        if (verbose)
            wcout << L"Iteration "+to_wstring(e)+L", loss="+to_wstring(loss)
                  << endl;
    }
    //Final averaging
    (*averaged_model) *= 1.0/C;
    (*parsing_model) -= (*averaged_model);
    delete averaged_model;
    return parsing_model;
}

/*---------------------------------------------------------------------------*/

int Boost::_local_learn_one(const LocalExample &local_example,
                                     SparseFloatMatrix &averaged_model,
                                     SparseFloatMatrix *parsing_model,
                                     int C) {

    vector<unsigned int> xvec = std::get<0>(local_example);
    ParseAction outgoing_action = std::get<1>(local_example);
    vector<float> y_scores(_grammar.num_actions(),0.0);
    parsing_model->dot(xvec, y_scores);//, ft_nums);
    //_grammar.select_actions(y_scores, incoming_action, sig);

    unsigned int argmax = _get_argmax(y_scores,y_scores.size());

    //update (if wrong)
    if(_grammar[argmax] != outgoing_action){
        SparseFloatVector lxvec(xvec,KERNEL_SIZE);
        parsing_model->subs_rowY(lxvec,argmax);
        parsing_model->add_rowY(lxvec,
                                _grammar.get_action_index(outgoing_action));

        //averaging
        lxvec *= C;
        averaged_model.subs_rowY(lxvec,argmax);
        averaged_model.add_rowY(lxvec,
                                _grammar.get_action_index(outgoing_action));
        //wcout << "got it wrong" << endl;
        return 1;
    }
    return 0;
}

/*---------------------------------------------------------------------------*/

tuple<float, set<unsigned int>, std::vector<unsigned int>> Boost::_eval_local(vector<unsigned int> &ft_nums,
                                                    SparseFloatMatrix *parsing_model) {

    SparseEncoder sp_encoder = SparseEncoder();
    for (unsigned int &ft : ft_nums) {
        sp_encoder.add_template(_search_space._templates[ft]);
    }

    StateSignature sig;
    vector<unsigned int> xvec(sp_encoder.ntemplates(),0);
    ParseAction outgoing_action;

    float correct=0;
    set<unsigned int> correct_examples;
    unsigned int nidx=0;
    int trainsize = _train._bintreebank.size()*PERCENTAGE_DATA;
    std::vector<unsigned int> trainex2action;

    float weights_correct=0;
    float weights_incorrect=0;
    int numcorrect=0;

    for (int i=0; i<trainsize; ++i) {
        InputDag input_sequence;
        tree_yield(_train._bintreebank[i], input_sequence);
        size_t N = input_sequence.size();
        ParseDerivation deriv(_train._bintreebank[i], _grammar, input_sequence,
                              false);

        for (int j=0; j<deriv.size()-1; ++j) {
            deriv[j]->get_signature(sig, &input_sequence, N);
            outgoing_action = deriv[j+1]->get_incoming_action();
            xvec.clear();

            sp_encoder.encode(xvec, sig);
            vector<float> y_scores(_grammar.num_actions(),0.0);;
            parsing_model->dot(xvec, y_scores);
            unsigned int argmax = _get_argmax(y_scores,y_scores.size());

            trainex2action.push_back(argmax);

            if(_grammar[argmax] == outgoing_action) {
                if (LOG)
                    correct += exp(_train._weights[nidx]);
                else
                    correct += _train._weights[nidx];

                correct_examples.insert(nidx);
                weights_correct+=_train._weights[nidx];
                numcorrect++;
            }
            else
                weights_incorrect+=_train._weights[nidx];
            nidx++;
        }
    }
    float acc = correct/
                (float)_train._cumul_weights[_train._cumul_weights.size()-1];

    return std::make_tuple(acc, correct_examples, trainex2action);
}

/*---------------------------------------------------------------------------*/

// Evaluate one training example (from example num)
// return 1 if correct, 0 otherwise
int Boost::_local_eval_one(const int example_num,
                           SparseFloatMatrix* parsing_model,
                           vector<unsigned int> &ft_nums){

    LocalExample local_example = _train[example_num];

    vector<unsigned int> xvec;
    for (unsigned int &ft_num : ft_nums) {
        xvec.push_back(std::get<0>(local_example)[ft_num]);
    }

    ParseAction outgoing_action = std::get<1>(local_example);

    vector<float> y_scores(_grammar.num_actions(),0.0);

    parsing_model->dot(xvec, y_scores);//, ft_nums);
    //_grammar.select_actions(y_scores, incoming_action, sig);

    unsigned int argmax = _get_argmax(y_scores,y_scores.size());

    if(_grammar[argmax] != outgoing_action)
        return 0; // incorrect
    else {
        _train._correct_examples.insert(example_num);
        return 1; // correct
    }
}

/*---------------------------------------------------------------------------*/

tuple<unsigned int, float> Boost::_select_next(bool verbose) {

    unsigned int index_best(0);
    tuple<float, set<unsigned int>, std::vector<unsigned int>>
            acc_correctexs_ex2actions =
            std::make_tuple(0,set<unsigned int>(), std::vector<unsigned int>());
    float acc(0.);
    float max_acc(0.);
    vector<unsigned int> ft_nums;
    _train._correct_examples.clear(); // start afresh
    SparseFloatMatrix* parsing_model;
    std::vector<unsigned int> best_ex2actions;

    _train.resample_idxs(_train.N); // get random local idxs

    for (int i=0; i<_search_space.size(); ++i){
        clog << to_string(i+1)+"/"+to_string(_search_space.size());
        if (verbose)
            clog << "\n";

        ft_nums.clear();
        for(unsigned int &ft_num: _search_space._tgroups[i]) {
            if (! (_search_space._template_included(ft_num)))
                    ft_nums.push_back(ft_num);
        }
        //parsing_model = _train_local_model(resampled,ft_nums, verbose);
        parsing_model = _train_local_model(ft_nums, verbose);
        acc_correctexs_ex2actions = _eval_local(ft_nums, parsing_model);
        acc = std::get<0>(acc_correctexs_ex2actions);

        if (verbose)
            wcout << "acc = " << acc << endl;

        // stock if better than past max_acc
        if (acc>max_acc){
            max_acc = acc;
            index_best = i;
            _train._correct_examples = std::get<1>(acc_correctexs_ex2actions);
            best_ex2actions = std::get<2>(acc_correctexs_ex2actions);

            if (i!=0)
                delete _best_parse_model;  // check this!!!
            _best_parse_model = parsing_model;
        }
        else {
            delete parsing_model;
        }
        clog << "\r";
    }
    clog << endl;

    // add argmaxes
    for (int i=0; i<best_ex2actions.size(); ++i) {
        _train.ex2actions[i].push_back(best_ex2actions[i]);
    }

    return std::make_tuple(index_best, max_acc);
}

/*---------------------------------------------------------------------------*/

void Boost::_add_templates_to_final_model(vector<unsigned int> &ft_nums,
                                          float acc) {

    //wcout << "adding templates to final model" << endl;
    for (unsigned int &ft_num: ft_nums) {
        // only add when the template is not already there
        if (! (_search_space._template_included(ft_num))) {
            _search_space._added_tidxs.insert(ft_num);
        }
    }
    // calculate and stock alpha
    double alpha = calculate_alpha(acc);
    _alphas_ntemps.push_back(std::make_tuple(alpha, ft_nums.size()));

    _train.recalculate_weights(_train._correct_examples, alpha);

    // get argmaxes for each example
    _get_argmaxes_on_devset(ft_nums, _best_parse_model);
    //_dump_model(ft_nums);

}
/*---------------------------------------------------------------------------*/

double Boost::calculate_alpha(float acc) {
    //wcout << "num actions = " << _nactions << endl;
    double alpha = (log(acc/(1.-acc)) + log(_nactions-1));
    return alpha;
}

/*---------------------------------------------------------------------------*/

void Boost::_log_iteration(float next_acc, vector<unsigned int> &ft_nums) {
    float alpha = std::get<0>(_alphas_ntemps[_alphas_ntemps.size()-1]);

    // calculate boosted predict and exponential loss
    tuple<float, float> accAndExploss;
    float exploss_train= 0;
    float acc_dev = 0;
    float acc_train = 0;
    acc_dev = std::get<0>(boost_predict(_dev.ex2actions, _dev.gold_actions));
    accAndExploss = boost_predict(_train.ex2actions, _train.gold_actions);
    acc_train = std::get<0>(accAndExploss);
    exploss_train = std::get<1>(accAndExploss);

    // log the whole iteration
    _log._log_iteration(_search_space, next_acc, -1, ft_nums, alpha, acc_dev,
                        acc_train, exploss_train);
    delete _best_parse_model;

}

/*---------------------------------------------------------------------------*/

void Boost::_get_argmaxes_on_devset(vector<unsigned int> &ft_nums,
                                    SparseFloatMatrix *parsing_model) {
    // generate derivation and for each local example, get argmax

    // create sparse encoder
    SparseEncoder sp_encoder = SparseEncoder();
    for (unsigned int &ft : ft_nums) {
        sp_encoder.add_template(_search_space._templates[ft]);
    }

    unsigned int exnum=0;
    StateSignature sig;
    vector<unsigned int> xvec(sp_encoder.ntemplates(),0);

    wcout << "about to get argmaxes, devsize = " << _dev._bintreebank.size()
          << endl;

    int devsize = _dev._bintreebank.size()*PERCENTAGE_DATA;
    for (int i=0; i<devsize; ++i) {
        InputDag input_sequence;
        tree_yield(_dev._bintreebank[i], input_sequence);
        size_t N = input_sequence.size();
        ParseDerivation deriv(_dev._bintreebank[i], _grammar, input_sequence,
                              false);

        for (int j=0; j<deriv.size()-1; ++j) {
            deriv[j]->get_signature(sig, &input_sequence, N);
            //outgoing_action = deriv[j+1]->get_incoming_action();
            xvec.clear();

            sp_encoder.encode(xvec, sig);
            vector<float> y_scores(_grammar.num_actions(),0.0);
            parsing_model->dot(xvec, y_scores);

            unsigned int argmax = _get_argmax(y_scores,y_scores.size());
            _dev.ex2actions[exnum].push_back(argmax);
            exnum++;

        }
    }
}

/*---------------------------------------------------------------------------*/
/* Perform a boosted prediction based on the predictions of each of the weak
 * models (stocked in ex2actions) and validated on the vector of gold actions.
 * Uses the alpha coefficients assigned to each model.
 *
 * ex2actions : vector of size N containing for each example 1...N
       a vector of size J where J is the number of weak models added.
       Each element of the vector corresponds to the action predicted by
       the jth model (from 1 to J)
 *
 * gold_actions : N-sized vector of gold parse actions for examples 1...N
*/
tuple<float,float> Boost::boost_predict(vector<vector<unsigned int>> ex2actions,
                                 vector<ParseAction> gold_actions) {

    map<unsigned int, float> action2score;

    int exnum=0;
    float ncorrect = 0;
    float sumcorrect = 0.;
    // for each example
    for (int i=0; i<gold_actions.size(); ++i) {
        action2score.clear();
        unsigned int gold_action = gold_actions[exnum].action_code;

        for (int j=0; j<ex2actions[i].size(); ++j) {
            if (action2score.count(ex2actions[i][j])==0) {
                action2score[ex2actions[i][j]] = 0;
            }
            action2score[ex2actions[i][j]] += std::get<0>(_alphas_ntemps[j]);
        }
        // get the action with the highest weighted score
        unsigned int max_action(0);
        float max_score = -1;
        for (auto iter : action2score) {
            if (iter.second >max_score) {
                max_score = iter.second;
                max_action = iter.first;
            }
        }
        // evaluate
        if (gold_action==max_action) {
            if (_balance) {
                if (action2number[gold_action]!=0)
                    ncorrect += 1/(float)action2number[gold_action];
            }
            else
                ncorrect++;
        }
        if (_balance)
            sumcorrect += 1/(float)action2number[gold_action];
        exnum++;
    }

    // Calculate exponential loss
    float k = (float)_nactions;
    // correct examples
    float exploss = ncorrect *
            exp((-1/k) * (1 + pow((-1/(k-1)),2)*(k-1) ));

    // incorrect examples
    exploss += (ex2actions.size()-ncorrect) *
                exp(
                    (-1/k) *
                    (
                      (-2/(k-1)) +
                        ( pow((-1/(k-1)), 2) * (k-2))
                    )
                );
    float acc;
    if (_balance)
        acc = ncorrect/sumcorrect;
    else {
        acc = ncorrect/(float)ex2actions.size();
    }

    return std::make_tuple(acc, exploss);
}


/*---------------------------------------------------------------------------*/

void Boost::_dump_model(vector<unsigned int> &ft_nums) {
    string output_folder =
            _log._output_folder+"/models/"+to_string(_alphas_ntemps.size());

    mkdir(output_folder.c_str(), S_IRUSR | S_IWUSR | S_IXUSR);

    _best_parse_model->save(output_folder+"/model");
    wofstream tpls(output_folder+"/templates");
    for (unsigned int &ft_num : ft_nums) {
        tpls << _search_space._templates[ft_num].toString(_search_space._ttd)
             << endl;
    }
    tpls.close();
    ofstream alphafile(output_folder+"/alpha");
    alphafile << std::get<0>(_alphas_ntemps[_alphas_ntemps.size()-1]);
    alphafile.close();

}

/*---------------------------------------------------------------------------*/
/* prepare train and dev data, associating sentence+parse steps to example
 numbers for later use
 */
void Boost::_prepare_local_data(Treebank &dev, Treebank &train) {

    Treebank binary_train;
    Treebank binary_dev;
    train.transform(binary_train, false, 0, Treebank::NO_DECORATIONS);
    dev.transform(binary_dev, false, 0, Treebank::NO_DECORATIONS);
    binary_train.update_encoder();
    binary_dev.update_encoder();
    _grammar = SrGrammar(binary_train, binary_dev, false);

    _train._bintreebank = binary_train;
    _dev._bintreebank = binary_dev;
    _train._grammar = _grammar;
    _dev._grammar = _grammar;

    //ParseAction incoming_action;
    StateSignature sig;
    ParseAction outgoing_action;

    int trainsize = _train._bintreebank.size()*PERCENTAGE_DATA;
    int devsize = _dev._bintreebank.size()*PERCENTAGE_DATA;

    // associate sentence and step numbers to example idxs
    // stock gold actions for train
    for (int i=0; i<trainsize; ++i) {

        InputDag input_sequence;
        tree_yield(_train._bintreebank[i], input_sequence);
        size_t Nseq = input_sequence.size();
        ParseDerivation deriv(_train._bintreebank[i], _train._grammar,
                              input_sequence, false);
        for (int j=0; j<deriv.size()-1; ++j) {

            _train.idx2sentstep[_train.N] = std::make_tuple(i, j);
            _train.N++;

            // stock gold actions for train
            deriv[j]->get_signature(sig, &input_sequence, Nseq);
            outgoing_action = deriv[j+1]->get_incoming_action();
            _train.gold_actions.push_back(outgoing_action);

            action2number[outgoing_action.get_action_code()]++;
        }
    }

    // stock local examples for trainset
    for (int i=0; i<devsize; ++i) {

        InputDag input_sequence;
        tree_yield(_dev._bintreebank[i], input_sequence);
        size_t Nseq = input_sequence.size();
        ParseDerivation deriv(_dev._bintreebank[i], _dev._grammar,
                              input_sequence, false);
        for (int j=0; j<deriv.size()-1; ++j) {
            _dev.N++;
            deriv[j]->get_signature(sig, &input_sequence, Nseq);
            outgoing_action = deriv[j+1]->get_incoming_action();
            _dev.gold_actions.push_back(outgoing_action);
            action2number[outgoing_action.get_action_code()]++;
        }
    }
    _nactions = _train._grammar.num_actions();

    for (auto iter: action2number) {
        clog << _grammar[iter.first] << endl;
    }

//    clog << _grammar.num_actions() << endl;
//    clog << action2number.size() << endl;
//    sleep(10);

    // initialise structures to stock predicted actions for each example
    _dev.ex2actions = vector<vector<unsigned int>>(_dev.size());
    _train.ex2actions = vector<vector<unsigned int>>(_train.size());

    for (int i=0; i<_train.gold_actions.size(); ++i) {
        float denom =action2number[_train.gold_actions[i].get_action_code()]
                *_nactions;
        ex2weight[i] = 1/denom;
    }
}






