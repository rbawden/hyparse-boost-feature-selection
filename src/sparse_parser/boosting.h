#ifndef BOOSTING_H
#define BOOSTING_H
#include "sparse.h"
#include "sparse_encoder.h"
#include <map>
#include <math.h>
#include <fstream>
#include <unistd.h>
#include <ostream>
#include <iostream>
#include <sys/stat.h>
#include <numeric>
#include <algorithm>

typedef unsigned int XSP;
typedef unsigned int YLABEL;
typedef unsigned int EXIDX; // example index
typedef unsigned int FTIDX; // feature template index
typedef unsigned int WLIDX; // weak learner index (also template group index)

template <typename Y, typename X> class StrongLearner;

//==========================================================
//utility functions
vector<unsigned int> sample_data(const vector<double> &data_weights,
                                 unsigned int sample_size);
unsigned int resample_one_idx(const vector<double> &cumul_weights);

unsigned int get_idx_binsearch(const vector<double> &cumul_weights,
                               const double rnd, int first=0, int last=-1);

unsigned int _argmax(vector<float> const &scores,size_t Vs);

bool _file_exists(const string &fileName);

void _empty_folder(string &folder_path);

string _remove_trailing_slash(string &folder);

string ws2s(wstring &wstr); // convert wstring to string

//==========================================================
template <typename Y>
class WeakLearner {

public:
    friend class BLOGGER;
    WeakLearner(){}
    ~WeakLearner();
    WeakLearner(string const &description,vector<Y> const &action_list,int num);
    WeakLearner(WeakLearner<Y> const &other);
    WeakLearner<Y>& operator=(WeakLearner<Y> const &other);

    float get_error(bool train, vector<double> weights)const;
    float get_error(bool train)const; // unweighted

    //data set (removes the dependency on templates)
    void add_train_datapoint(vector<XSP> const &xvals, Y ylbl);
    void add_dev_datapoint(vector<XSP> const &xvals, Y ylbl);
    void add_train_datapoint(vector<XSP> const &xvals, Y ylbl,vector<float> const &constraints);
    void add_dev_datapoint(vector<XSP> const &xvals, Y ylbl,vector<float> const &constraints);
    
    // assign properties
    inline size_t train_size()const{return xvec_train.size();}
    inline size_t dev_size()const{return xvec_dev.size();}

    void set_size(unsigned int size) {this->size = size;}
    
    // train
    void train(unsigned int nepochs,vector<unsigned int> &samp_idces);

    // eval
    void predict(bool train);

    // predictions
    Y& prediction_at(EXIDX idx, bool train);
    bool is_correct_at(EXIDX idx, bool train)const;
    string describe()const;

    /* identify weak learner by number (corresponds for example to template
        group index in ParseData - means that we can identify corresponding
        templates without having to stock them here */
    unsigned int wl_number;

    //constrains the predictions with the constraint vector
    void apply_constraints(vector<float> &predictions,vector<float> &constraints);
    
private:
    
    Bimap<Y> ylabels;
    SparseFloatMatrix* parsing_model;
    string description;
    unsigned int size;
    
    vector<vector<XSP>> xvec_train;
    vector<vector<XSP>> xvec_dev;
    vector<vector<float>> train_constraints;
    vector<vector<float>> dev_constraints;
    vector<Y> ys_train, ys_dev;
    vector<Y> predictions_train, predictions_dev;
    double werr;
};
//==========================================================
class TemplateInfo {
public:
    friend class ParseData;
    friend class BLOGGER;

    TemplateInfo();
    ~TemplateInfo();
    TemplateInfo(TemplateInfo const &other);
    TemplateInfo& operator=(TemplateInfo const &other);

    const FeatureTemplate &get_template(FTIDX ftidx) const;
    wstring describe_template(FTIDX ftidx) const;
    unsigned int get_size_wl(WLIDX idx) const;
    unsigned int get_idx_next(unsigned int current_learner_num) const;
    vector<FeatureTemplate> get_learner_templates(WLIDX idx) const;
    vector<FTIDX> get_learner_template_nums(WLIDX idx) const;

private:

    int associate_conds_to_tgidx(bool group, const int &k=3,
                                 vector<unsigned int> included_cnums={},
                                 int start_index=0, int tg_num=0);
    vector<vector<unsigned int>> get_combos(const vector<unsigned int> &cnums,
                                            int k, int start_index=0,
                                            vector<unsigned int> prefix={});
    void generate_conditions(vector<wstring> lex_info);
    void add_condition(wstring str_condition, wstring position);
    void generate_templates(int k=3, vector<unsigned int> prefix={},
                            int start_index=0);
    void create_template(const vector<unsigned int> &condition_nums);
    bool position_in_skeleton(wstring position);
    bool template_already_included(FTIDX idx); // check if already included
    void remove_template(FTIDX idx);

    vector<wstring> conditions;
    vector<FeatureTemplate> templates;
    int ntemplates=0;
    vector<vector<FTIDX>> template_groups; // indices of feature templates
    vector<bool> available_templates;
    vector<bool> available_learners;
    set<FTIDX> included_templates;
    TemplateTypeDefinition ttd;
    vector<wstring> skeleton;
    std::map<vector<unsigned int>,vector<unsigned int>> ccombos2tgs;
    vector<WLIDX> sorted_indices;
};


//==========================================================
class ParseData{

public:
    friend class BLOGGER;
    ParseData(Treebank train, Treebank dev);
    ~ParseData();
    ParseData(ParseData const &other);
    ParseData& operator=(ParseData const &other);

    //================ DATA ================
    void gen_examples(WeakLearner<ParseAction> *wl, bool train) const;
    float get_random_error();
    vector<ParseAction> get_gold_ys_train();
    vector<ParseAction> get_gold_ys_dev();
    unsigned int nexamples_train=0;
    unsigned int nexamples_dev=0;
    SrGrammar grammar;
    void dump_ordered_classifiers(string output_file);
    void prerank_classifiers();
    //================ TPLS ================
    // set heuristics to reduce search space
    // mutually exclusive (last one set is the option used)
    void set_skeleton(string template_guide_file);
    void set_default_set(string template_guide_file);
    void no_restrictions();

    // other heuristics
    void use_prerank(unsigned int topntemplates, bool local, string folder);
    void use_prerank_from_file(unsigned int topntemplates, string &filepath);
    inline unsigned int get_duration_prerank(){return duration_prerank;}
    void set_grouping(bool groupparam); // backoff templates

    // then once option set
    void generate_candidates();
    //unsigned int get_num_learners()const;
    bool is_available(size_t idx);
    unsigned int get_num_available_learners() const;
    unsigned int get_num_accessible_learners() const;
    void load_ordered_classifiers();
    
    
    WeakLearner<ParseAction> *get_next_wl();
    WeakLearner<ParseAction>* get_learner(size_t idx)const; // problem
    bool has_next_wl();
    void learner_chosen(WeakLearner<ParseAction>* wl, bool remove);
    void remove_learner(WeakLearner<ParseAction> *wl);
    void remove_learner(WeakLearner<ParseAction> const &wl);

    string describe_next_wl(WLIDX wl_idx) const;
    string describe_params();

private:

    void sort_classifiers_by_performance();

    // data
    Treebank bintreebank_train;
    Treebank bintreebank_dev;
    vector<ParseAction> gold_ys_train, gold_ys_dev;
    float random_error;

    // heuristics
    bool default_set     = false;
    bool group           = false;
    bool prerank         = false;
    unsigned int kbest   = 0;
    bool prerank_local   = false;
    string prerank_file  = "";
    int duration_prerank = 0; // in seconds
    int current_learner=-1; // to go through groups systematically
    TemplateInfo tpl_info; // templates and template groups (wls) found here

    // log files
    string output_folder;
};


class NumericData{

public:

    NumericData(vector<unsigned int> const &selected_columns, //colidxes from which to build the numeric templates
                string const &train_filename,
                string const &dev_filename);

    WeakLearner<unsigned int>* get_learner(size_t idx) const;
    bool is_available(size_t idx)const{return available_templates[idx];}
    void remove_learner(WeakLearner<unsigned int> const &wl);
    vector<unsigned int> get_label_set()const{return ylab_set;}

    size_t trainN()const{return ytrain_lbls.size();}
    const vector<unsigned int>& train_y()const{return ytrain_lbls;}
    const vector<unsigned int>& dev_y()const{return ydev_lbls;}

    float get_random_error()const;
    void gen_examples(WeakLearner<unsigned int> *wl,bool train)const;    
    
    void add_templates(vector<unsigned int> const &selected_columns);//default heuristic function to generate templates

    bool has_next_wl()const;
    void reset_learner_pool();
    unsigned int get_num_accessible_learners()const{return numeric_templates.size();}
    WeakLearner<unsigned int> *get_next_wl();


private:

    //make the class impossible to copy
    NumericData(NumericData const &other);
    NumericData& operator=(NumericData const &other);

    //constructor helpers (init fields)
    void read_dataset(string const &filename,bool train);
    //templates
    vector<bool> available_templates;
    vector<vector<unsigned int>> numeric_templates;
    int next_index = -1;

    //data set
    vector<vector<unsigned int>> xtrain_dataset;
    vector<vector<unsigned int>> xdev_dataset;
    vector<unsigned int> ytrain_lbls;
    vector<unsigned int> ydev_lbls;

    //label_set
    vector<unsigned int> ylab_set;
    size_t ydims = 0;
    
};


//==========================================================

template <typename Y, typename X>
class StrongLearner{

public:
    friend class BLOGGER;
    //~StrongLearner();
    StrongLearner(vector<Y> const &action_list, unsigned int nexamples,
                  vector<Y> train_gold_ys, vector<Y> dev_gold_ys);
    StrongLearner(StrongLearner const &other);
    StrongLearner& operator=(StrongLearner const &other);

    WeakLearner<Y>* select_weak_learner(X &data_set,
                                        unsigned int sample_size,
                                        unsigned int perc_iter);

    void add_weak_learner(X &data_set, WeakLearner<Y>* wl,bool remove);
    
    const Y& get_weak_prediction(EXIDX ex_idx,WLIDX weak_learner_idx,
                                 bool train)const;
    
    const Y& get_strong_prediction(EXIDX ex_idx, bool train)const;
    void calculate_strong_prediction(bool train);
    const vector<double>& get_weights()const{return weights;}
    void recalculate_weights(WeakLearner<Y>* wl);
    float get_train_accurracy()const{return boosted_acc_train;}
    float get_dev_accurracy()const{return boosted_acc_dev;}
    
    //debug and messages
    void display_highest_weighted_labels(unsigned int K);
    double weights_entropy()const;
    void display_k_worst_classed_labels(bool train, unsigned int k);
    double get_biggest_weight();
    

private:
    void initialise_weights(int nexamples); // initialise at beginning

    Bimap<Y> ylabels;
    unsigned int nclasses;
    vector<double> alphas;
    double boosted_acc_train, boosted_acc_dev, exp_loss_train, exp_loss_dev;
    vector<vector<Y>> train_predictions, dev_predictions;
    vector<Y> train_gold_ys, dev_gold_ys;
    vector<double> weights;
    unsigned int last_iteration_duration;
    //WeakLearner<Y> *best_weak_learner;
};


//=============================================================

struct BLOGGER {
    string output_folder="";

    // params for booster
    string dataset_train;
    string dataset_dev;
    bool group_used;
    string skeleton; // if non-empty, skeleton used
    string skeleton_file;
    bool defaultset_used;
    string default_file;
    bool prerank_used;
    bool use_replace;
    unsigned int k; // for prerank
    bool justprerank;
    unsigned int duration_prerank_secs=0;
    unsigned int num_perc_iters=0;
    float proportion_data; // TODO
    unsigned int total_num_learners;
    int num_procs;
    int max_num_tpls;


    // info for an iteration
    unsigned int iternum=0; // increment each time
    unsigned int ntemplates;
    unsigned int total_ntemplates=0; // additive
    unsigned int wl_num;
    float err;
    float werr;
    double alpha;
    float boosted_acc_train;
    float boosted_acc_dev;
    unsigned int time_secs;
    unsigned int cumul_time_secs=0; // additive
    double entropy_weights;
    double biggest_weight;
    wstring fts;

    BLOGGER(string output_folder);
    void initialise_params(ParseData *data, bool replacement);
    void set_duration_prerank(unsigned int duration);
    void new_iteration(StrongLearner<ParseAction, ParseData> *sl,
                       WeakLearner<ParseAction> *wl,
                       ParseData &data);
    void print_out_params();
    void print_iteration();
    void write_num_learners(unsigned int num);
};


#include "boosting.hpp"
#endif


