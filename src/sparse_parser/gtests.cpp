//#define ENABLE_DEBUG_LOGGER
//#define ENABLE_PARSE_LOGGER
//#define ENABLE_LEARN_LOGGER

#include "gtest/gtest.h"
#include "globals.h"
#include "sparse.h"
#include "sparse_encoder.h"
#include "lexer.h"
#include "treebank.h"
#include "str_utils.h"
#include "utilities.h"
#include "state_graph.h"
#include "srparser.h"
#include "forest.h"
#include <iostream>
#include <fstream>

using namespace std;

/**
 * This test suite is meant to run with Google tests.
 * It can also be used to get in touch with the main modules of the parser.
 * Tests prefixed with basic are tests that illustrate the intended usage of the classes,
 * Other prefixes such as adversarial are meant to further check the validity of the code. 
 */

class EncoderTest: public ::testing::Test{
  
protected:
  IntegerEncoder *enc;
  TOK_CODE a; 
  TOK_CODE b;  

  void SetUp(){
      enc =  IntegerEncoder::get();
      a = enc->encode("abc",0);
      b = enc->encode("abcd",0);
    }
   
    void TearDown(){
      enc->kill();
    }
};

TEST_F(EncoderTest,BasicTest){
  
  EXPECT_EQ(enc->encode(enc->decode8(a),0),a);
  EXPECT_EQ(enc->get_code(enc->decode8(a),0),a);
  EXPECT_EQ(enc->encode(enc->decode8(b),0),b);
  EXPECT_EQ(enc->get_code(enc->decode8(b),0),b);
}

TEST_F(EncoderTest,IOTest){
  enc->save("/tmp/xxxx");
  enc->load("/tmp/xxxx");
  EXPECT_EQ(enc->get_code(enc->decode8(a),0),a);
  EXPECT_EQ(enc->get_code(enc->decode8(b),0),b);
}

TEST_F(EncoderTest,UnknownTest){
//EXPECT_EQ(enc->get_code("zzzzzzzz",0),enc->get_code(enc->get_defaultvalue(),0));
 // EXPECT_EQ(enc->get_code("xyz",0),enc->get_code(enc->get_defaultvalue(),0));
  //EXPECT_NE(enc->encode("xyz",0),enc->get_code(enc->get_defaultvalue(),0));
  //EXPECT_NE(enc->get_code("xyz",0),a);
}

TEST_F(EncoderTest,ColumnTest){
   
    EXPECT_NE(enc->encode("xyz",1),enc->encode("xyz",0));
    EXPECT_NE(enc->get_code("xyz",1),enc->get_code("xyz",0));
    EXPECT_EQ(enc->decode8(enc->encode("xyz", 1)),"xyz");
    enc->head(20);
}


class StructuredEncoderTest: public ::testing::Test{

protected:

    TOK_CODE a;
    TOK_CODE b;
    TOK_CODE c;
    TOK_CODE d;
    TOK_CODE e;
    TOK_CODE f;
    TOK_CODE g;

    ColumnEncoder *enc;

    void SetUp(){
        StructuredEncoder *enc = StructuredEncoder::get();
        //encodes phrase structure categories
        a = enc->encode("NP",StructuredEncoder::PS_COLCODE);
        b = enc->encode("XP",StructuredEncoder::PS_COLCODE);
                
        //encodes column 0 in the data
        c = enc->encode("le",0);
        d = enc->encode("chat",0);
        e = enc->encode("dort",0);
                
        f = enc->encode("D",1);
        g = enc->encode("N",1);
    }
    
    void TearDown(){
        StructuredEncoder::kill(); //destroys everything and releases memory
    }    
};

TEST_F(StructuredEncoderTest,DISABLED_BasicTest){

    StructuredEncoder *enc = StructuredEncoder::get();

    EXPECT_EQ(enc->encode(enc->decode8(a,StructuredEncoder::PS_COLCODE),StructuredEncoder::PS_COLCODE),a);
    EXPECT_EQ(enc->get_code(enc->decode8(a,StructuredEncoder::PS_COLCODE),StructuredEncoder::PS_COLCODE),a);
    EXPECT_EQ(enc->encode(enc->decode8(b,StructuredEncoder::PS_COLCODE),StructuredEncoder::PS_COLCODE),b);
    EXPECT_EQ(enc->get_code(enc->decode8(b,StructuredEncoder::PS_COLCODE),StructuredEncoder::PS_COLCODE),b);
    
    EXPECT_EQ(enc->encode(enc->decode8(c,0),0),c);
    EXPECT_EQ(enc->get_code(enc->decode8(c,0),0),c);
    EXPECT_EQ(enc->encode(enc->decode8(d,0),0),d);
    EXPECT_EQ(enc->get_code(enc->decode8(d,0),0),d);
    EXPECT_EQ(enc->encode(enc->decode8(e,0),0),e);
    EXPECT_EQ(enc->get_code(enc->decode8(e,0),0),e);

    EXPECT_EQ(enc->encode(enc->decode8(f,1),1),f);
    EXPECT_EQ(enc->get_code(enc->decode8(f,1),1),f);
    EXPECT_EQ(enc->encode(enc->decode8(g,1),1),g);
    EXPECT_EQ(enc->get_code(enc->decode8(g,1),1),g);
    EXPECT_EQ(enc->encode(enc->decode8(e,1),1),e);
    EXPECT_EQ(enc->get_code(enc->decode8(e,1),1),e);

    EXPECT_EQ(StructuredEncoder::size(),2);
    
}


/**
 * Simply tests if the logger display stuff on screen without crashing -> automatic success
 */
class LoggerTest: public ::testing::Test{};

TEST_F(LoggerTest,BasicTestDebug){
 //Debug Logger test
  DLOGGER_START(DebugLogger::WARNING, "");
  DLOGGER_WRITE(DebugLogger::ERROR,"Error man!");
  DLOGGER_WRITE(DebugLogger::DDEBUG,"Error man!");
  DLOGGER_STOP();
  DLOGGER_WRITE(DebugLogger::ERROR,"Error man!");
  DLOGGER_STOP();
  ASSERT_EQ(0,0);
}

TEST_F(LoggerTest,BasicTestParseLogger){
 //Parse Logger test
  PLOGGER_START("");
  PLOGGER_START_TIMER()
  for(int i =0; i < 5000;++i){
    PLOGGER_ADD_ITEM();
  }
  PLOGGER_END_TIMER();
  PLOGGER_SET(ParseLogger::PREC,0.7);
  PLOGGER_SET(ParseLogger::RECALL,0.6);
  PLOGGER_SET(ParseLogger::FSCORE,0.65);
  PLOGGER_SET(ParseLogger::LEN,10);
  PLOGGER_SET(ParseLogger::NUNK,3);
  PLOGGER_WRITE();
  PLOGGER_STOP();
  ASSERT_EQ(0,0);
}

TEST_F(LoggerTest,BasicTestLearnerLogger){
  LLOGGER_START("");
  LLOGGER_SET_TRAIN(0.8,0.9,0.85);
  LLOGGER_SET_DEV(0.7,0.6,0.65);
  LLOGGER_SET(LearnLogger::NUPDATES,1750);
  LLOGGER_SET(LearnLogger::MODEL_SIZE,50000);
  LLOGGER_WRITE();
  LLOGGER_SET_TRAIN(0.1,0.2,0.15);
  LLOGGER_SET_DEV(0.5,0.6,0.45);
  LLOGGER_SET(LearnLogger::NUPDATES,1500);
  LLOGGER_SET(LearnLogger::MODEL_SIZE,60000);
  LLOGGER_WRITE();
  LLOGGER_STOP();
}

class SparseVectorsTest: public ::testing::Test{

protected:
  
};


TEST_F(SparseVectorsTest,BasicCountOps){
 /*
    SparseCountVectorX x(10);
    SparseCountVectorX y(10);
    
    SparseFloatVector v(10);
    SparseFloatVector w(10);
    
    x.add_count(9);
    x.add_count(3);
    x.add_count(17);
    
    y.add_count(2);
    y.add_count(3);
    y.add_count(14);
    
    v[9] = 0.5;
    v[3] = 2.0;
    v[17] = 12.0;
    w[2] = 1.0;
    w[9] = 3.0;
    w[17] = 21.0;
    
    SparseCountVectorX t = x;
    t += y;
    y += (x);
    SparseFloatVector tmp(t);
    SparseFloatVector tmpp(y);
    cout << tmp.to_string()<<endl;
    cout << tmpp.to_string()<<endl;
    
    ASSERT_EQ(tmp,tmpp);

    SparseFloatVector u = v-w+w;
    EXPECT_EQ(u,v);
    SparseFloatVector z = v+w;
    z *= 4;
    z /= 4;
    EXPECT_EQ(z,v+w);
  */
}


class SparseMatrixTest: public ::testing::Test{
 
protected:
  class Action{
  public:
    Action(unsigned int x){this->x=x;}
    unsigned int get_code()const{return x;};
  protected:
    unsigned int x;
  };
  
  void SetUp(){}


};

TEST_F(SparseMatrixTest,BasicTrainSimulation){
/*
  SparseFloatVector v(10);
  SparseFloatVector w(10);
  v[9] = 0.5;
  v[3] = 2.0;
  v[17] = 12.0;
  
  w[2] = 1.0;
  w[9] = -3.0;
  w[17] = -21.0;
  
  //Y classes
  Action a(0);
  Action b(5);
  vector<Action> yv;
  yv.push_back(a);
  yv.push_back(b);
  
  SparseMatrix<Action> sp(yv);
  sp.addtorow(v,a);
  sp.substorow(w,b);
  
  SparseMatrix<Action> spa(yv);
  SparseMatrix<Action> spb(sp);
  spb.substorow(v,a);
  spb.addtorow(v,a);
  EXPECT_EQ(spa,spb);

  SparseCountVectorX x(10);
  x.add_count(9);
  x.add_count(9);
  x.add_count(17);
  x.add_count(12);
  
  vector<float> res(yv.size(),0.0);
  sp.dot(x,res);

  EXPECT_EQ(res[0],13);
  EXPECT_EQ(res[1],27);
 */
}

class TreebankTest: public ::testing::Test{
  
protected:
 
  string testA;
  string testB;

  void SetUp(){
    if (getenv("HYPARSE_HOME") == NULL ) {cerr << "Please set up the environment variable HYPARSE_HOME before running the tests"<<endl;exit(1);}

    string base_path = getenv("HYPARSE_HOME");
    testA = base_path + "/trunk/data/test1.tbk";
    testB = base_path + "/trunk/data/test2.tbk";
  }
   
  void TearDown(){}
};

TEST_F(TreebankTest,BasicTreeOps){

  ifstream is(testA);

  AbstractParseTree *treeA;
  AbstractParseTree *treeB;

  string bfr;//skips header
  getline(is,bfr);

  while(get_tree(is,treeA)){
    treeB = clone(treeA);
    ASSERT_TRUE(head_check(treeB));
    ASSERT_TRUE(head_check(treeA));
    tuple<float,float,float> eval  = compare(treeA,treeB);
    EXPECT_EQ(std::get<2>(eval),1.0);
    destroy(treeA);
    destroy(treeB);
  }
 
  is.close();
}

TEST_F(TreebankTest,TreeDecorations){
  ifstream is(testA);

  AbstractParseTree *treeA;
  AbstractParseTree *treeB;
  
  string bfr;//skips header
  getline(is,bfr);

  while(get_tree(is,treeA)){
    treeB = clone(treeA);
    flajolet_decorate(treeA);
    undecorate(treeA);
    tuple<float,float,float> eval = compare(treeA,treeB);
    EXPECT_EQ(std::get<2>(eval),1.0) << (*treeA) << endl;
    parent_decorate(treeA);
    undecorate(treeA);
    eval  = compare(treeA,treeB);
    EXPECT_EQ(std::get<2>(eval),1.0) << (*treeA) << endl;
    destroy(treeA);
    destroy(treeB);
  }
  is.close();
}

TEST_F(TreebankTest,TreeBinarization){

  ifstream is(testA);

  AbstractParseTree *treeA;
  AbstractParseTree *treeB;

  string bfr;//skips header
  getline(is,bfr);

  while(get_tree(is,treeA)){
    treeB = clone(treeA); 
    head_markovize(treeA);
    ASSERT_TRUE(head_check(treeA)) << *treeA << endl;
    unbinarize(treeA);
    ASSERT_TRUE(head_check(treeA))  << *treeA << endl;
    tuple<float,float,float> eval  = compare(treeA,treeB);
    EXPECT_EQ(std::get<2>(eval),1.0);
    destroy(treeA);
    destroy(treeB);
  }
  is.close();
}


TEST_F(TreebankTest,TreeUnaryClosure){
  ifstream is(testA);

  AbstractParseTree *treeA;
  AbstractParseTree *treeB;

  string bfr;//skips header
  getline(is,bfr);

  while(get_tree(is,treeA)){
    treeB = clone(treeA); 
    unary_closure(treeA);
    ASSERT_TRUE(head_check(treeA)) << *treeA << endl;
    unpack_unaries(treeA);
    ASSERT_TRUE(head_check(treeA)) << *treeA << endl;
    tuple<float,float,float> eval = compare(treeA,treeB);
    EXPECT_EQ(std::get<2>(eval),1.0);
    destroy(treeA);
    destroy(treeB);
  }
  is.close();
}


TEST_F(TreebankTest,TreeBankTransformation){

  Treebank base_tbk(testA);
  Treebank trans_tbk;
  Treebank rev_tbk;
  base_tbk.transform(trans_tbk);
  trans_tbk.detransform(rev_tbk);
  ASSERT_EQ(base_tbk.size(),rev_tbk.size());
  for(int i = 0; i < base_tbk.size();++i){
    tuple<float,float,float> eval  = compare(base_tbk[i],rev_tbk[i]);
    EXPECT_EQ(std::get<2>(eval),1.0);
  }
  tuple<float,float,float> eval = base_tbk.evalb(rev_tbk);
  ASSERT_EQ(std::get<2>(eval),1.0);
}

TEST_F(TreebankTest,LexSmoothing){

 Treebank base_tbk(testA);
 vector<PSTRING> tokens;
 base_tbk.get_word_tokens(tokens);

 Counter<PSTRING> count; 
 count.add_all_counts(tokens);

 vector<PSTRING> highfreq;
 count.aboveEq_threshold(2,highfreq);
 IntegerEncoder::get()->encode_all(highfreq,0);

 ASSERT_GE(highfreq.size(), 0);
 ASSERT_LE(highfreq.size(), tokens.size());

 //Note: base_tbk.update_encoder() encodes everything in the treebank (more simple to use)

}

class DerivationTest: public ::testing::Test{
    
protected:
    
    string testA;
    string testB;
    
    void SetUp(){
        if (getenv("HYPARSE_HOME") == NULL ) {cerr << "Please set up the environment variable HYPARSE_HOME before running the tests"<<endl;exit(1);}
        
        string base_path = getenv("HYPARSE_HOME");
        testA = base_path + "/trunk/data/test1.tbk";
        testB = base_path + "/trunk/data/test2.tbk";
    }
    
    void TearDown(){}
};


TEST_F(DerivationTest,BaseTest){
    
    Treebank base_tbk(testA);
    Treebank trans_tbk;
    base_tbk.transform(trans_tbk);
    trans_tbk.update_encoder();
    SrGrammar grammar(trans_tbk);    //Extracts the grammar automaton from the treebank
    
    for(int i = 0; i  < trans_tbk.size();++i){
        ParseDerivation deriv(trans_tbk[i],grammar);
        cout << deriv << endl;
        
        vector<InputToken> input_sequence;
        tree_yield(trans_tbk[i],input_sequence);
        AbstractParseTree *p =  deriv.as_tree(input_sequence);
        tuple<float,float,float> eval  = compare(trans_tbk[i],p);
        EXPECT_EQ(std::get<2>(eval),1.0);
        destroy(p);
    }
}

class ForestTest:public ::testing::Test{
    
protected:
    string tb_path;
    string tpl_path;

    void SetUp(){
        
        if (getenv("HYPARSE_HOME") == NULL ) {cerr << "Please set up the environment variable HYPARSE_HOME before running the tests"<<endl;exit(1);}
        
        string base_path = getenv("HYPARSE_HOME");
        tb_path = base_path + "/trunk/data/test1_ttd.tbk";
        tpl_path = base_path + "/trunk/data/generic_ttd.tpl";
        
    }
    void TearDown(){}

};


TEST_F(ForestTest,BaseTest){
    
    Treebank base_tbk(tb_path);
    Treebank trans_tbk;
    base_tbk.transform(trans_tbk);
    trans_tbk.update_encoder();
    for(int i = 0; i < trans_tbk.size();++i){
        Forest f(trans_tbk[i]);
        cout << f << endl;
    }
}

TEST_F(ForestTest,DISABLED_Viterbi){
    
    Treebank base_tbk(tb_path);
    Treebank trans_tbk;
    base_tbk.transform(trans_tbk);
    trans_tbk.update_encoder();

    TemplateTypeDefinition ttd(trans_tbk.colnames);
  
    SparseEncoder templates(tpl_path,ttd);
    SrParser p(templates);
    p.train_local_model(trans_tbk,base_tbk,4,10);
    vector<InputToken> input_sequence;
    
    for(int i = 0; i < trans_tbk.size();++i){
        Forest f(trans_tbk[i]);
        tree_yield(trans_tbk[i], input_sequence);
        //AbstractParseTree *tree = p.predict_one(input_sequence);
        /*
         for(int k = 1; k < p.get_num_parses();++k){
            //AbstractParseTree *res = p.predict_kth_parse(k,input_sequence);
            f.add_tree(res);
            destroy(res);
        }
         */
        //AbstractParseTree *s = f.viterbi(input_sequence);
        //cout << *s << endl;
        //extract k-parses et plug it into the forest.
        //cout << f << endl;
        //destroy(s);
    }
}



class LocalSrParserTest: public ::testing::Test{
    
protected:
    
    string tb_path;
    string tpl_path;
    
    void SetUp(){
        if (getenv("HYPARSE_HOME") == NULL ) {cerr << "Please set up the environment variable HYPARSE_HOME before running the tests"<<endl;exit(1);}
        string base_path = getenv("HYPARSE_HOME");
        tb_path = base_path + "/trunk/data/test1_ttd.tbk";
        tpl_path = base_path + "/trunk/data/generic_ttd.tpl";
        IntegerEncoder::get()->kill();
    }
    
    void TearDown(){}
};


TEST_F(LocalSrParserTest,BaseTrain){
  IntegerEncoder::get()->kill();
  
  Treebank base_tbk(tb_path);
  Treebank trans_tbk;
  base_tbk.transform(trans_tbk);
    
  TemplateTypeDefinition ttd(trans_tbk.colnames);
  
  SparseEncoder templates(tpl_path,ttd);
    
  SrParser p(templates);

  p.train_local_model(trans_tbk,trans_tbk,4,10);
  tuple<float,float,float> E = p.eval_model(base_tbk);
  //EXPECT_NEAR(get<2>(E), 1.0, 0.1);
}

#ifdef ENABLE_PARALLEL
TEST_F(LocalSrParserTest,ParallelTrain){
  
  Treebank base_tbk(tb_path);
  Treebank trans_tbk;
  base_tbk.transform(trans_tbk);
    
  TemplateTypeDefinition ttd(trans_tbk.colnames);
    
  SparseEncoder templates(tpl_path,ttd);
  SrParser p(templates);
  //p.omp_train_local_model(trans_tbk,trans_tbk,4,10,2);
  tuple<float,float,float> E = p.eval_model(base_tbk);
  //EXPECT_NEAR(get<2>(E), 1.0, 0.1);
}
#endif

class GlobalSrParserTest: public ::testing::Test{
    
protected:
    
    string tb_path;
    string tpl_path;
    
    void SetUp(){
        if (getenv("HYPARSE_HOME") == NULL ) {cerr << "Please set up the environment variable HYPARSE_HOME before running the tests"<<endl;exit(1);}
        
        string base_path = getenv("HYPARSE_HOME");
        tb_path = base_path + "/trunk/data/test1_ttd.tbk";
        tpl_path = base_path + "/trunk/data/generic_ttd.tpl";
        IntegerEncoder *enc = IntegerEncoder::get();
        enc->kill();

    }
    
    void TearDown(){}
};

TEST_F(GlobalSrParserTest,BaseTrain){
    
    Treebank base_tbk(tb_path);
    Treebank trans_tbk;
    base_tbk.transform(trans_tbk);
    
    TemplateTypeDefinition ttd(trans_tbk.colnames);
    
    SparseEncoder templates(tpl_path,ttd);
    SrParser p(templates);
    p.train_global_model(trans_tbk,trans_tbk,1,10);
    tuple<float,float,float> E = p.eval_model(base_tbk);
    EXPECT_NEAR(get<2>(E), 1.0, 0.1);
}

/**
 * This class performs quite heavy tests than the other test classes, 
 * should be disabled most of the time.
 */
class GlobalParserDebug : public ::testing::Test{
    
protected:
    
    string tb_path;
    string tpl_path;
    
    void SetUp(){
        if (getenv("HYPARSE_HOME") == NULL ) {cerr << "Please set up the environment variable HYPARSE_HOME before running the tests"<<endl;exit(1);}
        
        string base_path = getenv("HYPARSE_HOME");
        tb_path = base_path + "/trunk/data/ftb20-train-ttd.tbk";
        tpl_path = base_path + "/trunk/data/generic.tpl";
        IntegerEncoder *enc = IntegerEncoder::get();
        enc->kill();
        
    }
    
    void TearDown(){}
};


TEST_F(GlobalParserDebug,DISABLED_BeamInspection){
    
    string base_path = getenv("HYPARSE_HOME");
    tb_path = base_path + "/trunk/data/ftb20-train-ttd.tbk";
    tpl_path = base_path + "/trunk/data/generic.tpl";
    Treebank base_tbk(tb_path);
    Treebank trans_tbk;
    base_tbk.transform(trans_tbk);
    TemplateTypeDefinition ttd(trans_tbk.colnames);
    
    SparseEncoder templates(tpl_path,ttd);
    SrParser p(templates);
    p.train_local_model(trans_tbk,trans_tbk,1, 10);
    p.show_internals(trans_tbk);
}


TEST_F(GlobalParserDebug,DISABLED_MiniBatchDebug){
//TEST_F(GlobalParserDebug,MiniBatchDebug){
    
    Treebank base_tbk(tb_path);
    Treebank trans_tbk;
    base_tbk.transform(trans_tbk);
    
    TemplateTypeDefinition ttd(trans_tbk.colnames);
    
    SparseEncoder templates(tpl_path,ttd);

    EXPECT_TRUE(SrParser::minibatch_debug(trans_tbk, templates));
}

#ifdef ENABLE_PARALLEL
TEST_F(GlobalSrParserTest,ParallelTrain){
    
    Treebank base_tbk(tb_path);
    Treebank trans_tbk;
    base_tbk.transform(trans_tbk);
    
    TemplateTypeDefinition ttd(trans_tbk.colnames);
    
    SparseEncoder templates(tpl_path,ttd);
    SrParser p(templates);
    //p.omp_train_global_model(trans_tbk,base_tbk,4,10,2);
    tuple<float,float,float> E = p.eval_model(base_tbk);
    EXPECT_NEAR(get<2>(E), 1.0, 0.1);
}
#endif

int main(int argc, char **argv){
  ::testing::InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS();
}
