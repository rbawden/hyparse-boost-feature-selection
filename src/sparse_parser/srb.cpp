#include "boosting.h"
#include <getopt.h>
#ifdef ENABLE_PARALLEL
    #include <omp.h>
#endif

void print_help();

/*-----------------------------------------------
 * COMMAND LINE INTERFACE AND DEFAULT VALUES
 * ---------------------------------------------*/
string output_folder; // obligatory
string train_file;
string dev_file;
string skeleton_file;
string template_file;
string prerank_file;

int num_perceptron_iters; // obligatory
float data_size(1.0);
int max_model_size(-1); // -1 when not activated
unsigned int num_procs(1);
int kbest(-1); // only activated when prerank=true

bool remove_tpl(true);
bool group(false);
bool prerank(false);

/*-----------------------------------------------
 * MAIN
 * ---------------------------------------------*/
int main(int argc,char *argv[]){

    int c;
    srand(time(NULL));

    while(true){
        static struct option long_options[] ={
            {"help",no_argument,0,'h'},
            {"output",required_argument,0,'o'},
            {"skeleton",required_argument,0,'s'},
            {"templates",required_argument,0,'t'},
            {"group",no_argument,0,'g'},
            {"replace",no_argument,0,'r'},
            {"kbest",required_argument,0,'k'},
            {"prerankfile",required_argument,0,'K'},
            {"procs",required_argument,0,'p'},
            {"maxboost",required_argument,0,'b'},
            {"maxperc",required_argument,0,'i'},
            {"datasize",required_argument,0,'d'},
        };
        int option_index = 0;
        c = getopt_long (argc, argv, "hgro:s:t:k:K:p:b:i:d:j",long_options, &option_index);
        if(c==-1){break;}

        switch(c) {
        case 'o':
            output_folder = string(optarg);
            break;
        case 's':
            skeleton_file = string(optarg);
            break;
        case 't':
            template_file = string(optarg);
            break;
        case 'g':
            group = true;
            break;
        case 'r':
            remove_tpl = false;
            break;
        case 'k':
            prerank=true;
            kbest = atoi(optarg);
            break;
        case 'K':
            prerank_file = string(optarg);
            cerr << "Warning: You have chosen to load a saved preranking file. Make sure that it was generated in the same conditions (i.e. with the same skeletons or default file.)" << endl;
            break;
        case 'p':
            num_procs = atoi(optarg);
            break;
        case 'b':
            if (atoi(optarg)<1) {
                cerr << "Maximum model size must be greater than 0. Aborting" << endl;
                exit(1);
            }
            max_model_size = atoi(optarg);
            break;
        case 'i':
            if (atoi(optarg)<1) {
                cerr << "The number of perceptron iteration must be greater than 0. Aborting"
                     << endl;
                exit(1);
            }
            num_perceptron_iters = atoi(optarg);
            break;
        case 'd':
            data_size = atof(optarg);
            break;
        case 'h':
            print_help();
            break;
        default:
            print_help();
            break;
        }
    }

    /*-----------------------------------------------
     * POSITIONAL ARGUMENTS (TREEBANKS)
     * ---------------------------------------------*/
    if(optind < argc-1){
      train_file = string(argv[optind]);
      dev_file = string(argv[optind+1]);
    }else{
      cerr << "Missing argument(s): you must select a train file and a dev file ... Aborting" <<endl;
      print_help();
      exit(1);
    }

    /*-----------------------------------------------
     * CHECK COHERENCY OF OPTIONS (OBLIGATORY ARGS + VALUES)
     * ---------------------------------------------*/
    //
    if (output_folder=="") {
        cerr << "You must select specify an output folder. Aborting"
             << endl;
        exit(1);
    }
    if (skeleton_file!="" && template_file!="") {
        cerr << "You must select only one of --skeleton and --templates. Aborting"
             << endl;
        exit(1);
    }
    if (template_file!="" && group) {
        cerr << "You cannot use a default template file AND group. Aborting"
             << endl;
        exit(1);
    }
    if (num_perceptron_iters<1) {
        cerr << "You must select a number of perceptron iterations. Aborting"
             << endl;
        exit(1);
    }
    if (num_procs<1) {
        cerr << "There must be at least one processor selected. Defaulting to one." << endl;
        num_procs=1;
    }
    if (data_size<=0.) {
        cerr << "Your data size is zero or negative. There must be some data... Aborting" << endl;
        exit(1);
    }
    if (!prerank && prerank_file!="") {
        cerr << "You have given a pre-rank file but have not specified a number for k. Aborting" << endl;
        exit(1);
    }
    if (prerank && prerank_file=="") {
        cerr << "You must provide a prerank file to only pool k learners each time." << endl;
        exit(1);
    }
    if (prerank && kbest<1) {
        cerr << "You have selected to prerank learners and pool a maxmimum of k at a time. \nBut k must be > 0. Aborting" << endl;
        exit(1);
    }

    /*-----------------------------------------------
     * MULTIPROCESSING
     * ---------------------------------------------*/
    #ifdef ENABLE_PARALLEL
        //clog << "number of processors = " << num_procs << endl;
        omp_set_num_threads(num_procs);
    #else
        num_procs = 1;
    #endif

    /*-----------------------------------------------
     * DATA, HEURISTICS AND LOGGING INITIALISATION
     * ---------------------------------------------*/
    BLOGGER blogger(output_folder);

    // INITIALISE DATA
    Treebank train(train_file);
    Treebank dev(dev_file);
    ParseData data(train, dev);
    data.get_random_error(); // TODO place within constructor!

     // HEURISTICS
    if (skeleton_file!="") {
        data.set_skeleton(skeleton_file);
        blogger.skeleton_file = skeleton_file;
    }
    else if (template_file!="") {
        data.set_default_set(template_file);
        blogger.default_file = template_file;
    }
    else
        data.no_restrictions();

    if (group)
        data.set_grouping(true);
    if (prerank && prerank_file=="")
        data.use_prerank(kbest, true, output_folder); // only local for now TODO num of perceptron iters
    else if (prerank)
        data.use_prerank_from_file(kbest, prerank_file);

    // LOG AND DISPLAY PARAMS
    blogger.initialise_params(&data, !(remove_tpl));
    blogger.num_perc_iters=num_perceptron_iters;
    blogger.dataset_train=train_file;
    blogger.dataset_dev=dev_file;
    blogger.num_procs = num_procs;
    blogger.max_num_tpls = max_model_size;
    blogger.proportion_data = data_size;
    blogger.print_out_params();

    /*-----------------------------------------------
     * RUN BOOSTING APPROACH TO FEATURE SELECTION
     * ---------------------------------------------*/
    data.generate_candidates();
    data.load_ordered_classifiers();
    /*if (prerank)
        blogger.set_duration_prerank(data.get_duration_prerank());*/
    blogger.write_num_learners(data.get_num_available_learners());

    vector<ParseAction> acvec;
    data.grammar.get_ordered_actions(acvec);
    
    StrongLearner<ParseAction,ParseData> sl(acvec, data.nexamples_train,
                                  data.get_gold_ys_train(),
                                  data.get_gold_ys_dev());
    
    WeakLearner<ParseAction>* wl;

    // JUST DO PRERANKING
    //exit(0);

    #ifdef ENABLE_PARALLEL
        omp_set_num_threads(num_procs);
    #else
        num_procs = 1;
    #endif

    bool keep_selecting(true);
    unsigned int i=0;
    while (keep_selecting) {
        i++;
        clog << "\n------------\nIteration number " << i << endl;

        wl=sl.select_weak_learner(data, (int)sl.get_weights().size()*data_size,
                                  num_perceptron_iters);

        if (wl->get_error(true, sl.get_weights()) < data.get_random_error()) {
            sl.add_weak_learner(data, wl, remove_tpl);
            blogger.entropy_weights = sl.weights_entropy();
            blogger.biggest_weight = sl.get_biggest_weight();
            blogger.new_iteration(&sl, wl, data);
            blogger.print_iteration();
            sl.recalculate_weights(wl);
            //sl.display_highest_weighted_labels(30);
            //cout << "H[W] = " << sl.weights_entropy()<<endl;

            //sl.display_k_worst_classed_labels(true, 50); // on train=truef
            data.learner_chosen(wl, remove_tpl);
            delete wl;
        }
        else {
            delete wl;
            keep_selecting=false;
        }
        if (data.get_num_accessible_learners()==0)
            keep_selecting=false;
            //cout << "no more accessible learners" << endl;
        if (max_model_size!=-1) {
            if (blogger.total_ntemplates>=max_model_size)
                keep_selecting=false; // break anyway if max boost iterations reached
                /*cout << "maximum model size reached" << endl;
                cout << blogger.total_ntemplates << "templates !" << endl;
                cout << "for a max model size of " << max_model_size << endl;*/
        }
    }
}

/*-----------------------------------------------
 * PRINT USAGE AND HELP
 * ---------------------------------------------*/
void print_help() {
    cerr << "\nUsage: ./srb TRAIN_FILEPATH DEV_FILEPATH [-h] [-p INT] [-d FLOAT] [-b INT] \n\t\t[-k INT] [-r] [-g] [-K FILENAME] [-s FILENAME] [-t FILENAME] -o FOLDERNAME -i INT " << endl;
    cerr << "\nObligatory arguments:" << endl;
    cerr << "   -o --output     [FOLDERNAME] Sets the output folder to FOLDERNAME" << endl;
    cerr << "   -i --maxperc    [INT]        Sets the maximum number of perceptron iterations" << endl;

    cerr << "\nOptional arguments:" << endl;
    cerr << "   -h --help                    Displays this message" << endl;
    cerr << "   -p --procs      [INT]        Sets the number of processors (default=1)" << endl;
    cerr << "   -d --datasize   [FLOAT]      Sets the proportion of train data (default=1)" << endl;
    cerr << "   -b --maxboost   [INT]        Sets a max number of boost iterations" << endl;
    cerr << "   -k --kbest      [INT]        Sets the max number pooled at each round.  file obligatory" << endl;
    cerr << "   -K --prerankfile[FILENAME]   Sets the prerank file to FILENAME" << endl;
    cerr << "   -r --replace                 Replaces learner after selection" << endl;
    cerr << "   -g --group                   Groups templates" << endl;
    cerr << "   -s --skeleton   [FILENAME]   Sets the skeleton file to FILENAME." << endl;
    cerr << "   -t --templates  [FILENAME]   Sets the default template file to FILENAME." << endl << endl;


    exit(0);
}

