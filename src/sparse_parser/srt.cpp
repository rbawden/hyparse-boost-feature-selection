#include "globals.h"
#include "utilities.h"
#include "srparser.h"
#include "treebank.h"
#include "sparse_encoder.h"
#include <getopt.h>
#include <iostream>
#include <fstream>
#include <sys/stat.h>
#ifdef ENABLE_PARALLEL
    #include "omp.h"
#endif


void display_help_message(){
  cerr << endl << " This is the trainer command line for the lexicalized Shift Reduce Parser. \n By default, it fits a global perceptron model with early update.\n But large margin variants are available. "<<endl<<endl;
  cerr << " Usage:"<<endl<<endl;
  cerr << "    srt [options] train_filename dev_filename" << endl << endl;
  cerr << "    -h   --help                       : prints this help message and exit." <<endl;
  cerr << "    -i   --iterations         [INT]   : sets the number of epochs for the perceptron (default = 35)" <<endl;
  cerr << "    -b   --beam-size          [INT]   : sets the size of the beam (default = 4)" <<endl;
  cerr << "    -B   --batch-size         [INT]   : sets the minibatch size to value (default = 1)" <<endl;
  cerr << "    -t   --templates-filename [PATH]  : sets the file where to seek for templates" <<endl;
  cerr << "    -m   --model-name         [PATH]  : sets the directory path where to save the model" <<endl;
  cerr << "    -p   --processors         [INT]   : trains in parallel mode with maximum p processors [if available] (default = 1)" <<endl;
  cerr << "    -l   --local                      : trains a local perceptron model instead of a global one." <<endl;
  cerr << "    -V   --max-Violation              : Uses max violation update instead of early update (global perceptron model only)" <<endl;
  cerr << "    -M   --mira               [FLOAT] : Fits a MIRA model with max violation update (default C = 0.01)" <<endl; //grid search should be around [0.004,0.006,0.01]
  cerr << "    -P   --pegasos            [FLOAT] : Fits an online SVM Pegasos model with max violation update (default lambda = 0.001)" <<endl;
  cerr << "    -T   --transformation     [STRING]: applies a transformation ('[p]arent' or '[f]lajolet') to the training data" <<endl;
  cerr << endl;
}

void display_summary(int beam_size,int epochs,size_t train_size,size_t dev_size,int nprocs,int batch_size,bool local,SrParser::ModelType modtype,SrParser::UpdateType uptype,float C,float lambda){

  cerr << "***** Training setup summary **********"<<endl;
  cerr << "   Max iterations       : " << epochs <<endl;
  cerr << "   Beam size            : " << beam_size <<endl;
  cerr << "   Training set size    : " << train_size <<endl;
  cerr << "   Development set size : " << dev_size <<endl;
  if (batch_size > 1 && !local){cerr << "   Minibatch size       : " << batch_size <<endl;}
  if (nprocs > 1 && !local){    cerr << "   Number of processors : "<< nprocs <<endl;}
  if (local){         cerr << "   Using a local perceptron model" <<endl;}
  if(!local && modtype==SrParser::PERCEPTRON){
      if (uptype == SrParser::EARLY_UP){cerr << "   Using a global perceptron model\n   with early update" <<endl;}
      else {cerr << "   Using a global perceptron model\n   with max violation update" <<endl;}
  }
  if(modtype==SrParser::MIRA){   cerr << "   Using a global MIRA model\n   with max violation update (C="<<C<<")" <<endl;}
  if(modtype==SrParser::PEGASOS){cerr << "   Using a global SVM/pegasos model\n   with max violation update (lambda="<<lambda<<")"<<endl;}
  cerr << "****************************************"<<endl<<endl;
}


//SETUP VARS
enum MODEL_LOCALITY{GLOBAL,LOCAL};
string train_path;
string dev_path;
string tpl_path;
string model_path;

int beam = 4;
int perceptron_iterations = 35;
int num_procs = 1;
int batch_size = 1;
float lambda = 0.001;//pegasos regularizer
float C = 0.01; //MIRA bound

MODEL_LOCALITY mlocality = GLOBAL;
SrParser::UpdateType uptype = SrParser::EARLY_UP;
SrParser::ModelType modtype = SrParser::PERCEPTRON;
Treebank::Decoration transformation = Treebank::NO_DECORATIONS;



int main(int argc,char *argv[]){

  srand(1);
  
  int c;
  //defaults
  tpl_path   = "default.tpl";
  model_path = "default.model";

    
  while(true){
    static struct option long_options[] ={
      {"help",no_argument,0,'h'},
      {"iterations",required_argument,0,'i'},
      {"batch-size",required_argument,0,'B'},
      {"templates-filename",required_argument,0,'t'},
      {"model-name",required_argument,0,'m'},
      {"beam-size",required_argument,0,'b'},
      {"processors",required_argument,0,'p'},
      {"local-model",no_argument,0,'l'},
      {"max-Violation",no_argument,0,'V'},
      {"pegasos",required_argument,0,'P'},
      {"mira",required_argument,0,'M'},
      {"transformation",required_argument,0,'T'}
    };
    int option_index = 0;
    c = getopt_long (argc, argv, "m:i:ht:b:B:lVT:p:M:P:",long_options, &option_index);
    if(c==-1){break;}
    switch(c){
    case 'i': 
      perceptron_iterations = atoi(optarg);
      break;
    case 't':
      tpl_path = string(optarg);
      break;
    case 'm':
      model_path = string(optarg);
      break;
    case 'p':
      num_procs = atoi(optarg);
      break;
    case 'b':
      beam = atoi(optarg);
      break;
    case 'B':
      batch_size = atoi(optarg);
      break;
    case 'P':
     lambda = atof(optarg);
     modtype = SrParser::PEGASOS;
     break;
    case 'M':
     C = atof(optarg);
     modtype = SrParser::MIRA;
     break;
    case 'l':
      mlocality = LOCAL;
      break;
    case 'V':
      uptype = SrParser::MAXV_UP;
      break;
    case 'T':
    {
      string s(optarg);
      if (s == "parent" || s == "p"){transformation = Treebank::PARENT_DECORATION;}
      if (s == "flajolet" || s == "f"){transformation = Treebank::FLAJOLET_DECORATION;}
      if (transformation == Treebank::NO_DECORATIONS){
       cerr << "Warning (unknown transformation) : " << s << " ignored." << endl;
      }
      break;
    }
    case 'h':
      display_help_message();
      exit(0);
    }
  }
  if (optind == 1){exit(0);}//abort if no arg at all
  if(optind < argc+1){
    train_path = string(argv[optind]);
    dev_path = string(argv[optind+1]);
  }else{
    cerr << "Missing either training or dev file name ... aborting." <<endl;
    exit(1);
  }
 
  //LOAD TREEBANKS
  Treebank base_tbk(train_path);
  Treebank dev_bank(dev_path);
    
  //MULTIPROCESSING SETUP
  #ifdef ENABLE_PARALLEL
      if (num_procs > batch_size){num_procs = batch_size;}
      omp_set_num_threads(num_procs);
  #else
    num_procs = 1;
  #endif
    
  //Check options integrity
  if(mlocality==LOCAL){
      num_procs = 1;
      batch_size = 1;
      modtype = SrParser::PERCEPTRON;
  }
    
  if(modtype != SrParser::PERCEPTRON){
      mlocality = GLOBAL;
      uptype = SrParser::MAXV_UP;
  }
    
  if(batch_size > base_tbk.size()){batch_size = base_tbk.size();}
  
  display_summary(beam,perceptron_iterations,base_tbk.size(),dev_bank.size(),num_procs,batch_size,mlocality == LOCAL,modtype,uptype,C,lambda);

  //BUILD PARSER
  TemplateTypeDefinition ttd(base_tbk.colnames);
  SparseEncoder templates(tpl_path,ttd);
  templates.do_sanity_check();//checks that templates are semantically valid.
  //wcout << templates.toString(ttd) << endl;
  SrParser srp(templates);

  mkdir(model_path.c_str(), S_IRUSR | S_IWUSR | S_IXUSR);//creates model dir
  string logfile = model_path +"/train.log";
  LLOGGER_START(logfile.c_str());//turns logger on

  //RUN
  if (modtype == SrParser::PERCEPTRON && mlocality == LOCAL){srp.train_local_model(base_tbk,dev_bank,transformation,beam,perceptron_iterations);}
  if (mlocality == GLOBAL && batch_size == 1){srp.train_global_model(base_tbk,dev_bank,transformation,beam,perceptron_iterations,uptype,modtype,C,lambda);}
  if (mlocality == GLOBAL && batch_size > 1){srp.train_global_minibatch_model(base_tbk,dev_bank,transformation,beam,perceptron_iterations, batch_size,uptype,modtype,C,lambda);}
    
  //SAVE MODEL
  IntegerEncoder::get()->save(model_path+"/encoder");
  ttd.save(model_path+"/ttd");
  copy_file(tpl_path,model_path+"/templates");//copies the templates to model dir
  srp.save(model_path);//saves the model to disk
  IntegerEncoder::kill();
  LLOGGER_STOP();//turns logger off
  cout << "done."<<endl;
  return 0;
}
