#ifndef SRPARSER_H
#define SRPARSER_H

#include <tuple>
#include <vector>
#include <algorithm>
#include "dynamic_oracle.h" // @@m
#include "globals.h"
#include "sparse.h"
#include "sparse_encoder.h"
#include "state_graph.h"
#include "srgrammar.h"
#include "lexer.h"
#include "treebank.h"

using namespace std;

static const float THRESHOLD_CONVERGENCE = 20; // percentage of loss to calculate threshold value seen as no change
static const float ITERATIONS_NO_CHANGE = 4.;

//@debug
void dump_xvec(vector<unsigned int> const &xvec);


class SrParser{

 public :
    
  enum UpdateType{MAXV_UP,EARLY_UP};
  enum ModelType{PERCEPTRON,MIRA,PEGASOS};
  
  SrParser(SparseEncoder const &spencoder); //creates a parser to be trained
  SrParser(const char* modelfile); //reads a parser from dir
  SrParser(const string &modelfile);//reads a parser from dir
  SrParser(SrParser const &other);
  SrParser& operator=(SrParser const &other);
  SrParser();//@deprecated : dummy constructor
  ~SrParser();
    
  //parse a corpus
  void parse_corpus(AbstractLexer *lex,istream &input_source,int Kbest,MultiOutStream &outstream);
  void parse_corpus(Treebank const &treebankIn,MultiOutStream &outstream);

  void batch_parse_corpus(size_t max_batch_size,AbstractLexer *lex,istream &input_source,int K,MultiOutStream &outstream);
    
  //train a model
  // RAB : added option 'test_convergence' and verbose mode
  void train_global_model(Treebank &train_set,
                          Treebank &dev_set,
                          Treebank::Decoration const &transformation,
                          size_t beam_size,
                          size_t epochs,
                          UpdateType uptype=EARLY_UP,
                          ModelType=PERCEPTRON,
                          float C=1.0,
                          float lambda=1.0,
                          bool verbose=true,
                          bool test_convergence=false);

  // RAB : added option 'test_convergence' and verbose mode
  void train_global_minibatch_model(Treebank &train_set,
                                    Treebank &dev_set,
                                    Treebank::Decoration const &transformation,
                                    size_t beam_size,
                                    size_t epochs,
                                    size_t minibatch_size,
                                    UpdateType uptype=EARLY_UP,
                                    ModelType=PERCEPTRON,
                                    float C=1.0,
                                    float lambda=1.0,
                                    bool verbose=true,
                                    bool test_convergence=false);

  void train_local_model(Treebank &train_set, Treebank &dev_set, Treebank::Decoration const &transformation, size_t beam_size, size_t epochs, bool verbose=true);

  //eval a model
  tuple<float,float,float> eval_model(Treebank const &eval_set);
  float eval_local_model(const Treebank &eval_set);//faster version for local models, returns accurracy

  //I/O
  void save(const string &filename)const; //saves the model into directory
    
  /**
    * This is a debugging function aiming at outputting small set of problematic sentences
    * It splits the test_bank in small batches and tries to identify on which batches
    * the model does not manage to overfit the data. These batches are then outputted on stdout.
    */
  static bool minibatch_debug(Treebank const &test_bank,SparseEncoder const &spencoder,size_t batch_size = 10);
  void show_internals(Treebank const &test_bank);

 protected:
    
   //Main prediction functions
   AbstractParseTree* predict_one(InputDag &input_sequence,TSSBeam &beam,vector<unsigned int> &xvec,vector<float> &y_scores,bool make_tree= true);
   AbstractParseTree* get_kth_parse(int k, InputDag &input_sequence,TSSBeam &beam)const;
   
  //splits a treebank into minibatches
  void make_minibatches(size_t mbatch_size,Treebank &train_bank,vector<vector<const AbstractParseTree*> > &mini_batches)const;
  void shuffle_minibatches(vector<vector<const AbstractParseTree*>> &mini_batches)const;

   //Learning methods
  int global_learn_minibatch(vector<const AbstractParseTree*> const &mbatch,SparseFloatMatrix &delta,SparseFloatMatrix &averaged_model, float T);
  bool global_learn_one(AbstractParseTree const *ref,SparseFloatMatrix &averaged_model,float T);//with averaging, returns true if update
  int local_learn_one(AbstractParseTree const *ref,SparseFloatMatrix &averaged_model,float T);//with averaging ; returns the number of updates


  //@@m << learning with dynamic oracle
public:
  void train_local_model_exploration(Treebank &train_set, Treebank &dev_set, Treebank::Decoration const &transformation, size_t beam_size, size_t epochs, int k, double p, bool verbose=true); // @@m
  int local_learn_one_exploration(AbstractParseTree const *ref, SparseFloatMatrix &averaged_model, float T, DynamicOracle &oracle, double p); //@@m with averaging ; returns the number of updates
  void generate_dataset(Treebank &train_set, double p, vector<vector<unsigned int>> &X, vector<vector<bool>> &Y, vector<vector<bool>> &G, DynamicOracle &oracle);
  void generate_dataset_one(AbstractParseTree const *root, double p, vector<vector<unsigned int>> &X, vector<vector<bool>> &Y, vector<vector<bool>> &G, DynamicOracle &oracle);
  void train_dagger_model(Treebank &train_set, Treebank &dev_set, Treebank::Decoration const &transformation, size_t beam_size, size_t epochs, int dagger_epoch, double p, bool verbose=true); // @@m
  int perceptron_learn_dataset(vector<vector<unsigned int>> &X, vector<vector<bool>> &Y, vector<vector<bool>> &G, SparseFloatMatrix &averaged_model, float &C);
     
protected:
  //@@m >>

  //Learning methods (experimental large margin stuff)
  float large_margin_learn_minibatch(vector<const AbstractParseTree*> const &mbatch,
                                    SparseFloatMatrix &delta,
                                    SparseFloatMatrix &averaged_model,
                                    ModelType modtype,
                                    float &eta,//learning rate
                                    float T,//time step
                                    float t0,//init time step
                                    float C,//PA-I bound
                                    float lambda);//PEGASOS regularizer
                                    //with averaging,
    
   float large_margin_learn_one(AbstractParseTree const  *ref,
                                SparseFloatMatrix &delta,
                                SparseFloatMatrix &averaged_model,
                                ModelType modtype,
                                float &eta,//learning rate
                                int T,//time step
                                float t0,
                                float C,//PA-I bound
                                float lambda);//PEGASOS regularizer
                                //with averaging,
    
   bool collect_large_margin_delta(AbstractParseTree const *ref,TSSBeam &beam,SparseFloatMatrix &delta,float &loss,ModelType modtype);
   void fill_delta(ParseDerivation const &pred_deriv,ParseDerivation const &ref_deriv,InputDag const &input_sequence,SparseFloatMatrix &delta);
   float get_loss(ParseDerivation &pred_deriv,ParseDerivation &ref_deriv,InputDag const &input_sequence,ModelType modtype);
   ParseState* find_max_violation(ParseDerivation &ref_deriv,TSSBeam &beam,InputDag const &input_sequence,size_t N,ModelType modtype);
  //End of experimental stuff
    
  pair<float,float> local_eval_one(AbstractParseTree const *root);
    
  ParseState* early_update_subsequence(ParseDerivation &ref_deriv,
                                        TSSBeam &beam,
                                        InputDag const &input_sequence,
                                        size_t N);//early
  
  
  //transforms the datasets
  void transform_dataset(Treebank &train_set,
                         Treebank &dev_set,
                         Treebank &train_trans,
                         Treebank &dev_trans,
                         Treebank::Decoration const &transform);
    
  void reload(const string &filename);    //reloads a full parser from a directory

  //Main components
  bool transformed = false; //says if the trees have been transformed by a parent or flajolet transform
  size_t max_procs = 1;
  size_t beam_size;
  SrGrammar     grammar;
  SparseEncoder spencoder;
  SparseFloatMatrix  *parsing_model;

};




#endif
