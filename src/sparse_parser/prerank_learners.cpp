#include "boosting.h"
#include <getopt.h>
#ifdef ENABLE_PARALLEL
    #include <omp.h>
#endif

void print_help();

/*-----------------------------------------------
 * COMMAND LINE INTERFACE AND DEFAULT VALUES
 * ---------------------------------------------*/
string output_file; // obligatory
string train_file;
string dev_file;
string skeleton_file;
string template_file;

int num_perceptron_iters; // obligatory
unsigned int num_procs(1);
bool group(false);

/*-----------------------------------------------
 * MAIN
 * ---------------------------------------------*/
int main(int argc,char *argv[]){

    int c;
    srand(time(NULL));

    while(true){
        static struct option long_options[] ={
            {"help",no_argument,0,'h'},
            {"output",required_argument,0,'o'},
            {"skeleton",required_argument,0,'s'},
            {"templates",required_argument,0,'t'},
            {"group",no_argument,0,'g'},
            {"procs",required_argument,0,'p'},
            {"perc",required_argument,0,'i'},
        };
        int option_index = 0;
        c = getopt_long (argc, argv, "hgro:s:t:p:i:",long_options, &option_index);
        if(c==-1){break;}

        switch(c) {
        case 'o':
            output_file = string(optarg);
            break;
        case 's':
            skeleton_file = string(optarg);
            break;
        case 't':
            template_file = string(optarg);
            break;
        case 'g':
            group = true;
            break;
        case 'p':
            num_procs = atoi(optarg);
            break;
        case 'i':
            if (atoi(optarg)<1) {
                cerr << "The number of perceptron iteration must be greater than 0. Aborting"
                     << endl;
                exit(1);
            }
            num_perceptron_iters = atoi(optarg);
            break;
        case 'h':
            print_help();
            break;
        default:
            print_help();
            break;
        }
    }

    /*-----------------------------------------------
     * POSITIONAL ARGUMENTS (TREEBANKS)
     * ---------------------------------------------*/
    if(optind < argc-1){
      train_file = string(argv[optind]);
      dev_file = string(argv[optind+1]);
    }else{
      cerr << "Missing argument(s): you must select a train file and a dev file ... Aborting" <<endl;
      print_help();
      exit(1);
    }

    /*-----------------------------------------------
     * CHECK COHERENCY OF OPTIONS (OBLIGATORY ARGS + VALUES)
     * ---------------------------------------------*/
    //
    if (output_file=="") {
        cerr << "You must select specify an output file. Aborting"
             << endl;
        exit(1);
    }
    if (skeleton_file!="" && template_file!="") {
        cerr << "You must select only one of --skeleton and --templates. Aborting"
             << endl;
        exit(1);
    }
    if (template_file!="" && group) {
        cerr << "You cannot use a default template file AND group. Aborting"
             << endl;
        exit(1);
    }
    if (num_perceptron_iters<1) {
        cerr << "You must select a number of perceptron iterations. Aborting"
             << endl;
        exit(1);
    }
    if (num_procs<1) {
        cerr << "There must be at least one processor selected. Defaulting to one." << endl;
        num_procs=1;
    }

    /*-----------------------------------------------
     * MULTIPROCESSING
     * ---------------------------------------------*/
    #ifdef ENABLE_PARALLEL
        //clog << "number of processors = " << num_procs << endl;
        omp_set_num_threads(num_procs);
    #else
        num_procs = 1;
    #endif

    /*-----------------------------------------------
     * DATA, HEURISTICS AND LOGGING INITIALISATION
     * ---------------------------------------------*/

    // INITIALISE DATA
    Treebank train(train_file);
    Treebank dev(dev_file);
    ParseData data(train, dev);
    //data.get_random_error(); // TODO place within constructor!

     // HEURISTICS
    if (skeleton_file!="") {
        data.set_skeleton(skeleton_file);
        //blogger.skeleton_file = skeleton_file;
    }
    else if (template_file!="") {
        data.set_default_set(template_file);
        //blogger.default_file = template_file;
    }
    else
        data.no_restrictions();

    if (group)
        data.set_grouping(true);

    /*-----------------------------------------------
     * RUN PRERANKING
     * ---------------------------------------------*/
    // calculate time taken to generate (+ preranking)
    time_t ctime = time(0);
    cout << "generating candidates" << endl;
    data.prerank_classifiers();
    time_t prerank_time = time(0) - ctime;
    cout << "Time taken to prerank: " << prerank_time << " seconds." << endl;
    data.dump_ordered_classifiers(output_file);

    return 0;
}

/*-----------------------------------------------
 * PRINT USAGE AND HELP
 * ---------------------------------------------*/
void print_help() {
    cerr << "\nUsage: ./prerank_learners TRAIN_FILEPATH DEV_FILEPATH [-h] [-p INT]\n\t\t[-g] [-s FILENAME] [-t FILENAME] -o FILENAME -i INT " << endl;
    cerr << "\nObligatory arguments:" << endl;
    cerr << "   -o --output     [FILENAME] Sets the output file to FILENAME" << endl;
    cerr << "   -i --maxperc    [INT]        Sets the maximum number of perceptron iterations" << endl;

    cerr << "\nOptional arguments:" << endl;
    cerr << "   -h --help                    Displays this message" << endl;
    cerr << "   -p --procs      [INT]        Sets the number of processors (default=1)" << endl;
    cerr << "   -g --group                   Groups templates" << endl;
    cerr << "   -s --skeleton   [FILENAME]   Sets the skeleton file to FILENAME." << endl;
    cerr << "   -t --templates  [FILENAME]   Sets the default template file to FILENAME." << endl << endl;


    exit(0);
}

