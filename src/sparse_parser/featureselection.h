#pragma once
#include "srparser.h"
#include "sparse.h"
#include "sparse_encoder.h"
#include "treebank.h"
#include "lexer.h"
#include <sys/stat.h>
#include <typeinfo>
#include <map>
#include <time.h>
#include <ostream>
#include <cmath>
#include <unistd.h>

float PERCENTAGE_DATA =  1; // change % of data for boosting between 0 and 1
bool LOG = false;

typedef std::vector<unsigned int> TemplateGroup;
typedef tuple<vector<unsigned int>, ParseAction> LocalExample;

class LocalSet {
    friend class Boost;
public:

    LocalSet(){}
    LocalSet(Treebank train, Treebank dev);
    void add_example(LocalExample example);
    LocalExample& operator[](unsigned int idx);
    LocalSet& operator=(const LocalSet &other_data);
    LocalSet(const LocalSet &other_data);

    unsigned int size();
    void shuffle();

    // weighting and resamping
    LocalSet resample_data();
    void initialise_weights();
    void initialise_weights(std::map<unsigned int, float> &ex2weight);
    void recalculate_weights(set<unsigned int> &correct_examples,
                             double &alpha);


private:
    // generation
    void generate_examples_from_idxs(vector<FeatureTemplate> &fts);
    // resamplng
    unsigned int _get_idx_binsearch(const double rnd, int first=0, int last=-1);
    void resample_idxs(unsigned int number);
    unsigned int _resample_one_idx();

    // treebanks and grammar
    Treebank _bintreebank;
    SrGrammar _grammar;

    // examples
    unsigned int N=0;
    vector<LocalExample> _examples;
    vector<ParseAction> gold_actions;
    set<unsigned int> _correct_examples; // stock indices of correct examples
    vector<vector<unsigned int>> ex2actions; // stock for each example the
                                              // actions predicted by each
                                              // weak model


    // weighting structures
    vector<float> _weights;
    vector<float> _cumul_weights;


    /* structures to identify example indexes with sentence numbers and
     * derivation step numbers */
    std::map<unsigned int, std::tuple<unsigned int, unsigned int>> idx2sentstep;
    std::map<unsigned int, std::map<unsigned int, unsigned int>> sent2step2num;

};


class SearchSpace {
    friend class ForwardMethod;
    friend class StdWrapper;
    friend class Boost;
    friend class LogFeatureSelection;
public:
    SearchSpace(){}
    SearchSpace(const vector<wstring> &lex_info,
                const unsigned int &nqueue=4, const unsigned int &nstack=3,
                const unsigned int &n_max_conditions=3, const bool &group=false,
                const bool &additive_ss=false);
    SearchSpace& operator=(const SearchSpace &other_space);
    SearchSpace(const SearchSpace &other_space);
    unsigned int size() const;
private:
    void _initialise_conditions(vector<wstring> lex_info,
                                const unsigned int nqueue,
                                const unsigned int nstack);
    int _associate_cidx_combos_to_tgidx(const int &k,
                                        vector<unsigned int> included_cnums={},
                                        int start_index=0, int tg_num=0);
    vector<vector<unsigned int>> get_combos(const vector<unsigned int> &cnums,
                                            int k, int start_index=0,
                                            vector<unsigned int> prefix={});

    void _create_template(const vector<unsigned int> &condition_nums,
                          vector<unsigned int> included_ft_nums);
    void _initialise_templates(int k=3, vector<unsigned int> prefix={},
                               vector<unsigned int> included_ft_nums={},
                               int start_index=0);
    bool _template_included(unsigned int ft_num);
    bool _template_in_search_space(unsigned int ft_num);
    void _increase_space(unsigned int last_ft_idx);

    std::map<vector<unsigned int>,vector<unsigned int>> _ccombos2tgs;
    std::map<vector<unsigned int>, unsigned int> _ccombos2tmps;
    std::map<unsigned int, vector<unsigned int>> _tidx2condsidx;
    std::vector<wstring> _conditions;
    std::vector<FeatureTemplate> _templates;
    std::vector<TemplateGroup> _tgroups;
    std::set<unsigned int> _added_tidxs;
    bool _group;
    bool _additive_ss;
    TemplateTypeDefinition _ttd;
    unsigned int _n_max_conditions;
    unsigned int _ntemplates=0;
    unsigned int _ntgs = 0;
};


////////////// LOG AND PRINT OUT //////////////
class LogFeatureSelection{
    friend class ForwardMethod;
    friend class StdWrapper;
    friend class Boost;

public:
    LogFeatureSelection(string &output_folder,
                        bool verbose);
    LogFeatureSelection& operator=(const LogFeatureSelection &other_log);
    LogFeatureSelection(const LogFeatureSelection &other_log);
    ~LogFeatureSelection();
private:
    void _start_clock();
    void _start_log(string algo, bool train_locally,
                    bool eval_locally, int beam_size,
                    unsigned int max_iterations_perceptron,
                    unsigned int nconditions, unsigned int ntemplates,
                    unsigned int ntemplategroups, unsigned int devsize,
                    unsigned int trainsize, bool group, bool additive_ss);
    void _log_iteration(SearchSpace &search_space, const float &acc,
                        const float &acc_gain,
                        vector<unsigned int> &ft_nums,
                        const double &alpha=-1, const float &boost_acc=-1,
                        const float &boost_train_acc=-1,
                        const float &exploss=-1);

    vector<vector<unsigned int>> _templates_added;

    time_t _start_time;
    time_t _last_time;
    int _iteration=1;
    bool _verbose;

    string _output_folder;
    ofstream _logfile;
    wofstream _templatefile;
    //ofstream _alphafile; // only for boosting
};


////////////// BASE CLASS FOR FORWARD SELECTION //////////////
class ForwardMethod {
public:
    void run(unsigned int max_iterations, bool verbose=true);
    void run(bool verbose=true);
    ForwardMethod(const SearchSpace &search_space, string &output_folder,
                  const unsigned int &max_perceptron_iterations,
                  const unsigned int &beam_size, bool verbose);
protected:
    virtual tuple<unsigned int, float> _select_next(bool verbose=true) = 0;
    virtual void _add_templates_to_final_model(vector<unsigned int> &ft_nums,
                                               float acc) = 0;
    virtual void _reset_threshold(float &acc) = 0;
    virtual void _log_iteration(float next_acc,
                                vector<unsigned int> &ft_nums) = 0;
    void _remove_templates_from_search_space(unsigned int tg_index);


    SearchSpace _search_space;
    unsigned int _max_perceptron_iterations;
    unsigned int _beam_size;
    float _threshold_acc;
    int _next_tg_idx;
    float _next_acc;
    string _algo;
    LogFeatureSelection _log;

};


////////////// STANDARD FORWARD WRAPPER //////////////
class StdWrapper : public ForwardMethod {
public:
    StdWrapper(const SearchSpace &search_space,
                      const Treebank &dev,
                      const Treebank &train,
                      string &output_folder,
                      unsigned int &max_perceptron_iterations,
                      unsigned int &beam_size,
                      bool train_locally=false,
                      bool eval_locally=false,
                      bool verbose=true);
protected :
    tuple<unsigned int, float> _select_next(bool verbose=false);
    void _update_current_encoder(vector<unsigned int> &ft_nums);
    void _add_templates_to_final_model(vector<unsigned int> &ft_nums,float acc);
    void _reset_threshold(float &acc);
    void _add_to_encoder(SparseEncoder &temp_encoder, unsigned int tg_index,
                         bool verbose=true);
    virtual void _log_iteration(float next_acc,
                                vector<unsigned int> &ft_nums);
    void _train_model(SrParser &srp, bool verbose=true);
    float _evaluate_model(SrParser &srp, bool verbose=true);

    Treebank _dev;
    Treebank _train;
    bool _train_locally;
    bool _eval_locally;
    SparseEncoder _current_encoder;

    string algo ="Standard Forward Wrapper";
};


////////////// BOOSTING VARIANT //////////////
class Boost : public ForwardMethod {
public:
    Boost(const SearchSpace &search_space,
          Treebank &dev,
          Treebank &train,
          string &output_folder,
          unsigned int &max_perceptron_iterations,
          unsigned int &beam_size,
          bool verbose=true,
          bool balance=false);
protected:
    void _reset_threshold(float &acc){}
    void _add_templates_to_final_model(vector<unsigned int> &ft_nums,
                                       float acc);
    tuple<unsigned int, float> _select_next(bool verbose=true);
    void _log_iteration(float next_acc, vector<unsigned int> &ft_nums);
    double calculate_alpha(float acc);
    void _dump_model(vector<unsigned int> &ft_nums);

    // training
    SparseFloatMatrix* _train_local_model(vector<unsigned int> &ft_nums,
                                          bool verbose);
    int _local_learn_one(const LocalExample &local_example,
                         SparseFloatMatrix &averaged_model,
                         SparseFloatMatrix *parsing_model, int C);

    // prediction and evaluation
    tuple<float, set<unsigned int>, std::vector<unsigned int> > _eval_local(
            vector<unsigned int> &ft_nums,SparseFloatMatrix* parsing_model);
    int _local_eval_one(const int example_num, SparseFloatMatrix* parsing_model,
                        vector<unsigned int> &ft_nums);
    unsigned int _predict_one_action(const LocalExample &local_example,
                                     SparseFloatMatrix* parsing_model,
                                     vector<unsigned int> &ft_nums);
    tuple<float, float> boost_predict(vector<vector<unsigned int>> ex2actions,
                                      vector<ParseAction> gold_actions);

    // data manipulation
    void _prepare_local_data(Treebank &dev, Treebank &train);
    void _get_argmaxes_on_devset(vector<unsigned int> &ft_nums,
                                        SparseFloatMatrix *parsing_model);

    string algo="Boosting";
    LocalSet _dev;
    LocalSet _train;
    SrGrammar _grammar;
    vector<tuple<double, int>> _alphas_ntemps; // alpha, ntemplates
    SparseFloatMatrix* _best_parse_model;
    unsigned int _nactions;



    // DEBUGGING
    bool _balance; // balance data by class (even distribution at beginning)
    std::map<unsigned int, unsigned int> action2number; // number of each action
    std::map<unsigned int, float> ex2weight;

};











