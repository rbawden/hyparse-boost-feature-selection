#!/usr/bin/python
# -*- coding: utf-8 -*-
import argparse
import os
import subprocess
import glob
import multiprocessing
from functools import partial
from time import *
import numpy as np
from joblib import Parallel, delayed
import math

#--------------------------------------------------
#   ARGUMENTS
#--------------------------------------------------

parser = argparse.ArgumentParser(description=""+os.sys.argv[0])

nargs=1

parser.add_argument('--folder', help=u"Experiment folder", action='store', dest='folder', metavar=('folder'),required=True)
parser.add_argument('-all', '--all', help=u"All data files", action='store', dest='all', metavar=('train.tbk', 'dev.tbk', 'test.raw', 'test.mrg'), required=True, nargs = 4)
parser.add_argument('-small', '--small', help=u"Data file <=20", action='store', dest='small', metavar=('train.tbk','dev.tbk','test.raw', 'test.mrg'), required=False, nargs = 4)
parser.add_argument('-p', '--p', help=u"Parameter file", action='store', dest='param', metavar=('paramfile'), required=True)
parser.add_argument('-i', '--i', help=u"Perceptron iterations", action='store', dest='percepiters', metavar=('INT'), required=True)
parser.add_argument('--beam', help=u"Beam size", action='store', dest='beamsize', metavar=('Beam size'),required=True)
parser.add_argument('--procs', help=u"Proc nums", action='store', dest='procnums', metavar=('Proc nums'), default=1)

args = parser.parse_args()  

'''if args.procnums != None:
	pool = multiprocessing.Pool( int(args.procnums) )
else:
	pool = multiprocessing.Pool(1)
'''
nprocs=int(args.procnums)
folder = args.folder
trainf = args.all[0]
devf = args.all[1]
testraw = args.all[2]
testmrg = args.all[3]
param = args.param
percepiters = args.percepiters
beam=args.beamsize

if args.small!=None:
    trainf20 = args.small[0]
    devf20 = args.small[1]
    test20raw = args.small[2]
    test20mrg = args.small[3]
else:
    trainf20 = None
    devf20 = None
    test20raw = None
    test20mrg = None  
    
#--------------------------------------------------
#   GLOBAL VARIABLES
#--------------------------------------------------
    
#--------------------------------------------------
#   FUNCTIONS
#--------------------------------------------------

def delete_slash(folder):
    if folder[-1]=="/": return folder[:-1]
    else: return folder

def train_model(iternum, model_size, template_list, iterations,beam, output_folder):
    print "\n*** Training and evaluating model at iteration "+str(iternum)+" of size "+str(model_size)+" ***"    

    templates = template_list[0:model_size]
    subprocess.check_output(['mkdir', '-p', output_folder+"/temp_"+str(iternum)]) 
    subprocess.check_output(['mkdir', '-p', output_folder+"/tpls"]) 
    
    # create template file
    tplf = output_folder+"/tpls/"+str(model_size)+"templates"+str(iternum)+".tpl"
    tplp = open(tplf, "w")
    for template in templates:
        print>>tplp, template
    tplp.close()
    
    ftb20_with_punct_f = "_"
    ftb20_without_punct_f = "_"
    ftb40_with_punct_f = "_"
    ftb40_without_punct_f = "_"
    ftball_with_punct_f = "_"
    ftball_without_punct_f = "_"

    # train ftb20 -> stock in output_folder+"/temp"
    if args.small!=None:
        # training
        command = "./srt -b "+str(beam)+" -t "+tplf+" -i "+str(iterations)+" -m "+output_folder+"/temp_"+str(iternum)+" "+trainf20+" "+devf20
        subprocess.call(command, shell=True)
       
        # predictions
        command = "./srp -m "+output_folder+"/temp_"+str(iternum)+" -I "+test20raw+" -O "+output_folder+"/predictions/"+str(model_size)+"_20-test-pred.mrg"
        subprocess.call(command, shell=True)
        
        # evaluating
        command = "./EVALB/evalb "+test20mrg+" "+output_folder+"/predictions/"+str(model_size)+"_20-test-pred.mrg > "+output_folder+"/evaluations/"+str(model_size)+"_20_evaluation_with_punct.txt 2>&1"
        subprocess.call(command, shell=True)
        
        command = "./EVALB/evalb "+test20mrg+" "+output_folder+"/predictions/"+str(model_size)+"_20-test-pred.mrg -p "+param+" > "+output_folder+"/evaluations/"+str(model_size)+"_20_evaluation_without_punct.txt 2>&1"
        subprocess.call(command, shell=True)

        # move trainlog to folder
        command = "mv "+output_folder+"/temp_"+str(iternum)+"/train.log "+output_folder+"/trainlogs/"+str(model_size)+"_20_train.log"
        subprocess.call(command, shell=True)
        
        # print results
        evalf = open(output_folder+"/evaluations/"+str(model_size)+"_20_evaluation_with_punct.txt", "r")
        evalc = evalf.read().split("\n")
        evalf.close()

        ftb20_with_punct_f = evalc[-7].split("=")[1].strip()
        
        evalf = open(output_folder+"/evaluations/"+str(model_size)+"_20_evaluation_without_punct.txt", "r")
        evalc = evalf.read().split("\n")
        evalf.close()
        ftb20_without_punct_f = evalc[-7].split("=")[1].strip()

        
    # on full data
    # training
    command = "./srt -b "+str(beam)+" -t "+tplf+" -i "+str(iterations)+" -m "+output_folder+"/temp_"+str(iternum)+" "+trainf+" "+devf
    print(command)
    subprocess.call(command, shell=True)
   
    # predictions
    
    command = "./srp -m "+output_folder+"/temp_"+str(iternum)+" -I "+testraw+" -O "+output_folder+"/predictions/"+str(model_size)+"_all-test-pred.mrg"
    print(command)
    subprocess.call(command, shell=True)
    
    # evaluating
    command = "./EVALB/evalb "+testmrg+" "+output_folder+"/predictions/"+str(model_size)+"_all-test-pred.mrg > "+output_folder+"/evaluations/"+str(model_size)+"_all_evaluation_with_punct.txt" # 2>&1"
    print(command)
    subprocess.call(command, shell=True)
    
    command = "./EVALB/evalb "+testmrg+" "+output_folder+"/predictions/"+str(model_size)+"_all-test-pred.mrg -p "+param+" > "+output_folder+"/evaluations/"+str(model_size)+"_all_evaluation_without_punct.txt" # 2>&1"
    print(command)
    subprocess.call(command, shell=True)

    # move trainlog to folder
    command = "mv "+output_folder+"/temp_"+str(iternum)+"/train.log "+output_folder+"/trainlogs/"+str(model_size)+"_all_train.log"
    subprocess.call(command, shell=True)
    
    # print results
    evalf = open(output_folder+"/evaluations/"+str(model_size)+"_all_evaluation_with_punct.txt", "r")
    evalc = evalf.read().split("\n")
    evalf.close()
    #print()
    #print(evalc)
    #raw_input()
    ftb40_with_punct_f = evalc[-7].split("=")[1].strip()
    ftball_with_punct_f = evalc[-21].split("=")[1].strip()
    
    evalf = open(output_folder+"/evaluations/"+str(model_size)+"_all_evaluation_without_punct.txt", "r")
    evalc = evalf.read().split("\n")
    evalf.close()
    ftb40_without_punct_f = evalc[-7].split("=")[1].strip()
    ftball_without_punct_f = evalc[-21].split("=")[1].strip()
    
            
    # empty temp folder
    #subprocess.check_output(['rm', '-r', output_folder+"/tpls_"+str(iternum)])     
    subprocess.check_output(['rm', '-r', output_folder+"/temp_"+str(iternum)])     

    return [ftb20_with_punct_f, ftb20_without_punct_f, ftb40_with_punct_f, ftb40_without_punct_f, ftball_with_punct_f, ftball_without_punct_f]
        
#--------------------------------------------------
#   MAIN
#--------------------------------------------------

if __name__ == '__main__':

    template_list = []
    model_sizes = []  

    # create files and folders for results, trainlogs and predictions
    #folder=delete_slash(folder)
    #resultsfile = open(folder+"/results.csv", "w")
    #print>>resultsfile, "Iternum\tSizeModel\tF20with\tF20without\tF40with\tF40without\tFAllwith\tFAllwithout"
    #resultsfile.close()
    
    subprocess.check_output(['mkdir', '-p', folder+"/trainlogs"]) 
    subprocess.check_output(['mkdir', '-p', folder+"/predictions"])
    subprocess.check_output(['mkdir', '-p', folder+"/evaluations"])     
    
    # get all templates
    tplfile = open(folder+"/templates.tpl", "r")
    template_list = tplfile.read().split("\n")
    template_list = [x for x in template_list if x not in [" ", ""]]
    tplfile.close()
    
    # get model sizes
    logfile = open(folder+"/log", "r")
    rows = logfile.read().split("\n")
    rows = [x for x in rows if x not in [" ", ""]]
    logfile.close()
    
    currentmodelsize=-1
    for i, row in enumerate(rows):
        if i==0: continue
        rowmodelsize = int(row.split("\t")[3])
        if rowmodelsize>currentmodelsize:
            model_sizes.append(rowmodelsize)
            currentmodelsize=rowmodelsize

    res = open(folder+"/results.csv", "w")
    print>>res, "Iternum\tSizeModel\tF20with\tF20without\tF40with\tF40without\tFAllwith\tFAllwithout"
    res.close()
    # process in groups of 10
    num=0
    
    print("There are "+str(len(model_sizes))+" models to train")
    print("There will be "+str(int(math.ceil(len(model_sizes)/float(10))*num))+" batches")

    for i in range(0, int(math.ceil(len(model_sizes)/float(5)))):
        print("Batch number "+str(i+1))

        # train each of the models and stock predictions
        results = Parallel(n_jobs=nprocs)(delayed(train_model)(iternum+(i*5), 
                                                                model_size, 
                                                                template_list, 
                                                                percepiters,beam, 
                                                                folder)
                                           for iternum, model_size in enumerate(model_sizes[i*5:min((i*5)+5, len(model_sizes))]))
        num+=1
        #ftb20_with_punct_f, ftb20_without_punct_f, ftb40_with_punct_f, ftb40_without_punct_f, ftball_with_punct_f, ftball_without_punct_f)
        
        print("Writing results")
        
        res = open(folder+"/results.csv", "a")
        for j, r in enumerate(results):
            r = [str(x) for x in r]
            #print r
            res.write(str((i*5)+j)+"\t"+str(model_sizes[(i*5)+j])+"\t"+"\t".join(r)+"\n")
            res.flush()
        res.close()        

    #iternum=0
    #for model_size in model_sizes:
    #    iternum+=1
    #    print "\n*** Training and evaluating model at iteration "+str(iternum)+" of size "+str(model_size)+" ***"
    #    train_model(iternum, model_size, template_list, percepiters, folder)
        


    
    









    
