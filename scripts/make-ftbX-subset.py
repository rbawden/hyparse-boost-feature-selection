#!/usr/bin/python2.7
# -*- coding: utf-8 -*-

def filtering(filename, maxlen):
    f = open(filename, "r")
    content = f.readlines()
    f.close()
    
    # line 1 = headers
    headers = content[0].strip().split("\t")
    
    # stock others
    trees = [""]
    for line in content[1:]: 
        trees[-1]+=line
        if "</ROOT" in line:
            trees.append("")
                
    # only keep those that have length <= maxlen
    kept_trees = []
    for tree in trees:
        num = len([branch for branch in tree.split("\n") if len(branch.strip().split("\t")) == len(headers)])
        if num <= maxlen: kept_trees.append(tree)
        
        
    # print out
    print "\t".join(headers)
    for tree in kept_trees: 
    	print tree
    	#input()
                


if __name__=="__main__":
    
    import argparse
    parser = argparse.ArgumentParser(description='Filter a corpus using a maximum sentence length. Outputs to stdout.')
    parser.add_argument("corpus", help="corpus in .tbk format")
    parser.add_argument("maxlen")
    args = parser.parse_args()
    corpus = args.corpus
    maxlen = args.maxlen
    filtering(corpus, int(maxlen))
    
    
