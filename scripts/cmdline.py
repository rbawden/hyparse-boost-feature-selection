#! /usr/bin/env python3

"""
GET columns from train dev test
MERGE filenameA ... into train dev test
TRAIN templates.tpl
TEST
LEARN_STATS
PARSE_STATS

//supposed to be located in the setup dir of the top level language dir           
"""
import cmd
import sys
import itertools
import os
import os.path

def get_treebank_path(scenario,family,corpus,format):
    """
    Gets a path to some treebank file into the SPMRL dir structure
    """
    base_path = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
    assert(scenario in ['gold','pred'])
    assert(family in ['ptb','conll'])
    assert(corpus in ['train','dev','test'])
    assert(format in ['raw','tbk','mrg'])
    p = os.path.join(base_path,scenario,family,corpus,'treebank.'+format)
    return p
    
#CORPUS OPS
def csplit(istream,ostream,colnames):
    """
    Generates a raw conll kind of file from a .tbk file
    @param istream: a .tbk format
    @param ostream: a .raw format
    @param colnames:names of the columns to extract 
    """
    header       = istream.readline().split()
    target_idxes = set([header.index(c) for c in colnames])
    tag_idx = header.index('tag')
    subheader = [elt for idx,elt in enumerate(header) if idx in target_idxes]
    print('\t'.join(subheader),file=ostream)
    
    prev_white = False
    for line in istream:
        if line.isspace():
            if not prev_white:
                print(line,file=ostream)
            prev_white = True
            continue

        tokens = line.split()
        first = tokens[0]
        if first == '</ROOT>':
            if not prev_white:
                print('',file=ostream)
            prev_white = True
            continue
        if first[0] == '<' and first[-1] == '>':
            continue
        if tag_idx >=0:
            tokens[tag_idx] = tokens[tag_idx].split('-')[0] #removes the head annotation if it exists
        subline = [elt for idx,elt in enumerate(tokens) if idx in target_idxes]
        print('\t'.join(subline),file=ostream)
        prev_white=False

def check_merge(istreamA,istreamB):
    """
    Optional check that can be done before merging two files.
    Checks if the files have same number of tokens
    """
    ntoksA = 0
    for line in istreamA:
         if not line.isspace():
            toks = line.split()
            first = toks[0]
            if not(first[0] == '<' and first[-1] == '>'):
                ntoksA += 1
    ntoksB = 0
    for line in istreamB:
         if not line.isspace():
            toks = line.split()
            first = toks[0]
            if not(first[0] == '<' and first[-1] == '>'):
                ntoksB += 1
    return (ntoksA == ntoksB)


def table2latex(istream):
    """
    Outputs a table as a latex formatted booktabs compatible table
    """
    bfr = istream.readline()
    header = bfr.split()
    lout = r'\begin{tabular}{'+'c'*len(header) + '}\n'+r'\topline'+'\n'
    lout += '&'.join(header)+ r'\\'+'\n\midline\n'
    for line in istream:
        lout += '&'.join(line.split())+ r'\\'+'\n'
    lout += r'\bottomline'+ '\n\end{tabular}\n'
    return lout
    
def next_token(istream,ostream=None):
    """
    Helper function that advances the stream to the next token
    (skipping whitespace and bracket lines)
    @param ostream: a stream where to copy verbatim skipped lines
    @return a non whitespace, non bracket line ready to be processed or None if end of stream is reached
    """
    bfr = istream.readline()
    while bfr:
        if bfr.isspace():
            if ostream:
                ostream.write(bfr)
            bfr = istream.readline()
        else:
            tokens = bfr.split()
            if tokens[0][0] == '<' and tokens[0][-1] == '>':
                if ostream:
                    ostream.write(bfr)
                bfr = istream.readline()
            else:
                return bfr
    return ''

def indentation(astr):
    "returns the indentation of a string"
    return ''.join(itertools.takewhile(str.isspace,astr))
         
def cmerge(isBase,isAdd,ostream):
    """
    Merges Additional columns into Base and stores the result in ostream.
    In case istreamAdd contains columns with identical names to the Base, the
    former has precedence and overwrites the Base values (except for word column)
    @return the header of the merged files
    """
    basebfr = isBase.readline()
    addbfr  = isAdd.readline()

    baseHeader = basebfr.split()
    addHeader  = addbfr.split()
    finalHeader = baseHeader + list([elt for elt in addHeader if elt not in baseHeader])
    diffN = len(finalHeader)-len(baseHeader)
    final_indexes = list([finalHeader.index(elt) for elt in addHeader])

    print('\t'.join(finalHeader),file=ostream)

    word_idx = finalHeader.index('word')
    tag_idx = finalHeader.index('tag')
    
    basebfr = next_token(isBase,ostream)
    addbfr  = next_token(isAdd)
    while basebfr and addbfr:
        baseindent = indentation(basebfr)
        base_toks = basebfr.split()
        add_toks = addbfr.split()
        #merges lines
        merged = base_toks + ['']*diffN
        for idx,elt in zip(final_indexes,add_toks):
            if idx == tag_idx:
                suff = merged[idx].split('-')[1:] #keeps the -head annotation suffix on tags
                merged[idx] = '-'.join([elt]+suff)
            elif idx != word_idx:                 #never alter base words
                merged[idx] = elt
        print(baseindent+'\t'.join(merged),file=ostream)
        basebfr = next_token(isBase,ostream)
        addbfr  = next_token(isAdd)        
    return finalHeader

class ParserConfig:

    def __init__(self):
        self.beamsize = 4
        self.niterations = 25
        self.nprocs = 1
        self.mbatch_size = 1
        self.algo = "PERCEPTRON"
        self.local = False
        self.maxv = False
        self.C = 0.01
        self.Lambda = 0.001

        
class ParserCommander(cmd.Cmd):
    
    def __init__(self):
        cmd.Cmd.__init__(self)
        self.sc = 'pred'
        self.colnames = self.get_colnames() #column names of tbk and raw corpora
        self.model = 'generic'
        self.config = ParserConfig()

        self.update_prompt()
        self.intro = """
        Parser command line interface for running experiments.
        Allows to customize data and analyze parsing results.
        Type 'help' or '?' for details.
        Type '!' cmd for running a regular shell command

        Make sure srt and srp and evalb(_spmrl) are in your system PATH
        before trying to train or parse a corpus.
        """
                
    def do_PSUMMARY(self,cmdstring):
        """PSUMMARY [ test ] [ file ] [ 40 ]
        computes overall Precision, Recall, F-score, POS acc, Exact match, Avg time, on dev set (or test if parameter is supplied)
        """
        corpus = 'dev'
        out = sys.stdout
        is40 = False
        fileout = False
        cfields = cmdstring.split()
        if len(cfields) > 0:
            if 'test' in cfields:
                corpus = 'test'
            if '40' in cfields:
                cfields.remove('40')
                is40 = True
            for elt in cfields:
                if elt != 'test':
                    out = open(elt,'w')
                    fileout = True
        self.do_PSTATS(' '.join(cfields),show=False)
        rfile = self.model+'.'+corpus+'.pred'
        if not os.path.exists(rfile):
            print('No parsed file found for this model. Run the parse command before evaluating.')
            return
        evalfile = rfile+'.evalb'
        sfile = open(evalfile)
        ALL = False
        _40 = False
        P = R = F = T = E = 0
        for line in sfile:
            if line[0] == '-' and not ALL:
                ALL = True
                _40 = False
            elif line[0] == '-' and ALL:
                _40 = True
                ALL = False
            if is40 and _40 and line.startswith("Bracketing Recall"):
                R = line.strip().split('=')[1].strip()
            if is40 and _40 and line.startswith("Bracketing Precision"):
                P = line.strip().split('=')[1].strip()
            if is40 and _40 and line.startswith("Bracketing FMeasure"):
                F = line.strip().split('=')[1].strip()
            if is40 and _40 and line.startswith("Complete match"):
                E = line.strip().split('=')[1].strip()
            if is40 and _40 and line.startswith("Tagging accuracy"):
                T = line.strip().split('=')[1].strip()
            if not is40 and ALL and line.startswith("Bracketing Recall"):
                R = line.strip().split('=')[1].strip()
            if not is40 and ALL and line.startswith("Bracketing Precision"):
                P = line.strip().split('=')[1].strip()
            if not is40 and ALL and line.startswith("Bracketing FMeasure"):
                F = line.strip().split('=')[1].strip()
            if not is40 and ALL and line.startswith("Complete match"):
                E = line.strip().split('=')[1].strip()
            if not is40 and ALL and line.startswith("Tagging accuracy"):
                T = line.strip().split('=')[1].strip()
        if not fileout:
            print('\t'.join(['P','R','F','Tag','EM']),file=out)
        print('\t'.join([str(P),str(R),str(F),str(T),str(E)]),file=out)
        if fileout:
            out.close()
        sfile.close()

    def do_SET_EPOCHS(self,cmdstring):
        """SET_EPOCHS [int]
        sets the number of epochs for training
        """
        try:
            self.config.niterations = int(cmdstring)
        except:
            print("Error. Could not interpret value as an integer.")
        
    def do_PSTATS(self,cmdstring,show=True):
        """PSTATS  [test] [ latex ] [ file ]
        Runs evalb and merges it into the parse stats results 
        """
        corpus = 'dev'
        latex = False
        out = sys.stdout
        fileout = False
        cfields = cmdstring.split()
        if len(cfields) > 0:
            if 'test' in cfields:
                corpus = 'test'
            if 'latex' in cfields:
                latex = True
            for elt in cfields:
                if elt != 'test' and elt != 'latex':
                    out = open(elt,'w')
                    fileout = True
        #run evalb
        gfile = get_treebank_path('gold','ptb',corpus,'mrg')
        rfile = self.model+'.'+corpus+'.pred'
        paramfile = 'spmrl.prm'
        if not os.path.exists(rfile):
            print('No parsed file found for this model. Run the parse command before evaluating.')
            return
        evalfile = rfile+'.evalb'
        self.do_shell('evalb_spmrl -p %s %s %s > %s'%(paramfile,str(gfile),rfile,evalfile))
        #reads in the evalb scores
        evalb_scores = []
        ef = open(evalfile)
        bfr = ef.readline()
        while bfr[0] != '=':
            bfr = ef.readline()
        for line in ef:
            if line[0] == '=':
                break
            fields = line.split()
            F = 0.0
            if (float(fields[3])+float(fields[4]))  > 0:
                F = 2*(float(fields[3])*float(fields[4]))/(float(fields[3])+float(fields[4]))
            evalb_scores.append((fields[3],fields[4],str(F)))
        #reads in the parse stats
        parse_stats = []
        p = os.path.dirname(os.path.abspath(__file__))
        p = os.path.join(p,self.model+'.model','parse.log')
        stats_file = open(p)
        bfr = stats_file.readline()#skip header
        header = bfr.split()
        for line in stats_file:
            parse_stats.append(line.split())
        stats_file.close()
        #merge all and replaces parse stats
        stats_file = open(p,'w')
        print('\t'.join(header),file=stats_file)
        for prf,ps in zip(evalb_scores,parse_stats):
            ps[0] = prf[0]
            ps[1] = prf[1]
            ps[2] = prf[2]
            print('\t'.join(ps),file=stats_file)
        stats_file.close()
        #display
        if show:
            stats_file = open(p)
            if latex:
                out.write(table2latex(stats_file))
            else:
                for line in stats_file:
                    out.write(line)
            stats_file.close()
        if fileout:
            out.close()

    def do_FSTATS(self,cmdstring):
        """
        Computes the accurracy of pred features given as input to the parser
        """
        corpus = 'dev'
        out = sys.stdout
        cfields = cmdstring.split()
        if len(cfields) > 0:
            if 'test' in cfields:
                corpus = 'test'
        #run evalb
        gfile = get_treebank_path('gold','ptb',corpus,'raw')
        rfile = get_treebank_path('pred','ptb',corpus,'raw')
        if not os.path.exists(rfile):
            print('No parsed file found for this model. Run the parse command before evaluating.')
            return
        gold = open(gfile)
        header = gold.readline().split()[1:]
        d = []
        for h in header:
            d.append([])
        for line in gold:
            vals = line.split()[1:]
            for idx,v in enumerate(vals):
                d[idx].append(v)
        pred = open(rfile)
        predd = []
        pheader = pred.readline().split()[1:]
        assert(len(header) == len(pheader))
        for h in pheader:
            predd.append([])
        for line in pred:
            vals = line.split()[1:]
            for idx,v in enumerate(vals):
                predd[idx].append(v)
                
        for idx,elt in enumerate(header):
            print(elt,sum([ a==b for a,b in zip(d[idx],predd[idx])])/float(len(d[idx])),file=out)

                
                
    def do_LSTATS(self,cmdstring):
        """LSTATS [ latex ] [ filename ]
        Extracts the stats computed during the learning process.
        The stats may be turned into a latex table (booktabs) and or dumped to filename
        Example:
        LSTATS latex foo.tex
        """
        latex=False
        out = sys.stdout
        fields = cmdstring.split()
        if len(fields) > 0:
            if fields[0]== 'latex':
                latex = True
        if len(fields) > 1:
            out = open(fields[1],'w')
             
        p = os.path.dirname(os.path.abspath(__file__))
        p = os.path.join(p,self.model+'.model','train.log')
        if not os.path.exists(p):
            print(str(p))
            print ('Error, no learning stats available. You should train the model first.')
            return
        statsin = open(str(p))
        if latex:
            out.write(table2latex(statsin))
        else:
            for line in statsin:
                out.write(line)
        if len(fields) > 1:
            out.close()
        statsin.close()
            
    def do_SET_PROCS(self,cmdstring):
        """SET_PROCS int
        Sets the number of processor to int
        """
        try:
            self.config.nprocs = int(cmdstring.strip())
            if self.config.nprocs > 1:
                self.config.mbatch_size =  self.config.nprocs * 4
        except:
            print("Error. Could not interpret value as an integer.")
        
        
    def do_SET_BEAM(self,cmdstring):
        """SET_BEAM int
        Sets the beam value to int"""
        try:
            self.config.beamsize = int(cmdstring)
        except:
            print("Error. Could not interpret value as an integer.")

    def do_SET_LEARNER(self,cmdstring):
        """SET_LEARNER [PERCEPTRON,PERCEPTRON-MAX,PERCEPTRON-LOCAL,MIRA Cvalue ,PEGASOS lambda-value]
        Selects the learning algorithm and its regularization parameter (if applicable)
        Example: SET_LEARNER MIRA 0.01
        """
        fields = cmdstring.split()
        if fields[0] in ['PERCEPTRON','PERCEPTRON-MAX','PERCEPTRON-LOCAL','MIRA','PEGASOS']:
            self.config.algo = fields[0]
            if fields[0] == 'PERCEPTRON-MAX':
                self.config.algo = 'PERCEPTRON'
                self.config.maxv = True
                self.config.local = False 
            if fields[0] == 'PERCEPTRON-LOCAL':
                self.config.algo = 'PERCEPTRON'
                self.config.local = True 
            if  fields[0] == 'MIRA':
                self.config.C = fields[1]
                self.config.maxv = False
                self.config.local = False 
            elif fields[0] == 'PEGASOS':
                self.config.Lambda = fields[1]
                self.config.maxv = False
                self.config.local = False 
        else:
            print('Error:: unknown learning algorithm')
        
    def do_TRAIN(self,cmdstring):
        """TRAIN [modelname]
        trains a parser with the setup specified and stores the model.
        <modelname> should be chosen such that a template file modelname.tpl is accessible within the setup dir.
        The model will be stored in the setup dir as modelname.model"""

        
        self.model =  cmdstring.strip()
        trainfile = get_treebank_path(self.sc,'ptb','train','tbk')
        devfile = get_treebank_path(self.sc,'ptb','dev','tbk')

        if not os.path.exists(self.model+'.tpl'):
            print("Error templates file %s not found. aborting."%(self.model+'.tpl',))
            return
            
        if self.config.local:
              self.do_shell('srt -l -i %d -b%d -t%s -m %s %s %s'%(self.config.niterations,
                                        self.config.beamsize,
                                        self.model+'.tpl',
                                        self.model+'.model',
                                        trainfile,
                                        devfile))
        else:
            pstring = '-p%d'%(self.config.nprocs) if self.config.nprocs > 1 else ''
            batchstring = '-B%d'%(self.config.mbatch_size)  if self.config.mbatch_size > 1 else ''
            maxvstring = '-V' if self.config.maxv else ''
            if self.config.algo == 'PERCEPTRON':
                self.do_shell('srt %s %s %s -i %d -b%d -t %s -m %s %s %s'%(
                    maxvstring,
                    pstring,
                    batchstring,
                    self.config.niterations,
                    self.config.beamsize,
                    self.model+'.tpl',
                    self.model+'.model',
                    trainfile,
                    devfile))
            elif self.config.algo == 'MIRA':
                mirastring = '-M%f'%(self.config.C)
                self.do_shell('srt %s %s %s -i %d -b%d -t%s -m %s %s %s'%(
                    mirastring,
                    pstring,
                    batchstring,
                    self.config.niterations,
                    self.config.beamsize,
                    self.model+'.tpl',
                    self.model+'.model',
                    trainfile,
                    devfile))
            elif self.config.algo == 'PEGASOS':
                pegasosstring = '-P%s'%(self.config.Lambda)
                self.do_shell('srt %s %s %s -i %d -b%d -t%s -m %s %s %s'%(
                    pegasosstring,
                    pstring,
                    batchstring,
                    self.config.niterations,
                    self.config.beamsize,
                    self.model+'.tpl',
                    self.model+'.model',
                    trainfile,
                    devfile))
        self.update_prompt()

    def do_PARSE(self,cmdstring):
        """PARSE  
        Parses dev files and test files
        """
        if not os.path.exists(self.model+'.model'):
            print('Error:: model has not been trained.\n Please train it before attempting to parse.')
            return
        dev_path = get_treebank_path(self.sc,'ptb','dev','raw')
        test_path = get_treebank_path(self.sc,'ptb','test','raw')
        self.do_shell('srp -m %s -I %s -O %s -C%s'%(self.model+'.model',dev_path,self.model+'.dev.pred',self.model+'.dev.conll'))
        self.do_shell('srp -m %s -I %s -O %s -C%s'%(self.model+'.model',test_path,self.model+'.test.pred',self.model+'.test.conll'))
        print('done.') 

    def do_EOF(self, line):
        """Exits the interpreter
        """
        return True
    
    def update_prompt(self):
        self.prompt = ''
        if self.use_rawinput:
            self.prompt = '%s#%s> '%(self.model,self.sc)

    def do_shell(self, line):
        "Run a shell command"
        output = os.popen(line).read()
        sys.stdout.write(output)

    def do_SET_MODEL(self,cmdstring):
        """SELECT_MODEL modelname
        if modelname.model is a trained parsing model dir in the current working directory,
        This model will be used by further parsing commands
        """
        name = cmdstring.strip()
        if os.path.isdir(name+'.model'):
            self.model = name
            self.update_prompt()
        else:
            print ('No such model in current working directory')
    
    def do_SWITCH_SCENARIO(self,cmdstring):
        """Switches the scenario from pred to gold and vice versa
        """
        if self.sc == 'pred':
            self.sc = 'gold'
        else:
            self.sc = 'pred'
        print("scenario set to %s"%(self.sc))
        self.get_colnames()
        self.update_prompt()
        
    def do_COLNAMES(self,cmdstring):
        """Shows the available column names in the data sets
        """
        print(','.join(self.colnames))
        
    def get_colnames(self):
        p = get_treebank_path(self.sc,'ptb','test','tbk')
        f = open(str(p))
        self.colnames = f.readline().split()
        f.close()
        return self.colnames

    def do_QUIT(self,cmdstring):
        """
        Exits the interpreter
        """
        exit(0)

    def do_GSIZE(self,cmdstring):
        """GSIZE
        Returns the actual number of non terminals (including temporaries) in the grammar 
        """
        p = os.path.dirname(os.path.abspath(__file__))
        p = os.path.join(p,self.model+'.model','grammar')
        gf = open(p)
        c=0
        bfr = gf.readline()
        while bfr and bfr[0]!= '#':
            c += 1
            bfr = gf.readline()
        print (c)

    def do_PTIME(self,cmdstring):
        """PTIME
        Computes average parsing times on the test set
        """
        p = os.path.dirname(os.path.abspath(__file__))
        p = os.path.join(p,self.model+'.model','parse.log')
        logf = open(p)
        s= 0.0
        t = 0
        logf.readline()#skips header
        for line in logf:
            s+= float(line.split()[3])
            t+=1
        logf.close()
        print(s/float(t))
        
    def do_MERGE(self,cmdstring):
        """MERGE filename filenameB filenameC
        Merges three raw files into the train dev and test.
        Appends their content to tokens into treebank and raw files
        If two columns have same name, new columns replace old ones 
        """
        args = cmdstring.split()
        if len(args) != 3:
            print("Error: not 3 files specified. Aborting merge.")
            return
        #process and fails
        try:
            trainf = open(args[0])
            devf = open(args[1])
            testf = open(args[2])
        except Exception as e:
            print("Error: failed to open file (%s). Aborting merge."%(str(e)))
            return
        #check compatibilities (on raw only)
        for fA,fB in zip(['train','dev','test'],[trainf,devf,testf]):
            tstf = open(get_treebank_path(self.sc,'ptb',fA,'raw'))
            res = check_merge(tstf,fB)
            tstf.close()
            fB.close()
            if not res:
                print("Incompatibility detected for %s files. Aborting merge")%(fA,)
                return
        #check that merged files have consistent headers
        colnames = []
        for mf in args:
            f = open(mf)
            ncolnames = f.readline().split()
            f.close()
            if not colnames:
                colnames = ncolnames
            else:
                if not all([nA==nB for nA,nB in zip(colnames,ncolnames)]):
                    print("Error inconsistent headers in merged files. aborting.")
                    return
            
        #Run the merge for tbk and raw files
        print("Merging files...")
        ncolnames = [] 
        for pA,pB in zip(['train','dev','test'],args):
            for suffix in ['tbk','raw']:
                fA =  open(get_treebank_path(self.sc,'ptb',pA,suffix))
                fB =  open(pB)
                fM =  open(str(pB)+'.tmp','w')
                ncolnames = cmerge(fA,fB,fM)
                fA.close()
                fB.close()
                fM.close()
                os.rename(str(pB)+'.tmp',get_treebank_path(self.sc,'ptb',pA,suffix))
        print("Merge successful.")
        self.colnames = ncolnames
                  
    def do_GET(self,cmdstring):
        """GET colnames FROM [train | dev | test | all] 
        generates a dataset with colnames from selected corpora 
        """
        args = cmdstring.split()
        if "FROM" not in args[1:-1]:
            print('**  invalid syntax: probably missing FROM keyword' )
            return
        idx = args.index('FROM')
        cols  = args[:idx]
        file_codes = args[idx+1:]
        if not self.colnames:
            self.colnames = self.get_colnames()
        for col in cols:
            if col not in self.colnames:
                print("Error:: unkown colname")
                return
        for f in file_codes:
            if f not in ['train','dev','test','all']:
                print("Error:: invalid corpus (%s) corpus must be in {train,dev,test}"%(f,))
                return
        #exec
        print('extracting from %s...'%(self.sc,))
        if 'all' in file_codes:
            for f in ['train','dev','test']:
                pth = get_treebank_path(self.sc,'ptb',f,'tbk')
                istream = open(str(pth))
                ostream = open('_'.join(cols)+'.'+f+'.raw','w')
                csplit(istream,ostream,cols)
                istream.close()
                ostream.close()
        else:
            for f in file_codes:
                pth = get_treebank_path(self.sc,'ptb',f,'tbk')
                istream = open(str(pth))
                ostream = open('_'.join(cols)+'.'+f+'.raw','w')
                csplit(istream,ostream,cols)
                istream.close()
                ostream.close()
        print('result saved as '+'_'.join(cols)+'....raw')

if __name__ == '__main__':

    p = ParserCommander()
    if len(sys.argv) > 1: #reads in a script
        rinput = open(sys.argv[1])
        p = ParserCommander()
        p.stdin = rinput
        p.use_rawinput = False
        p.prompt = ''
        p.intro  = ''
        p.cmdloop()
        rinput.close()
    else:
        p = ParserCommander()
        p.use_rawinput = True
        p.cmdloop()



