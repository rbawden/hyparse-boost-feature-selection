#!/usr/bin/python3
# -*- coding: utf-8 -*-
import os
import sys
import subprocess
import tempfile
import fastptbparser as parser
from LabelledTree import *
import argparse

"""
This is the main script for generating data for testing the parser
The idea is that it creates an output experiment directory for a given language by copying the SPMRL data set for this language.

WARNING :: this script must be run with python 3 for proper encoding management.

"""

class SpmrlPaths:
    """
    Does SPMRL path management for SPMRL-style data structures
    """
    def __init__(self, source_language_dir, target_language_dir):
        """
        @param source: the source SPMRL dataset top level dir (above all languages)
        @param target: the target SPMRL dataset top level dir where data is to be generated
        """
        self.srcbase = source_language_dir
        self.tgetbase = target_language_dir
        self.lang = source_language_dir.split('/')[-1].lower().title().split('_')[0]

    def get_source_dir_path(self, scenario, annotation, subset):
        """
        Returns the full dir path for a language, a given scenario, an annotation style and a subset.
        @param scenario: 'gold' or 'pred'
        @param annotation: 'conll' or 'ptb'
        @param subset: 'train' 'dev' or 'test'
        """
        assert(subset in ['train', 'dev', 'test'])
        assert(annotation in ['ptb', 'conll'])
        assert(scenario in ['gold', 'pred'])
        
        p = os.path.join(self.srcbase, scenario, annotation, subset)
        return p

    def get_source_treebank_path(self, scenario, annotation, subset, heads=True):
        """
        @param scenario: 'gold' or 'pred'
        @param annotation: 'conll' or 'ptb'
        @param subset: 'train' 'dev' or 'test'
        """
        assert(subset in ['train', 'dev', 'test'])
        assert(annotation in ['ptb', 'conll'])
        assert(scenario in ['gold', 'pred'])

        tbname = ''
        if annotation == 'ptb' and heads:
            tbname = '.'.join([subset, self.lang, scenario, annotation, 'heads'])
        else:
            tbname = '.'.join([subset, self.lang, scenario, annotation])
            
        p = os.path.join(self.srcbase, scenario, annotation, subset, tbname)
        return p

    def get_source_dict_path(self):
        """
        Returns the path to the morphological dictionary associated with this language
        """
        p = os.path.join(self.srcbase, 'mdict', 'morphdict.txt')
        if os.path.isfile(p):
            return p
        else:
            return None
    
    
    def get_target_dir_path(self, scenario, annotation, subset):
        """
        Returns the full target dir path for a language, a given scenario, an annotation style and a subset.
        if the path does not exist it creates it
        @param scenario: 'gold' or 'pred'
        @param annotation : 'conll' or 'ptb'
        @param subset : 'train' 'dev' or 'test'
        """
        assert(subset in ['train', 'dev', 'test'])
        assert(annotation in ['ptb', 'conll'])
        assert(scenario in ['gold', 'pred'])
        p = os.path.join(self.tgetbase, scenario, annotation, subset)
        try:
            os.makedirs( p, 0O777 )
        except:
            sys.stderr.write('(warning) could not generate dir :%s (already exists ?)\n'%(str(p)))
        return p

        
    def get_target_treebank_path(self, scenario, annotation, subset, format):
        """
        @param scenario: 'gold' or 'pred'
        @param annotation : 'conll' or 'ptb'
        @param subset : 'train' 'dev' or 'test'
        @param format: 'raw' 'mrg' 'conll' 'tbk' 'mwe'
        """
        assert(subset in ['train', 'dev', 'test'])
        assert(annotation in ['ptb', 'conll'])
        assert(scenario in ['gold', 'pred', 'silver'])
        assert(format in ['raw', 'mrg', 'conll', 'tbk', 'mwe'])

        p = os.path.join(self.tgetbase, scenario, annotation, subset, 'treebank.'+format)
        try:
            os.makedirs(os.path.join(self.tgetbase, scenario, annotation, subset), 0O777 )
        except:
            #sys.stderr.write('(warning) could not generate file :%s  (already exists ?) \n'%(str(p)))
            pass
        return p

###############################################################    
## headed PTB trees preprocessing (loading)                  ##

def spmrl_node_preprocessor(node):
    """
    Removes functions and sets heads
    """
    fields = node.label.split('-')
    node.label = fields[0]
    if 'head' in fields[1:]:
        node.is_head = True
    else:
        node.is_head = False

def safe_split(att_val):
    """
    This avoids various bugs found in the data set
    """
    if att_val[-1] == '=':
        return (att_val[:-2], '=')
    fsplit = att_val.split('=')
    if len(fsplit) == 2:
        return fsplit
    else:
        return ('_', '_')
 
def sprml_tag_preprocessor(node, normaliser):
    """
    Removes functions and sets heads and features
    """
    (cat, feats, _) = node.label.split('##')
    if isinstance(normaliser, EnglishNormaliser):
        if cat[0] == '-' and cat[-1] == '-':
            cat = cat[1:-1]          
    node.label = cat.split('-')[0]
    node.features = dict([ safe_split(f) for f in feats.split('|') if len(f) > 1 ]) # trashes invalid features
    #if isinstance(normaliser, KoreanNormaliser):
    #    node.label = node.features['cpos'] detrimental
    if isinstance(normaliser, EnglishNormaliser):
        node.label = node.label.replace(':', '^')
    if isinstance(normaliser, ChineseNormaliser):
        node.label = node.label.replace('#', '^')
        node.children[0].label = node.children[0].label.replace('#', '^')
    if isinstance(normaliser, FrenchNormaliser):
        if node.label == 'PONCT':
            node.label = 'PONCTW'
            if node.children[0].label in ['.', '!', '?'] :
                node.label = 'PONCT'
                    
    node.is_head = node.features['head'] == 'true' if 'head' in node.features else False
    node.features = normaliser.normalise_avm(node.features)
    
def spmrl_mwe_preprocessor(root, normaliser, mwe_val='O'):
    """
    Sets the mwe feature (IOB tagging) in the tree tags
    Should be called right after spmrl tree preprocessor (assumes node.features is set)
    """
    if root.is_tag():
        root.features['mwe'] = mwe_val
        if mwe_val.startswith('B_'):
            mwe_val = 'I_'+mwe_val[2:]
        return mwe_val
    else:
        if root.label.endswith('+'):
            mwe_val = 'B_' + root.label
        else:
            mwe_val == 'O'
        for child in root.children:
            mwe_val = spmrl_mwe_preprocessor(child, normaliser, mwe_val)
        if root.label.endswith('+'):
            return 'O'
        else:
            return mwe_val

def spmrl_tree_preprocessor(root, normaliser):
    if not root.is_leaf():
        for child in root.children:
            spmrl_tree_preprocessor(child, normaliser)
        if root.is_tag():
            sprml_tag_preprocessor(root, normaliser)
        else :
            spmrl_node_preprocessor(root)
            
def spmrl_treebank_loader(istream, tree_list, normaliser):
    """
    Reads a treebank file and appends it to the tree_list.
    The function calls an additional clean-up function that manages additional SPMRL annotations (features)
    """
    bfr = istream.readline()
    local_list = []
    while bfr != '':
        tree = parser.parse_line(bfr)
        spmrl_tree_preprocessor(tree, normaliser)
        if isinstance(normaliser, FrenchNormaliser):
            spmrl_mwe_preprocessor(tree, normaliser)
        local_list.append(tree)
        bfr = istream.readline()
    tree_list.extend(local_list)

    
def load_sprml_treebank(trainfile, devfile, testfile, normaliser, do_testset=False):
    tf = open(trainfile, mode='rt', encoding='utf-8')
    td = open(devfile, mode='rt', encoding  ='utf-8')
    tt = open(testfile, mode='rt', encoding ='utf-8')
    T  = []
    D  = []
    Tst= []
    spmrl_treebank_loader(tf, T, normaliser)
    spmrl_treebank_loader(td, D, normaliser)
    if do_testset: spmrl_treebank_loader(tt, Tst, normaliser)
    tf.close()
    td.close()
    tt.close()
    return (T, D, Tst)
            
class MarmotWrapper:
    """
    Wraps marmot into python, allowing it to be manipulated as a python object
    """
    def __init__(self, marmotjar=None):
        self.marmotjar=marmotjar
        pass
    
    def train(self, train_file, dict_file=None, pos=True):
        try:
            train_file.flush()
            sys.stderr.write("Training CRF tagger...")
            if pos:
                subprocess.check_call(
                    ["java","-Xmx8G","-cp",self.marmotjar,"marmot.morph.cmd.Trainer","-train-file", 
                     "form-index=1,tag-index=2,%s"%(str(train_file.name)), 
                     "-tag-morph","false", "-quadratic-penalty","0.1","-rare-word-max-freq","5", 
                    "-model-file","xxx.marmot"])
            else:
                if dict_file == None:
                    subprocess.check_call(
                        ["java","-Xmx8G","-cp",self.marmotjar,"marmot.morph.cmd.Trainer","-train-file", 
                        "form-index=1,tag-index=2,morph-index=3,%s"%(str(train_file.name)), 
                        "-tag-morph","true","-quadratic-penalty","0.1","-rare-word-max-freq","5", 
                        "-model-file","xxx.marmot"])
                else:
                    print('with dict')
                    subprocess.check_call(
                        ["java","-Xmx8G","-cp",self.marmotjar,"marmot.morph.cmd.Trainer", 
                         "-train-file", "form-index=1,tag-index=2,morph-index=3,%s"%(str(train_file.name)), 
                        "-tag-morph","true","-quadratic-penalty","0.1","-rare-word-max-freq","5", 
                        "-model-file","xxx.marmot", 
                        "-type-dict", str(dict_file), 
                       ])                      
        except subprocess.CalledProcessError as e:
            print(e)
            exit(1)
        
    def pred(self, in_file):
        """
        @return tag/word yields ready to be plugged into the trees
        """
        in_file.flush()
        with tempfile.NamedTemporaryFile(mode='r', encoding='utf-8') as out_file:
            try:
                sys.stderr.write("CRF tagger predictions...\n")
                # sys.stderr.write(out_file.name+"\n")
                subprocess.check_call(["java" , "-cp",self.marmotjar,"marmot.morph.cmd.Annotator", 
                                        "--model-file","xxx.marmot", 
                                        "--test-file","form-index=0,%s"%(str(in_file.name), ) , 
                                        "--pred-file",str(out_file.name)])
            except subprocess.CalledProcessError as e:
                print(e)
                exit(1)
            
            # Fetching results
            all_preds = []
            bfr       = []
            for line in out_file:
                if line.isspace():
                    if len(bfr) > 0:
                        all_preds.append(bfr)
                        bfr = []
                else:
                    fields = line.split()
                    word = LabelledTree(fields[1])
                    tag  = LabelledTree(fields[5])
                    tag.add_child(word)
                    features = fields[7].split('|')
                    tag.features = dict([attval.split('=') for attval in features if attval != '_'])
                    bfr.append(tag)
                
        # out_file.close()
        return all_preds
            
    def clean(self):
        subprocess.check_call(['rm', '-f', 'xxx.marmot'])

class LanguageNormaliser:
    """
    A class that normalises attributes and values in the SPMRL datasets
    """
    def __init__(self):
        self.norm_attributes = {} # dict of unnormalised -> normalised features
        # any unnormalised attribute absent from this dict is ignored by the conversion
        self.norm_values = {} # maps keys to their norm values. if key is absent, value is left unchanged

    def get_norm_attributes(self):
        return list(set(self.norm_attributes.values()))
        
    def normalise_avm(self, avm):
        """
        This method has the purpose of transforming an avm
        given as arg into a normalised one
        @return the normalised avm
        """
        navm = {}
        for elt in avm:
            if elt in self.norm_attributes:
                navm[self.norm_attributes[elt]] = avm[elt]
        return navm
            
    def normalise_value(self, val):
        if val in self.norm_values:
            return self.norm_values[val]
        else:
            return val

# Lang specifics
class ArabicNormaliser(LanguageNormaliser):
    """
    Normalises arabic features
    """
    def __init__(self):
        LanguageNormaliser.__init__(self)
        self.norm_attributes = {'gender':'gen', 'number':'num', 'case':'case', 'aspect':'aspect', 'mood':'mood', 'cat':'cat', 'subcat':'subcat'}
        self.norm_values = {'sj':'s', 'm':'masc', 'f':'fem'}



class BasqueNormaliser(LanguageNormaliser):
    """
    Normalises basque features
    """
    def __init__(self):
        LanguageNormaliser.__init__(self)
        self.norm_attributes = {'NUM':'num', 'KAS':'case', 'ASP':'aspect', 'MDN':'mood', 'ERL':'erl', 'DADUDIO':'dadudio', 'NORK':'nork', 'NORI':'nori', 'NOR':'nor'}
        self.norm_values = {'sj':'s', 'm':'masc', 'f':'fem'}
        
    def normalise_avm(self, avm):
        avm = LanguageNormaliser.normalise_avm(self, avm)
        if 'KAS' in avm:
            avm['KAS'] = avm['KAS'].split('_')[0]
        return avm
    
class FrenchNormaliser(LanguageNormaliser):
    def __init__(self):
        LanguageNormaliser.__init__(self)
        self.norm_attributes = {'g':'gen', 'n':'num', 'm':'mood', 'mwe':'mwe'}

class GermanNormaliser(LanguageNormaliser):
    def __init__(self):
        LanguageNormaliser.__init__(self)
        self.norm_attributes = {'case':'case', 'mood':'mood', 'gender':'gender', 'number':'number'}

class HebrewNormaliser(LanguageNormaliser):
    def __init__(self):
        LanguageNormaliser.__init__(self)
        self.norm_attributes = {'gen':'gen', 'num':'num'}
        
class HungarianNormaliser(LanguageNormaliser):
    def __init__(self):
        LanguageNormaliser.__init__(self)
        self.norm_attributes = {'Num':'num', 'SubPOS':'SubPOS', 'Cas':'case', 'Mood':'mood'}

class KoreanNormaliser(LanguageNormaliser):
    def __init__(self):
        LanguageNormaliser.__init__(self)
        self.norm_attributes = {'case-type':'case', 'case-type1':'case', 'verb-type1':'vtype', 'verb-type':'vtype'}

class SwedishNormaliser(LanguageNormaliser):
    def __init__(self):
        LanguageNormaliser.__init__(self)
        self.norm_attributes = {'verbform':'mood', 'gender':'gen', 'number':'num', 'case':'case', 'perfectform':'perfectform'}

class PolishNormaliser(LanguageNormaliser):
    def __init__(self):
        LanguageNormaliser.__init__(self)
        self.norm_attributes = {'case':'case', 'gender':'gen', 'number':'num', 'aspect':'aspect', 'post-prepositionality':'post-prepositionality'}

class EnglishNormaliser(LanguageNormaliser):
    def __init__(self):
        LanguageNormaliser.__init__(self)
class ChineseNormaliser(LanguageNormaliser):
    def __init__(self):
        LanguageNormaliser.__init__(self)
        
class ConllWord:

    def __init__(self, wordform, tag, idx):
        self.idx = idx
        self.wordform = wordform
        self.tag = tag
        self.head =  0
        
    def __str__(self):
        return '\t'.join([str(self.idx), self.wordform, "-", self.tag, self.tag, "_", str(self.head), '_', '_', '_'])

class ConllGraph:
    
    def __init__(self):
        """
        On-the-fly conll graph for generating conll silver data sets
        """
        self.deps2head = {} # maps a dependant to its governor
        self.words = {}

    def add_dependency(self, gov, dep):
        self.deps2head[dep] = [gov]

    def add_word(self, word):
        self.words[word.idx] = word

    def __str__(self):
        return '\n'.join([str(self.words[wd]) for wd in sorted(list(self.words.keys()), key = lambda w: int(w))])

    @staticmethod
    def fromTree(tree, g):
        """
        Generates a conll graph from a head annotated tree
        """
        if tree.is_tag():
            return tree.clidx
        else:
            for child in tree.children:            
                hidx = ConllGraph.fromTree(child, g)
                if child.is_head:
                    tree.clidx = hidx
            for child in tree.children:
                if not child.is_head:
                    g.add_dependency(tree.clidx, child.clidx)
                    g.words[child.clidx].head = tree.clidx
            return tree.clidx
        
class TreebankTypeDefinition:
    """
    This class provides the means to select which features are to be generated under which names in the native and marmot-conll formats 
    """
    def __init__(self, normaliser):
        self.normaliser = normaliser
        self.src_list = normaliser.get_norm_attributes()
        self.mapping = self.normaliser.norm_attributes
        assert('word' not in self.src_list)
        assert('tag' not in self.src_list)

    def generate_header(self):
        return '\t'.join(['word', 'tag']+self.src_list)

    def generate_fvalue(self, key, avm):
        if key in avm:
            return avm[key]
        else:
            return 'Na'
        
    def generate_native_token(self, tok_node, mark_head=False, indent=0):
        if tok_node.is_head and mark_head:
            #                               word                        , tag                      
            return ' '*indent + '\t'.join([tok_node.first_child().label, tok_node.label+'-head']
                                          +[self.generate_fvalue(var, tok_node.features) for var in self.src_list])
        else:
            #                               word                        , tag                     
            return ' '*indent + '\t'.join([tok_node.first_child().label, tok_node.label]
                                          +[self.generate_fvalue(var, tok_node.features) for var in self.src_list])

    def generate_native_symbol(self, begin, label, indent, head):
            if begin:
                if head:
                    return ' '*indent + '<'+label+'-head>'
                else: 
                    return ' '*indent + '<'+label+'>'
            else:
                return ' '*indent + '</'+label+'>'
            
    def generate_native_tbk(self, root, indent=0):
        """
        @return the tree as a string in native format
        """
        if not root.is_tag():
            return '\n'.join([self.generate_native_symbol(True, root.label, indent, root.is_head)]
                           + [self.generate_native_tbk(child, indent = indent+3) for child in root.children]
                           + [self.generate_native_symbol(False, root.label, indent, root.is_head)])
        else:
            return self.generate_native_token(root, True, indent)
        
    def generate_native_raw(self, root, tags=False):
        """
        @return the tree yield as a string in native format
        """
        if tags:
            y = root.tag_yield()
            return '\n'.join([self.generate_native_token(tok[0], False, 0) for tok in y])
        else:
            return '\n'.join(root.do_flat_string().split())

    def make_conll_features(self, node):
       
        lst =  '|'.join([ '='.join([av[0], av[1]]) for av in node.features.items() if av[0] in self.src_list])
        if len(lst) == 0:
            lst = '_'
        return lst
    
    def generate_conll_marmot(self, root):
        """
        @return the tree as a marmot training file (conll style)
        """
        y = root.tag_yield()
        s = '\n'.join(['\t'.join([str(idx+1), tok[1].label, tok[0].label, self.make_conll_features(tok[0])]) for idx, tok in enumerate(y)])
        return s
    
def do_native_gold_file(ostream, ttd, tree_list, format, tags=True):
    """
    Generates a single gold file from tree list.
    @param format: raw or tbk
    @param tags: says if tags are output in raw mode
    """
    assert(format in ['raw', 'tbk'])
    print(ttd.generate_header(), file=ostream)
    if format == 'tbk':
        for tree in tree_list:
           print(ttd.generate_native_tbk(tree)+'\n', file=ostream)
    if format == 'raw':
        for tree in tree_list:
            print(ttd.generate_native_raw(tree, tags)+'\n', file=ostream)
            #ostream.write(ttd.generate_native_raw(tree, tags)+'\n\n')

def do_conll_silver_file(tree_list, ostream):
    """
    Generates a conll silver file from head annotated trees
    """
    for tree in tree_list:
        ty = tree.tag_yield()
        g = ConllGraph() 
        for idx, elt in enumerate(ty):
            elt[0].clidx = idx+1
            w = ConllWord(elt[1].label, elt[0].label, elt[0].clidx)
            g.add_word(w)
        ConllGraph.fromTree(tree, g)
        print(str(g)+'\n', file=ostream)

        
def do_native_mrg_file(ostream, tree_list):
    """
    Generates a single ptb mrg file from tree list.
    """
    for tree in tree_list:
        ostream.write(tree.do_flat_string()+'\n')

def do_marmot_trainfile(ostream, ttd, tree_list):
    """
    Generates a single marmot train file from tree list.
    """
    for tree in tree_list:
        ostream.write(ttd.generate_conll_marmot(tree)+'\n\n')
              
def do_marmot_raw(ostream, tree_list):
    for tree in tree_list:
        words = tree.tree_yield()
        ostream.write(('\n'.join([w.label for w in words])+"\n\n"))
                
def do_merge_predicted_tags(ref_treebank, predicted_tags):
    print(len(ref_treebank))
    print(len(predicted_tags))
    assert(len(ref_treebank) == len(predicted_tags))

    for (tree, tag_sequence) in zip(ref_treebank, predicted_tags):
        ty = list([tag for (tag, word) in tree.tag_yield()])
        assert(len(ty)==len(tag_sequence))
        for (ref_tag, pred_tag) in zip(ty, tag_sequence):
                ref_tag.label = pred_tag.label
                ref_tag.features = pred_tag.features
                ref_tag.children = pred_tag.children


def do_language_silver_dataset(spaths, ttd, train_trees, dev_trees, test_trees, do_testset=False):
    """
    Generates CoNLL trees as converted from the head-annotated CFG trees
    """
    L = [train_trees, dev_trees, test_trees]
    if do_testset: sets=['train', 'dev', 'test']
    else: sets=['train', 'dev']
    for idx, sc in enumerate(sets):
        sys.stderr.write('Processing %s file..\n'%(sc))
        conllf = spaths.get_target_treebank_path('silver', 'conll', sc, 'conll')
        cstream = open(conllf, 'wt', encoding='utf-8')
        do_conll_silver_file(L[idx], cstream)
        cstream.close()       
                                        
def do_language_gold_dataset(spaths, ttd, train_trees, dev_trees, test_trees, do_testset=False):
    """
    Generates the gold files for a given language
    """
    L = [train_trees, dev_trees, test_trees]
    if do_testset: sets=['train', 'dev', 'test']
    else: sets=['train', 'dev']
    for idx, sc in enumerate(sets):
        sys.stderr.write('Processing %s file..\n'%(sc))
        ptbk = spaths.get_target_treebank_path('gold', 'ptb', sc, 'tbk')
        praw = spaths.get_target_treebank_path('gold', 'ptb', sc, 'raw')
        pmrg = spaths.get_target_treebank_path('gold', 'ptb', sc, 'mrg')
        ftbk = open(ptbk, 'wt', encoding='utf-8')
        fraw = open(praw, 'wt', encoding='utf-8')
        fmrg = open(pmrg, 'wt', encoding='utf-8')
        do_native_gold_file(ftbk, ttd, L[idx], 'tbk')
        do_native_gold_file(fraw, ttd, L[idx], 'raw')
        do_native_mrg_file(fmrg, L[idx])
        ftbk.close()        
        fraw.close()
        fmrg.close()


def do_marmot_jackknife(spmrl_paths, ttd, marmot, train_trees, dev_trees, test_trees, K=10, do_testset=False):
    """
    Performs a K-way jackknifing with Marmot: yields a pred mode
    """
    dataset = train_trees + dev_trees
    newdataset = []
    N = len(dataset)
    fsize = int(N / float(K))
    for k in range(K):
        sys.stderr.write("Fold %d..."%(k + 1))
        pred_fold = list(range(k * fsize, (k + 1) * fsize))
        train_fold = list(range(0, k * fsize)) + list(range((k + 1) * fsize, N))
        if k == K-1:# corrects rounding error for last fold 
            pred_fold = list(range(k*fsize, N))
            train_fold = list(range(0, k*fsize))
            
        # make data sets
        tf = tempfile.NamedTemporaryFile(mode='w', encoding='utf-8')
        tp = tempfile.NamedTemporaryFile(mode='w', encoding='utf-8')
        
        do_marmot_trainfile(tf, ttd, list([dataset[index] for index in train_fold]))
        # os.sys.stderr.write("tp = "+tp.name+"\n")
        if do_testset:do_marmot_raw(tp, list([dataset[index] for index in pred_fold]))
        
        # make inference
        # os.sys.stderr.write("tf = "+tf.name+"\n")
        marmot.train(tf, spmrl_paths.get_source_dict_path(),len(ttd.src_list) == 0)
        # os.sys.stderr.write("tp = "+tp.name+"\n")
        if do_testset: newdataset.extend(marmot.pred(tp))
        
        # retrieve results
        tf.close()
        tp.close()
        
    # final prediction on test
    tf = tempfile.NamedTemporaryFile(mode='w', encoding='utf-8')
    tp = tempfile.NamedTemporaryFile(mode='w', encoding='utf-8')
    do_marmot_trainfile(tf, ttd, dataset)
    if do_testset: do_marmot_raw(tp, test_trees)
    
    # make inference
    marmot.train(tf, spmrl_paths.get_source_dict_path(), len(ttd.src_list) == 0)
    if do_testset: newtest = marmot.pred(tp)
    
    # merge results
    new_train = newdataset[:len(train_trees)]
    new_dev = newdataset[len(train_trees):]
    do_merge_predicted_tags(train_trees, new_train)
    do_merge_predicted_tags(dev_trees, new_dev)
    if do_testset: do_merge_predicted_tags(test_trees, newtest)
    tf.close()
    tp.close()

    
def do_language_pred_dataset(spaths, ttd, train_trees, dev_trees, test_trees, marmotjar, K=10, do_testset=False):
    """
    Generates the pred files for a given language
    ** Warning it destructively modifies the treebanks passed as params **
    @param K: the jackknife number of iterations
    """
    sys.stderr.write("Performing a %d-fold jackknife for tagging (this may take some time)...\n"%K)
    marmot = MarmotWrapper(marmotjar)
    do_marmot_jackknife(spaths, ttd, marmot, train_trees, dev_trees, test_trees, K, do_testset)
    L = [train_trees, dev_trees, test_trees]
    if do_testset: sets=['train', 'dev', 'test']
    else: sets=['train', 'dev']
    for idx, sc in enumerate(sets):
        sys.stderr.write('Processing %s file..\n'%(sc))
        ptbk = spaths.get_target_treebank_path('pred', 'ptb', sc, 'tbk')
        praw = spaths.get_target_treebank_path('pred', 'ptb', sc, 'raw')
        pmrg = spaths.get_target_treebank_path('pred', 'ptb', sc, 'mrg')
        ftbk = open(ptbk, 'wt', encoding='utf-8')
        fraw = open(praw, 'wt', encoding='utf-8')
        fmrg = open(pmrg, 'wt', encoding="utf-8")
        do_native_gold_file(ftbk, ttd, L[idx], 'tbk')
        do_native_gold_file(fraw, ttd, L[idx], 'raw')
        do_native_mrg_file(fmrg, L[idx])
        ftbk.close()        
        fraw.close()
        fmrg.close()



#####################
#Language specifics

def generate_language(spmrl_paths, ttd, marmotjar, jack_K = 10, do_testset=False):
    # copy scripts

    (train_trees, dev_trees, test_trees) = load_sprml_treebank(spmrl_paths.get_source_treebank_path('gold', 'ptb', 'train', heads=True), 
                                                                spmrl_paths.get_source_treebank_path('gold', 'ptb', 'dev', heads=True), 
                                                                spmrl_paths.get_source_treebank_path('gold', 'ptb', 'test', heads=True), 
                                                                ttd.normaliser, do_testset)

     
    #train_trees = list([x for x in train_trees if len(x.tree_yield()) < 20])
    #dev_trees = list([x for x in dev_trees if len(x.tree_yield()) < 20])
    #test_trees = list([x for x in test_trees if len(x.tree_yield()) < 20])
    
    do_language_gold_dataset(spmrl_paths, ttd, train_trees, dev_trees, test_trees, do_testset)
    do_language_silver_dataset(spmrl_paths, ttd, train_trees, dev_trees, test_trees, do_testset)
    do_language_pred_dataset(spmrl_paths, ttd, train_trees, dev_trees, test_trees, marmotjar, jack_K, do_testset)


if __name__=="__main__":

    argparser = argparse.ArgumentParser()
    argparser.add_argument('-i', help='input data folder', required=True)
    argparser.add_argument('-o', help='output data folder', required=True)
    argparser.add_argument('-lang', choices=['fr', 'sv', 'eu', 'en', 'pl', 'he', 'hu', 'de', 'ko', 'zh', 'ar'], required=True) # other languages to be added later
    argparser.add_argument('-marmot', help='marmot jar', required=True)
    argparser.add_argument('-test', help='also process the test set', action="store_true")
    args = argparser.parse_args()
    if args.i[-1]=="/": args.i = args.i[:-1]
    if args.o[-1]=="/": args.o = args.o[:-1]
    
    paths = SpmrlPaths(args.i, args.o)
    
    if args.lang=="fr":
        print('Processing FRENCH')
        ttd = TreebankTypeDefinition(FrenchNormaliser())
        
    elif args.lang=="sv":    
        print('Processing SWEDISH')
        ttd = TreebankTypeDefinition(SwedishNormaliser())

    elif args.lang=="eu":
        print('Processing BASQUE')
        ttd = TreebankTypeDefinition(BasqueNormaliser())

    elif args.lang=="en":
        print('Processing ENGLISH')
        ttd = TreebankTypeDefinition(EnglishNormaliser())

    elif args.lang=="pl":
        print('Processing POLISH')
        ttd = TreebankTypeDefinition(PolishNormaliser())

    elif args.lang=="he":
        print('Processing HEBREW')
        ttd = TreebankTypeDefinition(HebrewNormaliser())

    elif args.lang=="hu":
        print('Processing HUNGARIAN')
        ttd = TreebankTypeDefinition(HungarianNormaliser())

    elif args.lang=="de":
        print('Processing GERMAN')
        ttd = TreebankTypeDefinition(GermanNormaliser())

    elif args.lang=="ko":
        print('Processing KOREAN')
        ttd = TreebankTypeDefinition(KoreanNormaliser())

    elif args.lang=="ar":
        print('Processing ARABIC')
        ttd = TreebankTypeDefinition(ArabicNormaliser())

    elif args.lang=="zh":    
        print('Processing CHINESE')
        ttd = TreebankTypeDefinition(ChineseNormaliser())

    generate_language(paths, ttd, args.marmot, 10, args.test)



