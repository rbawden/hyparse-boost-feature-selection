#!/usr/bin/env python -OO
# -*- coding: iso-8859-15 -*-
 
import pickle
import os
import math
import sys
import PropagTable

from parser_constants import *

# Constant methods
def parse_failure():
      """
      This returns a special tree encoding a parse_failure.

      @return: a labelled tree encoding a parse failure
      @rtype: a Labelled tree
      """
      return LabelledTree(None)



def add_dummy_root(tree,dummyroot=''):
      """
      This adds a node rooted with a dummy root passed in argument as immediate ancestor of the actual root of this tree
      This is used to add a root compliant with PennTreebank style format

      @return: The dummy root of this tree being the new pointer on this tree
      @rtype: a LabelledTree
      """
      return LabelledTree(dummyroot,children=[tree])


def remove_dummy_root(tree,dummyroot=''):
      """
      This removes a node rooted with a dummy root as immediate ancestor of the actual root of this tree
      This is used to remove the dummy root encoded in PennTreebank style format
      
      @return: a pointer on the child of the dummy root or the root itself if it was not a dummy root
      @rtype: a LabelledTree
      """
      if len(tree.children) == 1 and tree.label == dummyroot:
          return tree.first_child()
      else:
          return tree

def tree_diff(treea,treeb,ignore_root=False,ignore_leaves=False,ignore_tags=False):
      """
      This performs a basic diff on two trees.
      The function returns 3 sets of Parseval-style triples of the form(Label,leftindex,rightindex)
      * The first set is the set of shared triples
      * The second set is the set of triples where a differs from b
      * The third set is the set of triples where b differs from a
      
      @param treea: a tree to be compared
      @type treea: LabelledTree
      @param treeb: a tree to be compared
      @type tree: LabelledTree
      @param ignore_root:flag. set to true ignore roots for comparisons
      @type ignore_root: boolean
      @param ignore_leaves: set to true, ignore leaves for comparisons
      @type ignore_leaves: boolean
      @return: a tuple of sets of triples (shared, a diffs, b diffs)
      @rtype: a tuple of sets of triples
      """

      triplesa = set(treea.triples(ignore_root=ignore_root,ignore_leaves=ignore_leaves,ignore_tags=ignore_tags))
      triplesb = set(treeb.triples(ignore_root=ignore_root,ignore_leaves=ignore_leaves,ignore_tags=ignore_tags))
      ta_inter_tb = triplesa.intersection(triplesb)
      ta_minus_tb = triplesa.difference(triplesb)
      tb_minus_ta = triplesb.difference(triplesa)
      return (ta_inter_tb,ta_minus_tb,tb_minus_ta)

class TreebankTree:
      """
      This class represents a tree as used in a treebank.
      This encapsulates a LabelledTree and associates it with meta information such as 
      its index in the treebank, the file it is encoded in (and possibly its annotator or the date it has been annotated)
      """
      def __init__(self,tree,index=-1,srcfile=None):
            """
            This creates a Treebank Tree with specified index and source file

            @param tree: the tree annotated
            @type tree: LabelledTree
            @param index: the index of the tree in the treebank
            @type index: integer
            @param srcfile: the name of the file where the tree is encoded
            @type srcfile: string
            """
            self.tree = tree
            self.index= index
            self.srcfile = srcfile

      def do_xml_string(self):
            """
            This returns a Treebank Tree encoded in French Treebank XML
            
            @return: a multiline string encoding this tree in XML
            @rtype: string
            """
            return self.tree.do_xml_string(index=self.index,root=True)

      def __str__(self):
            """
            This returns a Treebank Tree encoded in French Treebank XML
            
            @return: a multiline string encoding this tree in XML
            @rtype: string
            """
            return self.do_xml_string()

      def to_db_string(self, colsep='\t',index=True,filename=False,display_functions=False,display_features=False):
            """
            This does a string representation of this tree in database format.
            that is one tree per line with tab separated fields (by default)

            @param colsep: the column separating char
            @type colsep: char
            @param index: boolean indicating whether the sentence index has to be output
            @type index: boolean
            @param filename: boolean indicating whether the orginal filename of the tree has to be output
            @type filename: boolean
            """
            columns=[self.tree.do_flat_string(sep=',',display_functions=display_functions,display_features=display_features,untyped=True)]
            if filename:
                  columns = [self.srcfile] + columns
            if index:
                  columns = [str(self.index)] + columns
            return colsep.join(columns)

class TagFixer:
      """
      A TagFixer is a class mapping a conflated symbol like VINF to a base category V and (optionally) a feature structure
      e.g. VINF-> basecat=V, mood=inf, NC --> basecat=N, subcat=common
      """
      def __init__(self):
            """
            Creates an empty tag fixer with a type definition specified by attlist
            """
            self.mappings = {}
            
      def build_inverse_mapping(self):
            """ Builds the inverse mapping: from basecat to complexcat + features
            in order to facilitate the computation of complexcat from basecat + features
            The method sets the dictionary inverse_mappings
            key = basecat, 
            value = list of pairs: (complexcat, dictionary of features)
                    the list of pairs is sorted, with decreasing order of number of features (so that (ADJINT, {subcat:1}) appears before (ADJ, {}))
                    """
            self.inverse_mappings = {}
            for complextag in self.mappings:
                  (basecat, list_of_features) = self.map_tag(complextag)
                  dfeats = dict(list_of_features)
                  if basecat in self.inverse_mappings:
                        self.inverse_mappings[basecat].append( (complextag, dfeats) )
                        # sort the feature dictionaries with decreasing order of keys
                        s = sorted(self.inverse_mappings[basecat], 
                                   lambda x,y: cmp(len(y[1].keys()),len(x[1].keys())))
                        self.inverse_mappings[basecat] = s
                  else:
                        self.inverse_mappings[basecat] = [ (complextag, dfeats) ]


      def set_tagmap(self,orig_tag,dest_tag,list_of_features=[]):
            """
            This sets a mapping from a conflated tag to its analytical decomposition.
            e.g. 'VINF' --> 'V', ('mood','inf')
            
            @param orig_tag: the tag to analyse
            @type orig_tag: a string
            @param dest_tag: a base category
            @type dest_tag: a string
            @param list_of_features: an optional list of couples (2-tuples) of the form (attribute,value)
            @type list_of_features: a list of 2-tuples of strings
            """
            self.mappings[orig_tag] = (dest_tag,sorted(list_of_features))
		
      def map_tag(self,tag):
            """
            This returns a couple of the form (basecat,list_of_features) being the analytical decomposition of a tag.
            
            @param tag: the tag from which to derive the analytical form
            @type tag: string
            @return:a couple (basecat,list_of_features) where list of features is None if there are no features. In case there is no mapping for tag, returns the tag itself with an empty list of features
            @rtype: a couple(string,list of couples of strings)
            """
            if self.mappings.has_key(tag):
                  return self.mappings[tag]
            else:
                  return (tag,[])
            
      def reverse_tag_map(self,tag,featuredict):
            """
            This takes a base cat and a list of features and maps them to a label for use in parsing
            e.g.  'V', ('mood','inf') --> 'VINF'
            """
            if self.inverse_mappings.has_key(tag):
                  candidates = self.inverse_mappings[tag]
                  for (target,tfeatures) in candidates:
                        if self.fmatch(tfeatures,featuredict):
                              return target
            return tag

      def fmatch(self,fdict1,fdict2):
            for attr in fdict1.keys():
                  if not (fdict2.has_key(attr) and fdict2[attr] == fdict1[attr]):
                        return False
            return True

def ftb4_fixer():

    tfixer = TagFixer()
    tfixer.set_tagmap('V','V',[('mood','ind')])
    tfixer.set_tagmap('VINF','V',[('mood','inf')])
    tfixer.set_tagmap('VIMP','V',[('mood','imp')])
    tfixer.set_tagmap('VS','V',[('mood','subj')])
    tfixer.set_tagmap('VPP','V',[('mood','part'),('tense','past')])
    tfixer.set_tagmap('VPR','V',[('mood','part'),('tense','pst')])
    tfixer.set_tagmap('NC','N',[('subcat','c')])
    tfixer.set_tagmap('NPP','N',[('subcat','p')])
    tfixer.set_tagmap('CS','C',[('subcat','s')])
    tfixer.set_tagmap('CC','C',[('subcat','c')])
    tfixer.set_tagmap('CLS','CL',[('subcat','suj')])
    tfixer.set_tagmap('CLO','CL',[('subcat','obj')])
    tfixer.set_tagmap('CLR','CL',[('subcat','refl')])
    tfixer.set_tagmap('CL','CL',[('subcat','')])
    tfixer.set_tagmap('ADJ','A',[])
    tfixer.set_tagmap('ADJWH','A',[('subcat','int')])
    tfixer.set_tagmap('ADV','ADV',[])
    tfixer.set_tagmap('ADVWH','ADV',[('subcat','int')])
    tfixer.set_tagmap('PRO','PRO',[])
    tfixer.set_tagmap('PROREL','PRO',[('subcat','rel')])
    tfixer.set_tagmap('PROWH','PRO',[('subcat','int')])
    tfixer.set_tagmap('DET','D',[])
    tfixer.set_tagmap('DETWH','D',[('subcat','int')])
    tfixer.build_inverse_mapping()
    return tfixer

      
class LabelledTree:
      """
      This class implements ordered n-ary branching labelled trees as used in Constituent Parsing
      Trees may be optionally decorated with atomic features, <attribute:value> couples
      The class supposes that leaves have no direct siblings.

      Fields:
      * LABEL
      * CHILDREN
      * FEATURES
      Opts Fields (= introduced by some functions otherwise are absent from the internal dictionary)
      * IDX       (introduced by index_yield)
      * HEAD_IDX  (introduced by head_annotate)
      * HEAD_WORD (introduced by head_annotate)
      * IS_HEAD   (introduced by head_annotate)
      * BASECAT   (introduced by do_base_cat)
      * FCT       (introduced by set_funvalue)
      * COMPOUND  (introduced by XMLreader)
      * GAP       (todo ?)
      * POINTER   (todo ?)
      @author: Benoit Crabb�
      """
      def __init__(self,label,children=None,features=None):
            """
            This creates a new LabelledTree node with a label, some children and some features.
            By convention the leaf node label is a word, non leaf nodes' labels are Grammatical categories
            By convention features encode grammatical (sub-)categories, 
            metafeatures like a tree index are encoded as additional fields by relevant methods
            
            @param label: a String being the label of that node
            @type label: String
            @param children: the children of this tree ordered linearly
            @type children: list of Labelled Trees
            @param features: a dictionary of attribute value couples 
            @type features: a dictionary <String:String>
            """
            self.label = label
            self.children = children
            self.fct = None
            if features == None:
                  self.features = {}
            else:
                  self.features = features

      def is_parse_failure(self):
            """
            Indicates whether this tree is a parse failure or a parse success

            @return: True if the tree is a parse failure False otherwise
            @rtype: boolean
            """
            return self.label == None

      def isHead(self):
            """
            Returns true if this node is marked as head, false otherwise
            
            @return: a boolean indicating whether this node is a head
            @rtype:boolean
            """
            return self.__dict__.has_key('is_head') and self.is_head == True

      def is_leaf(self):
            """
            Indicates if this tree node is a leaf or a non leaf node

            @return: True if the tree is a leaf False otherwise
            @rtype: boolean
            """
            return self.children == None

      def is_tag(self):
            """
            Indicates if this tree node has the structural position of a PoS Tag 

            @return: True if this tree has only one child being a leaf, false otherwise
            @rtype:boolean
            """
            return self.children != None and len(self.children) == 1 and self.children[0].is_leaf()

      def first_child(self):
            """
            Returns the leftmost child of this tree immediately dominated by this tree

            @return: The leftmost child of this tree or this tree if this tree is a leaf
            @rtype: LabelledTree
            """
            if self.is_leaf() or self.children == []:
                  return self
            else:
                  return self.children[0]
            
      def last_child(self):
            """
            Returns the rightmost child of this tree immediately dominated by this tree

            @return: The rightmost child of this tree or this tree if this tree is a leaf
            @rtype: LabelledTree
            """
            if self.is_leaf() or self.children == []:
                  return self
            else:
                  return self.children[-1]

      def left_corner(self):
            """
            Returns the leftmost leaf of the overall tree below this node. 
            This is the reflexive, transitive leftcorner relation

            @return: The leftmost desendant of this node being a leaf of the tree or this tree if it is a leaf
            @rtype: LabelledTree
            """
            plc = None 
            clc = self
            while clc != plc:
                  plc = clc
                  clc = clc.first_child()
            return clc

      def right_corner(self):
            """
            Returns the rightmost leaf of the overall tree below this node. 
            This is the reflexive, transitive rightcorner relation

            @return: The leftmost descendant of this node being a leaf of the tree or this tree if it is a leaf
            @rtype: LabelledTree
            """
            prc = None 
            crc = self
            while crc != prc:
                  prc = crc
                  crc = crc.last_child()
            return crc

      def add_child(self,child):
            """
            This appends a child immediately dominated by this node at the rightmost position of existing children

            @param child: the child to be added
            @type child: LabelledTree
            """
            if self.is_leaf():
                  self.children = [child]
            else:
                  self.children.append(child)

      def add_children(self,childlist):
            """
            This is furcation. The children in the list are added as rightmost children
            of this node in the order specified in the list.

            @param childlist: the list of children to be added
            @type childlist: a list of LabelledTree
            """
            if self.is_leaf():
                  self.children = childlist
            else:
                  self.children.extend(childlist)
      

      def get_funvalue(self,dummyval = "None"):
            """
            This returns the function value associated to this node if there is one,
            otherwise returns the dummy value

            @param dummyval: a dummy value to return if there is no function associated to this node
            @type dummyval: string
            @return: the function of this node
            @rtype: string
            """
            if self.has_function():
                  return self.fct
            else:
                  return dummyval


      def set_funvalue(self, funval=None):
            """ 
            This sets an attribute 'fct' with value funval if the function is not already set

            @param funval: a string being a function name
            @type funval: a string
            """
            if not self.has_function():
                  self.fct = funval


      def has_function(self):
            """
            This returns true if this tree has a non null fct attribute, false otherwise

            @return: true if this tree has a non null fct attribute, false otherwise
            @rtype: a boolean
            """

            return self.__dict__.has_key('fct') and self.fct != None and self.fct != '' and not self.fct.isspace()
      

      def has_basecat(self):
            """
            This returns true if this tree has a non null basecat attribute, false otherwise

            @return: true if this tree has a non null basecat attribute, false otherwise
            @rtype: a boolean
            """

            return self.__dict__.has_key('basecat') and self.basecat != None


      def set_basecat(self, basecat=None):
            """ 
            Base cat is a category without features encoded in its symbol
            
            @param basecat: a string being an atomic base category
            @type basecat: string
            """
            self.basecat = basecat

      def tree_yield(self):
            """
            This returns the yield of the tree below this node.

            @return: the yield of this tree
            @rtype: a list of LabelledTree
            """
            if self.is_leaf():
                  return [self]
            else:
                  cyield = []
                  for child in self.children:
                        cyield.extend(child.tree_yield())
                  return cyield

      def tag_yield(self):
            """
            This returns the yield of the tree below this node.
            Here the yield is made of couples (POS tags,wordform)
            
            @return: the yield of this tree or empty list if the tree is a leaf
            @rtype: a list of couples (as 2-tuples of LabelledTree)
            """
            if self.is_leaf():
                  return []
            if self.is_tag():
                  return [(self,self.first_child())]
            else:
                  cyield = []
                  for child in self.children:
                        cyield.extend(child.tag_yield())
                  return cyield

      def extract_base_np(self):
            """
            This extracts base_np chunks from parse trees
            """
            chunklist = []
            if self.is_tag():
                  return []
            if self.label == 'NP':
                  interrupt = False
                  for idx,child in enumerate(self.children):
                        if child.label in ['PP','NP','Srel','COORD','VPpart','Sint','VPinf ','Ssub','VN']:
                              chunklist.append((self.left_corner().idx,self.children[idx-1].right_corner().idx)) 
                              interrupt = True
                              break
                  if not interrupt:
                        chunklist.append((self.left_corner().idx,self.right_corner().idx))
            for child in self.children:
                  chunklist.extend(child.extract_base_np())
            return chunklist

      def is_compound(tree):
            """
            This indicates if a Labelled tree node has been annotated as a compound
            
            @return: a boolean indicating if this node has a compound flag
            @rtype: boolean
            """
            return tree.__dict__.has_key('compound') and self.compound

      def append_yield(self,node_list):
            """
            This appends a new yield to this tree if the node list has the same size as the actual yield
            otherwise does nothing. Typically used to append the words under the PoS tags.
            
            @param node_list: the new yield to be appended
            @type node_list: a list of equal size to the size of the yield of this tree
            @return a flag indicating whether the yield is appended or not
            @rtype: boolean
            """
            cyield = self.tree_yield()
            if len(node_list) == len(cyield):
                  for i,node in enumerate(cyield):
                        node.add_child(node_list[i])
                  return True
            return False

      def has_pointer(self):
            """
            Indicates if this node hqs a pointer to another node.
            Only gap nodes have pointers directed on their fillers

            @return: true if there is a non null pointer to another node, false otherwise
            @rtype: boolean
            """
            return self.__dict__.has_key('pointer') and self.pointer != None

      def is_gap(self):
            """
            Indicates if this node is a gap

            @return: true if this node has a gap variable set to true, false otherwise
            @rtype: boolean
            """
            return self.__dict__.has_key('gap') and self.gap

      def index_leaves(self):
            """
            This explicitly indexes the leaf nodes of the trees according to their index wrt the sentence positions
            For a sequence of leaves w0...wN , the leaves are indexed from 0 to N according to the linear order
            The non leaf nodes are not indexed.
            This internally adds the field 'idx' to each leaf node
            """
            cyield = self.tree_yield()
            for i in range(len(cyield)):
                  cyield[i].idx = i

      def triples(self,ignore_root=False,ignore_leaves=False,ignore_tags=False,root=True):
            """
            This recursively turns a tree into a list of triples (3-tuples)
            Each triple is of the form (label,lidx,ridx) where lidx and ridx are computed as for typical parsing purposes. 
            These triples can then be used for performing tree diffs and related operations.

            @param ignore_root: if set to true, the function does not emit a triple for the root of this tree
            @type ignore_root: boolean
            @param ignore_leaves: if set to true the function does not emit triples for the leaves
            @type ignore_leaves: boolean
            @return: a list of triples
            @rtype: a liste of (3)tuples
            """
            if root:
                  #ensures that leaf nodes are properly indexed according to the current root/state of the tree
                  self.index_leaves()
                  
            if self.is_leaf():
                  if not ignore_leaves:
                        return [(self.label,self.idx,self.idx+1)]
                  else:
                        return []
            elif self.is_tag():
                  if not ignore_tags:
                        #buggy todo add leaf stuff here
                        return [(self.label,self.left_corner().idx,self.right_corner().idx+1)]
                  else:
                        return []
                  
            else:
                  if ignore_root:
                        tlist = []
                        ignore_root = False
                  else:
                        tlist = [(self.label,self.left_corner().idx,self.right_corner().idx+1)] #...bulky isn't it ?
                  for child in self.children:
                        tlist.extend(child.triples(ignore_root=ignore_root,ignore_tags=ignore_tags,ignore_leaves=ignore_leaves,root=False)) 
                  return tlist

      def get_form(self,dummy_val=None):
            """  
            Returns the word form of a node known to be a preterminal (father of a leaf)
            """
            if self.is_tag():
                  return self.children[0].label
            else:
                  return dummy_val

      def head_node(self):
            """
            This returns the leaf node being the head of a given node
            
            @return: the head of this constituent
            @rtype: LabelledTree
            """
            ctree = self
            while not ctree.is_tag():
                  ctree = ctree.head_child()
            return ctree.first_child()

      def head_tag(self):
            """
            This returns the node being the preterminal above a given node's head word
      
            @return: the headnode's preterminal node of this constituent or None if this node is a leaf
            @rtype: LabelledTree
            """
            if self.is_tag():
                  return self
            elif self.is_leaf():
                  return None
            else:
                tagtree = self
                lookahead = tagtree.head_child()
                while not lookahead == None:
                    tagtree = lookahead 
                    lookahead = lookahead.head_child()
                return tagtree

      def head_child(self):
            """
            Returns the immediate child of this node marked as head
            
            @return: The immediate child marked as head or None if there is none (the node is a leaf)
            @rtype: Labelled Tree
            """
            if self.is_tag():
                  return None
            else:
                  for child in self.children:
                    if child.isHead():
                        return child
 
      def annotate_all(self,tagfixer,propagation_table,clusters=None):
            """
            This performs a sequential annotation of the trees by:
            (1) indexing the leaves
            (2) Decomposes labels into basecats and features
            (3) Marking heads
            (4) Annotating functions with heuristics
            @todo: finish it off -> functional annotation
            """
            self.index_leaves()
            self.do_base_cats(tagfixer)
            self.head_annotate(propagation_table,clusts=clusters)
            self.fun_heuristic_annotate()
            
      def do_base_cats(self,tagfixer):
            """
            This populates a tree by recursively setting its basecat and features from the tree labels using a map specified by tagfixer.
            
            @param tagfixer: a tagfixer used for the analysis of labels
            @type tagfixer: TagFixer
            """
            if not self.is_leaf():
                  for child in self.children:
                        child.do_base_cats(tagfixer)
                  (self.basecat,features) = tagfixer.map_tag(self.label)
                  self.set_features(dict(features))


      def set_features(self,featsb):
            """
            This merges the feature structures dictionary featsb with the current feature dictionary to yield a single one.
            
            @warning: this is NOT unification !!! (this is a crappy merge instead:-)
            The function basically does the union of the two dictionaries.
            In case of value conflicts, the value of featsb take precedence over current feature values .
            In other words, this is a non monotonic crappy merge !
            
            @param featsb: a feature structure
            @type featsb: a dictionary of attribute/values
            @return: the merge of those 2 feature structures
            @rtype: a dictionary of attribute/values
            """
            self.features = dict(self.features.items() + featsb.items())


      def set_feature(self,attribute,value):
            """
            This attaches a feature with the given attribute and value to the node.
            In case the feature is already present with a conflicting value the current value replaces the former !

            @param attribute: the attribute of the feature
            @type attribute: String
            @param value: the value of the feature
            @type value: String 
            """
            self.features[attribute] = value


      def clitics_downwards(self):
         """
         distributes VN annotations on clitics 
         """
         if not self.is_tag():
            if self.label == "VN":
                if self.fct != None:
                    flist = self.fct.split("/")
                    self.fct=None
                    for child in self.children:
                        if child.label in ["CLS","CLO","CLR","CL"] and len(flist) > 0:
                            child.fct = flist[0]
                            flist = flist[1:]
                        child.clitics_downwards()
                else:
                    for elt in self.children:
                        elt.clitics_downwards()
            else:
                for elt in self.children:
                    elt.clitics_downwards()


      def fun_heuristic_annotate(self):
            """
            This annotates tree nodes not already annotated with relevant functional labels 
            """
            if not self.is_tag():
                  for child in self.children:
                        if not child.has_function():
                              child.set_funvalue(guess_funlabel(self,child))
                        child.fun_heuristic_annotate()


      def head_annotate(self,ptable,root=True,clusts=None):
            """
            This recursively head annotates the tree using the specified propagation table.
            This specifically adds: 
            * a boolean flag 'is_head' to every non terminal node. The root is a head as well
            * a 'head_word' field to every non terminal node being the wordform of the head
            * an 'head_idx' field to every non terminal node, being the index of the head on the input
            The function ensures that every non terminal as a child annotated as head.
            
            @precondition: this function assumes that the leaves have been properly indexed
            @param ptable: the propagation table used for marking heads
            @param ptable: PropagationTable
            """
            if self.is_tag():
                  leaf = self.first_child()
                  self.head_word = leaf.label
                  if clusts != None:
                        self.head_clust = clusts.lookup(leaf.label)
                  self.head_idx = leaf.idx
            else:
                  if self.children == None: #there remains lonely nodes sometimes
                        #self.children = []
                        raise MalformedTreeException(self)
                  for child in self.children:
                        child.is_head = False
                        child.head_annotate(ptable,root=False,clusts=clusts)
                  idx = ptable.head_index(self,self.children)
                  self.children[idx].is_head = True
                  self.head_word = self.children[idx].head_word
                  if clusts != None:
                        self.head_clust = self.children[idx].head_clust
                  self.head_idx = self.children[idx].head_idx
                  if root:
                        self.is_head = True

      def do_node_string(self,feats=None,sep='/',typedef=None,dummy_val="*",display_features=False,display_functions=False,untyped=False):
            """
            This returns a string representation of a node.
            If the node has features the features *values* are appended to the node label. 
            in an order specified by typedef or randomly if typedef equals None.

            @param feats: a mask specifying which feature values should be printed out. 
            If feats is set to None, all relevant features are printed out 
            @type feats: a list of Strings (being relevant attributes for which values are to be printed)
            @param sep:the symbol used to separate the feature values fields  
            @type sep: a string (typically a char)
            @param typedef: a list of attributes specifying which features are to be outputted in which order
            @type typedef: a list of strings
            @param dummy_val: a value used for features with no known value
            @type dummy_val: string
            @param untyped: display the feature attributes as well for instanciated features. Features are displayed in square brackets as attribute=value pairs
            @type untyped:boolean
            @param display_features:flag indicating whether to output features or not
            @type display_features:boolean
            @return: the string representation of this node
            @rtype: string
            """
            lbl = encode_const_metasymbols(self.label)            
            #if display_functions and self.isHead():
                  #lbl = lbl+'-HEAD'
            if display_functions and self.has_function():
                  lbl += '-'+ self.fct
            if display_features and self.features != {}:
                  if typedef != None:
                        flist = typedef
                  else:
                        flist = self.features.keys()
                  if feats != None:
                        flist  = filter(lambda x: x in feats,flist)
                  if untyped:
                        vals = [attribute+'='+ encode_const_metasymbols(self.feature_value(attribute,dummy_val),ext=True) for attribute in flist if self.has_feature(attribute)]
                        return lbl+'-['+sep.join(vals)+']'
                  else:
                        vals = map(lambda x: encode_const_metasymbols(self.feature_value(x,dummy_val)),flist)
                        return sep.join([lbl]+vals)
            else:
                  return lbl


       #Type definition for generating R data tables
      def rtypedef(self):
            return ['word','lemma','cat','subcat','gen','num','pers','mood','tense']
      
      def do_R_string(self):
            """
            This encodes the sentence as an R datatable suitable for importing into R
            """
            tagseq = self.tag_yield()
            return '\n'.join(map(lambda x:self.R_node_string(x),tagseq))
      
      def R_node_string(self,tagword):
            (tagnode,wordnode) = tagword
            tempf = tagnode.features
            tempf['word'] = wordnode.label
            tempf['cat'] = tagnode.basecat
            return '\t'.join(map(lambda x:self.R_feature_value(x,tempf),self.rtypedef()))
      
      def R_feature_value(self,attribute,features,dummyval='<NA>'):
            """
            Maps an attribute to its value. Returns the dummy val if the feature is not specified for this node.
            This also escapes some reserved chars: quotes are normalised   
            
            @param attribute: the feature's attribute
            @type:string
            @param: a dict of attribute value couples
            @type dict
            @param dummy_val: a default value if this node does not specify anything for the attribute
            @type dummy_val: string
            @return: the feature value or the dummy_val if this node does not specify anything for the attribute
            @rtype: a string
            """
            if features.has_key(attribute):
                  return features[attribute]
            else:
                  return dummyval
                  
            
      def feature_value(self,attribute,dummy_val=None):
            """
            Maps an attribute to its value. Returns the dummy val if the feature is not specified for this node.
            
            @param attribute: the feature's attribute
            @type:string
            @param dummy_val: a default value if this node does not specify anything for the attribute
            @type dummy_val: string
            @return: the feature value or the dummy_val if this node does not specify anything for the attribute
            @rtype: a string
            """
            if self.features.has_key(attribute):
                  return self.features[attribute]
            else:
                  return dummy_val

      def has_feature(self,attribute):
            """
            This returns true if this attribute has a non null value, false otherwise
            
            @param attribute: the attribute for which we want to know whether it has a value
            @type attribute: string
            @return: true if this attribute has a non null value, false otherwise
            @rtype:boolean
            """
            return self.features.has_key(attribute) and self.features[attribute] != None
            

      def dependents(self,treeness=True,projective=True):
        """
        Provides the list of immediate dependents of this node head following their linear order.
        The dependencies extracted can be restricted to two forms:
        * To ensure the dependency graph is a tree, set the treeness param to true (ignores control like links). 
        * To ensure the dependency graph is a projective tree, set the projective param to true (ignores filler-gap links)
        The latter implies the former.
        Note that these restrictions do guarantee a formal specification, not that the resulting dependencies are linguistically sound !!

        @param treeness: ensures that the extracted dependencies have a tree structure (no cycles)
        @type treeness: boolean
        @param projective: ensures that the extracted dependencies are strictly projective
        @type projective:boolean
        @return a list of LabelledTree being leaves of the global tree
        @rtype: a list of Labelled Tree
        """
        if self.is_tag():
            return []
        else:
            deplist = []
            for child in self.children:
                if child.isHead():
                    deplist.extend(child.dependents())
                elif projective or not child.is_gap():
                    if not treeness and child.has_pointer():
                        deplist.append(child.pointer.head_node())   
                    else:
                        deplist.append(child.head_node())
            return deplist

      def build_dependency_graph(self,treeness=True,projective=True):
        """
        This turns a LabelledTree into a dependency graph by reading off the dependencies.
        
        @param treeness: ensures that the extracted dependencies have a tree structure (no cycles)
        @type treeness: boolean
        @param projective: ensures that the extracted dependencies are strictly projective
        @type projective:boolean
        @return: a labelled Dependency Graph
        @rtype: a DependencyGraph
        """
        dg = DependencyGraph()
        self.___build_dg(dg,treeness=treeness,projective=projective)
        return dg

      def ___build_dg(self,dg,treeness=True,projective=True):
        """ 
        This is the inner recursive backbone of the prev func.

        @param treeness: ensures that the extracted dependencies have a tree structure (no cycles)
        @type treeness: boolean
        @param projective: ensures that the extracted dependencies are strictly projective
        @type projective:boolean
        """
        if not self.is_tag():
            for child in self.children:
                if child.isHead():
                    child.___build_dg(dg,treeness=treeness,projective=projective)
                elif projective or not child.gap:
                    if not treeness and child.has_pointer():
                        gov = DepVertex(self.head_word,self.head_idx)
                        dep = DepVertex(child.pointer.head_word,child.pointer.head_idx)
                        dg.add_edge(gov,child.pointer.fct,dep)
                        child.pointer.___build_dg(dg,treeness=treeness,projective=projective) # !! introduces potential infinite loops (check this out later on...)
                    else:
                        gov = DepVertex(self.head_word,self.head_idx)
                        dep = DepVertex(child.head_word,child.head_idx)
                        dg.add_edge(gov,child.fct,dep)
                        child.___build_dg(dg,treeness=treeness,projective=projective)

      def __str__(self):
            """
            This pretty prints a PennTreebank style representation of this tree encoded as an indented multiline string.
            Each line of the string ends up by a token.
            Note that parse failures are encoded as '(())'
            return self.pprint(padding=0)
            """
            return self.do_pretty_string()

     

      def do_pretty_string(self,padding=0,failure_code='(())',display_functions=False,display_features=False,lc=True):
            """
            This pretty prints a PennTreebank style representation of this tree encoded as a indented multiline string.
            Each line of the string ends up by a token.
            Note that parse failures are encoded as '(())' unless an alternative code is specified as parameter

            @param: failure_code: a code used to display parse_failures
            @type failure_code: String
            @param:sep: indicates which char to use for appending additional non terminal symbols features to the label at printout
            @type: a string (usually a single char)
            @param:sep: indicates which char to use for appending additional terminal symbols features to the label at printout
            @type: a string (usually a single char)
            @return: a String representation of this tree
            @rtype: a one line String
            """            
            if self.is_parse_failure():
                  return failure_code
            elif self.is_leaf():
                  return self.do_node_string()
            else:
                  str_node = self.do_node_string(display_functions=display_functions,display_features=display_features)
                  if lc:
                        res = ['(',str_node,' ']
                  else:
                        res = ['\n '.ljust(padding+1)+'(',str_node,' ']
                  padding += len(str_node)+2  
                  res.append(self.first_child().do_pretty_string(padding=padding,display_functions=display_functions,display_features=display_features,lc=True))
                  for child in self.children[1:]:
                        res.append(child.do_pretty_string(padding=padding,display_functions=display_functions,display_features=display_features,lc=False))
                  res.append(')')
                  return ''.join(res)


      def do_flat_string(self,failure_code='(())',display_functions=False,display_features=False,feats=None,sep='/',untyped=True):
            """
            This returns a PennTreebank style representation of this tree encoded as a flat one line string.
            This function outputs a format compliant with that of evalb and of standard parsers
            Note that parse failures are encoded as '(())' unless an alternative code is specified as parameter

            @param: failure_code: a code used to display parse_failures
            @type failure_code: String
            @param:sep: indicates which char to use for appending features at printout
            @type: a string (usually a single char)
            @return: a String representation of this tree
            @rtype: a one line String
            """            
            if self.is_parse_failure():
                  return failure_code  
            elif self.is_leaf():
                  return self.do_node_string()
            else:
                  return '(' + self.do_node_string(sep=sep,display_functions=display_functions,display_features=display_features,feats=feats,untyped=untyped) + ' ' + ' '.join(map(lambda child: child.do_flat_string(sep=sep,display_functions=display_functions,display_features=display_features,feats=feats,untyped=untyped),self.children))+')'


      def do_tag_string(self,failure_code='(())',wordsep=" ",tagsep='@',fsep='/',feats=None,tag_word=False,wordtypedef=None,dummy_val="-",display_features=False):
            """
            This pretty prints the POS tagged version of a phrase rooted by this tree.
            In case the parse tree is a parse failure it ouptuts the failure code instead
            
            @param wordsep: the string used to separate tokens
            @type wordsep: string
            @param tagsep: the string used to separate the tag and the word (usually a single char)
            @type tagsep: string
            @param fsep: the string used to separate the fields in a single symbol (usually a single char separating the fields of a tag)
            @type fsep: string
            @param tag_word: flag setting the word/tag order. set to true, the function will print the tag before the word, set to false it prints the word before the tag 
            @type tag_word: boolean
            @return: a pretty printed pos tagged string
            @rtype: String
            """
            if self.is_parse_failure():
                  return failure_code()
            else:
                  taglist =  self.tag_yield() 
                  if tag_word:
                        return wordsep.join(map(lambda x: x[0].do_node_string(sep=fsep,feats=feats,display_features=display_features,typedef=wordtypedef,dummy_val=dummy_val) + tagsep + x[1].do_node_string(sep=fsep,feats=feats,display_features=display_features,typedef=wordtypedef,dummy_val=dummy_val) ,taglist))
                  else:
                        return wordsep.join(map(lambda x: x[1].do_node_string(sep=fsep,feats=feats,display_features=display_features,typedef=wordtypedef,dummy_val=dummy_val) + tagsep + x[0].do_node_string(sep=fsep,feats=feats,display_features=display_features,typedef=wordtypedef,dummy_val=dummy_val) ,taglist))


      def do_xml_node(self,opening_tag=True):
            """
            This returns a French Treebank XML like representation of this node.
            
            @param opening_tag: if set to true, generates an XML starting tag, set to false it generates an XML end tag
            @type opening_tag: boolean
            @return: a string being the xml tag representation of this node
            @rtype: string
            """
            if self.has_basecat():
                  cat = self.basecat
            else:
                  cat = self.label
            if self.is_tag():
                  if opening_tag:
                        nodestr = '<w cat="'+cat+'"'
                        if self.has_function():
                              nodestr += ' fct="'+self.fct+'"'
                        if self.isHead():
                              nodestr += ' head="true"'
                        if len(self.features) > 0:
                              nodestr += ' '+' '.join(map(lambda attribute:attribute+'="'+self.features[attribute]+'"' ,self.features.keys()))+'>'
                              return nodestr
                        elif len(self.features) == 0:
                              return nodestr + '>'
                  else:
                        return '</w>'
            else:
                  if opening_tag:
                        nodestr = '<'+cat
                        if self.has_function():
                              nodestr += ' fct="'+self.fct+'"'
                        if self.isHead():
                               nodestr += ' head="true"'
                        if len(self.features) > 0:
                              return nodestr + ' '+' '.join(map(lambda attribute:attribute+'="'+self.features[attribute]+'"' ,self.features.keys()))+'>'
                        elif len(self.features) == 0:
                              return nodestr+'>'
                  else:
                        return '</'+cat + '>'
                  

      def do_pseudo_xml_string(self,indent=0):
            if self.is_tag():
                  lemma = '*none*'
                  if 'lemma' in self.features:
                        lemma = self.features['lemma']
                  indentstr = ' '*indent
                  return ' '.join([indentstr,self.children[0].label,self.label,lemma])
            else:
                  indentstr = ' '*indent
                  return '\n'.join([indentstr+'<'+self.label+'>']
                      +[child.do_pseudo_xml_string(indent+2) for child in self.children]
                      +[indentstr+'</'+self.label+'>'])

      def do_xml_string(self,index=0,padding=0,root=False):
            """
            This returns a French Treebank like representation of this tree.

            @return: a string encoding the tree in french treebank xml format
            @rtype: a string
            """
            if self.is_leaf():
                  return self.label
            if self.is_tag():
                  return ''.ljust(padding)+self.do_xml_node()+ self.first_child().do_xml_string() + self.do_xml_node(opening_tag=False)
            else:
                  if self.label == '':
                        self.label = 'tree'
                  if root:
                        res = ''.ljust(padding)+'<'+self.label+ ' nb="' +str(index)+ '">\n'
                  else:
                        res = ''.ljust(padding)+self.do_xml_node()+'\n'

                  return res + '\n'.join(map(lambda child: child.do_xml_string(padding=padding+2),self.children)) + '\n'+''.ljust(padding)+'</'+self.label+'>'

      def do_sentence_string(self,wordsep=" ",escape_metasymbols=True):
            """
            This pretty prints the sentence being the yield of this tree.
            
            @param wordsep: the string used to separate the tokens
            @type wordsep: string
            @return: the sentence being the yield of this tree
            @rtype: string
            """
            if self.is_parse_failure():
                  return failure_code()
            else:
                  cyield = self.tree_yield()
                  if escape_metasymbols:
                        return wordsep.join(map(lambda x:encode_const_metasymbols(x.label),cyield))
                  else:
                        return wordsep.join(map(lambda x:x.label,cyield))


      def spmrl_strip(self):
            """
            This preprocesses the SPMRL data format. Moves tag annotated features to the feature avm of the nodes
            """
            if self.is_tag():
               (cat,features,_) = self.label.split('##')
               self.label = cat
               self.features = dict([ tuple(attval.split('='))  for attval in features.split('|')])
            else:
                for child in tree.children:
                    child.spmrl_strip()
            
      def spmrl_unstrip(self):
            """
            Reciprocal of spmrl strip.
            """
            if self.is_tag():
                self.label = self.label+' ##'+'|'.join(["%s=%s"%(att,val) for att,val in self.features.items() ])+'##'
                self.features = {}
            else:
                for child in tree.children:
                    child.spmrl_unstrip()


  
def demo():
      """
      This performs a demo of the main functionalities implemented in this module.
      It may be used as a starting point for developers wishing to reuse the functionalities of this module
      """
      #import svm
      import fastptbparser as parser
      import sys

      tree = parser.parse_line('(S (NP (D the) (N cat)) (VP (V sleeps) (PP (P on) (NP (D the) (N mat))))(ADVP (ADV really))(PONCT .))')
      # print "<A sentence>"
      # print tree.do_sentence_string()
      # print 
      # print "<The PoS tags in Brown format>"
      # print tree.do_tag_string(tagsep="/")
      # print
      # print "<The PoS tags in Database format>"
      # print tree.do_tag_string(tagsep="\t",wordsep="\n")
      # print
      # print "<A parse tree (PTB style, one line flat)>"
      # print tree.do_flat_string()
      # print
      # print "<A parse tree with dummy root (PTB style, multiline indented)>"
      # tree = add_dummy_root(tree)
      # print tree
      # print
      # print "<A parse tree with head annotation (PTB style, multiline indented)>"
      # tree = parser.parse_line("(SENT (NP-SUJ (D le) (NC chat) (ADJ noir) (PP (P de) (NP (D la) (NC voisine)))) (VN (V dort)) (PP-MOD (P sur) (NP (D le) (NC paillasson))))",parse_symbols=True)
      # tree.annotate_all(ftb4_fixer(),ftb_symset4())
      # print tree     
      # print
      # print "<A parse tree in French Treebank XML like format>"
      # print TreebankTree(tree,index=10).do_xml_string()
      # print
      # print "<Dependency triples extracted from the tree>"
      # dg = tree.build_dependency_graph()
      # print dg.triples2string(sorted=True)
      # print
      # print "<A diff illustrated>" 
      # treea = parser.parse_line('(S (NP (D the) (N cat)) (VP (V sleeps) (PP (P on) (NP (D the) (N mat)))))')
      # treeb = parser.parse_line('(S (NP (D the) (N cat)) (VP (V sleeps)) (PP (P on) (NP (D the) (N mat))))')
      # print 'A:',treea.do_flat_string()
      # print 'B:',treeb.do_flat_string()
      # print
      # (common,aspec,bspec) = tree_diff(treea,treeb)
      # print 'Intersection:',', '.join(map(lambda x:x[0]+'|'+str(x[1])+'|'+str(x[2]),common))
      # print 'A diffs:', ', '.join(map(lambda x:x[0]+'|'+str(x[1])+'|'+str(x[2]),aspec))
      # print 'B diffs:',', '.join(map(lambda x:x[0]+'|'+str(x[1])+'|'+str(x[2]),bspec))

      
# A DEPLACER AUTRE MODULE SPECIFIQUE?        
def guess_funlabel(govnode,depnode,dummyval = "DEP"):
      """ Inference of the functional label of the given constituant
      Returns the inferred label
      @param: input dependency (Maximal projections)
      @type: Dep
      @author = Mathieu Falco, Marie Candito
      """
      gov_head = govnode.head_tag()
      dep_head = depnode.head_tag()

      if depnode.isHead():
        return 'PRED'

      # hack for subject clitics that do not appear as subject!!!
      if dep_head.label == 'CLS': return 'SUJ'
      # nothing to do if label already specified
      if depnode.fct != None:
            return depnode.fct 
      dep_pos = depnode.head_tag().basecat
      gov_pos = govnode.head_tag().basecat

      nb_of_dependents = len(govnode.children)# May be fine tuned later on if needed
      # test on the dependent's cat
      if dep_pos == 'D': return 'DET'
      if dep_pos == 'PREF': return 'MOD'
      # marie: relation coord: pour g�rer d'un coup tous les cas, y compris des erreurs de tagging sur le coordonnant,
      #         on recherche simplement les d�pendants dont la projection maximale est COORD
      if depnode.label == 'COORD': return 'COORD'
      # les coord �tant d�j� g�r�s supra, les PONCT restant ne sont pas des coordonnants
      if dep_pos == 'PONCT': return 'PONCT'
      # if fine-grained cat is available
      if dep_head.label == 'CC': return 'COORD'
      # MOD_REL(GVT, X) si X a Srel comme t�te de son constituant
      # marie: on sort les cas COORD dominant Srel
      if depnode.label == 'Srel' and govnode.head_child() != 'COORD': return 'MOD_REL'

      # test on both governor and dependent cat
      if dep_pos != 'PONCT' and gov_pos == 'PONCT':
            return 'DEP_COORD'
      # Traitement des conjonctions en gouverneur
      if gov_pos == 'C':
            # coordination: DEP_COORD(C, dep) si le constituant au-dessus du C est COORD dans le spine (spine)
            if govnode.basecat=='COORD': #BC:I translate dep.governor.first_projection() == 'COORD' by govnode.basecat=='COORD'
                # marie: debug � la hache: pb des modifieurs "contre la monnaie mais(C) AUSSI(adv) contre ...
                # si dependant adverbial, on ne le code en dep_coord que s'il est le seul dep
                  if dep_pos == 'ADV' and nb_of_dependents > 1: return 'MOD'
                  return 'DEP_COORD'
            # subordination: OBJ(C, dep) sinon
            return 'OBJ'

        # rem: cas des CS d�pendant de CC (et donc dep_coord) trait�s supra
      if dep_head.label == 'CS': 
            if gov_pos == 'P':
                return 'OBJ'
            elif dep_head.head_word not in ['que','qu\'']: 
                return 'MOD'
            else:
                return dummyval # (eg. "le fait QUE Paul soit parti => QUE added as dependent of "fait")
        # si fine-grained cat non disponible
      if dep_pos == 'C':
            if depnode.basecat == 'COORD':
                  return 'coord'
            return 'COMP' # should rather be DEP here (eg. "le fait QUE Paul soit parti => QUE added as dependent of "fait")

        # GOUVERNEUR ADJ or ADV
      if gov_pos in ['A','ADV']:
            # marie: insuffisant: il faut changer de gouverneur aussi ...
            # marie: TODO: TESTER presence d'un adv comparatif!
            # arg(A, C) "plus importante que"
            # arg(A, C) "plus faiblement que"
            if dep_pos == 'C' and depnode.basecat != 'COORD':
                return 'ARG_A'
            if dep_pos == 'ADV': return 'MOD'

        # GOUVERNEUR PRO
      if gov_pos == 'PRO':
            if dep_pos in ['A','ADV','N']: return 'MOD'

        # GOUVERNEUR PREP
      if gov_pos in ['P', 'P+D']:
            # OBJ(P ou P+D, N ou PRO)
            # marie: to handle cases of VP obj of P 
            # (appearing only if original constituent trees are modified)
            # if dep_pos in ['N', 'PRO']:            dependent.deplabel = 'OBJ'
            if dep_pos in ['N','PRO','V','A']: return 'OBJ'
            # ARG(P ou P+D, P ou P+D)
            # marie debug: un P d�pendant de P: si seul d�pendant = OBJ (de chez moi), sinon = ARG
            if dep_pos in ['P','P+D']:
                if nb_of_dependents == 1: return 'OBJ'
                return 'ARG'
            # marie: ADV d�pendant de P
            # -> OBJ si seul d�pendant ("depuis longtemps"), MOD sinon
            # TODO: gestion des cas "pour encore longtemps" "pour longtemps encore"
            if dep_pos == 'ADV':
                if nb_of_dependents == 1: return 'OBJ'
                return 'MOD'

        # GOUVERNEUR N
      if gov_pos == 'N':
            if dep_pos in ['P','P+D']:
                # marie: "peu de surprises" => "de" est tagg� Prep, mais deplabel = DET
                # rep�r� par: la prep ne projette pas de PP
                if depnode.label != 'PP': return 'DET'
                # sinon: sous-specification (mod ou arg): on ne gere pas la sous-cat du nom
                # DEP(N, P ou P+D)
                return dummyval
            # to comply to input where prepositional PPs are within VPinf
            if dep_pos == 'V':
                if depnode.label in [ 'Ssub', 'VPinf']:
                    return 'DEP'
                else: return 'MOD'
            # MOD(N, A, ADV, N)
            if dep_pos in ['A', 'ADV', 'N']: return 'MOD'

        # GOUVERNEUR V
      if gov_pos == 'V':
            # MOD(V, A ou N ou ADV)
            if dep_pos in ['A', 'N', 'ADV']: return 'MOD'
            # Traitement du cas AUX(V,V)
            if dep_pos == 'V':
                if depnode.label == 'Ssub': # cas de fonction manquante dans le treebank
                    return 'MOD'
                # AUX_TPS(V, V) si le d�pendant est une forme conjugu�e de l'auxiliaire avoir
                if dep_head.head_word in GramForms['avoir']: return 'AUX_TPS'
                # AUX_TPS(V, V) si le d�pendant est une forme du passif � un temps compos�
                if dep_head.head_word in ['�t�']: return 'AUX_PASS'
                # AUX_CAUS(V, V) si le d�pendant est une forme conjugu�e de "faire"
                if dep_head.head_word in GramForms['faire']: return 'AUX_CAUS'
                if dep_head.head_word in GramForms['etre']:
                    #if tete.lemma in liste_etre_pp:
                    if gov_head.head_word in GramForms['etre_pp']: return 'AUX_TPS'
                    # si pr�sence d'un clitique pronominal alors AUX_TPS(V, V) sinon AUX_PASS(V, V)
                    for child in govnode.children:
                          if child.head_word in ['se','s\'']:
                                return 'AUX_TPS'
                    return 'AUX_PASS'
                # cases of VPpart starting with a prep, modifying a V => always modifiers
                if dep_head.label == 'VPR': return 'MOD'
            # COMP(V, C ou P) si le C ou le P n'a pas de d�pendants


            #****TODO******
            #if dep_pos in ['C','P'] and len(self.get_dep_by_governor(dep.dependent.lidx)) == 0:
                # Problem with the compound: "si bien que"
            #    return 'COMP'


            # AFF(V, CL)
            if dep_pos == 'CL': return 'AFF'
            # Marie: hack: les d�pendants de participes pass�s non trait�s par functional role labelling
            # => � la h�che
            if dep_pos in ['P', 'P+D']:
                if dep_head.label == 'par': return 'P_OBJ'
                if dep_head.label in ['de', 'du', 'des', 'd']: return 'DE_OBJ'
                if dep_head.label in ['�', 'au', 'aux']: return 'A_OBJ'
                return 'MOD'
            if dep_pos in verbs:
                # look for a prep before the verb, without any dep
                #****TODO****:  
                  pass
                #for deplidx in sorted([d.lidx for d in self.get_dependents(dep.dependent.lidx) if d.lidx < dep.dependent.lidx]):
                #    depv = self.get_lexnode(deplidx)
                #    if depv.cat == 'P' and not(self.has_dependents(depv.lidx)):
                #        if depv.form == 'par': return 'P_OBJ'
                #        if depv.form in ['de', 'du', 'des', 'd']: return 'DE_OBJ'
                #        if depv.form in ['�', 'au', 'aux']: return 'A_OBJ'
                #        return 'MOD'
                        
                    
        # Marie: d�faut
      return dummyval
 


def treebank2strings(parser):
      i = 0
      tf =  ftb4_fixer()
      pt = ftb_symset4()
      line = sys.stdin.readline()
      while line != '':
            tree = parser.parse_line(line,parse_symbols=True)
            print (' '.join(map(lambda x: str(x),tree.tree_yield())))
            line = sys.stdin.readline()
            
def treebank2xml(parser):
      line = sys.stdin.readline()
      i = 0
      tf =  ftb4_fixer()
      pt = ftb_symset4()
      while line != '':
            tree = parser.parse_line(line,parse_symbols=True)
            tree.annotate_all(tf,pt)
            #print tree.do_pretty_string(display_functions=True)
            print (TreebankTree(tree,index=i).do_xml_string())
            line = sys.stdin.readline()
            i += 1

def tree2IOB(tree):
      tree.index_leaves()
      chunks = tree.extract_base_np()
      tag_words = tree.tag_yield()
      iob_toks = []
      for token in tag_words:
            iob_toks.append([token[1].label,token[0].label,'O-NP'])
      for c in chunks:
            startidx = c[0]
            iob_toks[startidx][2] = 'B-NP'
            for idx in range (startidx+1,c[1]+1):
                  iob_toks[idx][2] = 'I-NP'
      for token in iob_toks:
            print ('\t'.join(token))

def extract_base_np(parser):
      line = sys.stdin.readline()
      while line != '':
            tree = parser.parse_line(line,parse_symbols=True)
            tree2IOB(tree)
            print 
            line = sys.stdin.readline()

class MalformedTreeException (Exception):
      """
      This class encodes an error while processing the tree (inconsistent formal structure coming from incorrect annotations in general)
      """
      def __init__(self,tree):
            self.tree = tree
      def __str__(self):
            return "(Malformed Tree Error) in (sub)tree \n\t%s\n" % (self.tree.do_flat_string(),)
            
if __name__ == "__main__":
      pass
#      treebank2strings(parser)
#      treebank2xml(parser)
#      extract_base_np(parser)
